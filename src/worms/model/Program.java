package worms.model;

import java.util.Map;

import be.kuleuven.cs.som.annotate.*;
import worms.model.Exceptions.FailedExecutionException;
import worms.model.Types.*;
import worms.model.programs.ProgramParser;

/**
 * A class representing programs involving a factory, parser, global variables
 * and a worm.
 * 
 * @invar The mainstatement must be a valid statement for any program.
 * 		  |	isValidStatement(getMainStatement())
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 * 
 */
public class Program {

	/**
	 * Initialize a new program with the given parser and programfactory.
	 * 
	 * @param parser
	 *            The parser to use in this program.
	 * @param factory
	 *            The factory to use in this program.
	 * @effect The GlobalVariable in the given factory are set to the GlobalVariable of this
	 *         program. 
	 *       | factory.setGlobalVariable(getGlobalVariable())
	 * @throws IllegalArgumentException
	 * 		 	An IllegalArgumentException is thrown if the statement from the given parser
	 * 			is not a valid statement, or if the globals from the parser are not valid.
	 * 		 |	!isValidStatement(parser.getStatement()) || !isValidGlobals(parser.getGlobals())
	 */
	@Raw
	public Program(ProgramParser<Expression<? extends Type>, Statement, Type> parser,
			ProgramFactoryImpl factory) throws IllegalArgumentException{
		if (!isValidStatement(parser.getStatement()))
			throw new IllegalArgumentException();
		this.mainStatement = parser.getStatement();
		if (!isValidParserGlobals(parser.getGlobals()))
			throw new IllegalArgumentException();
		this.globalVariable = new GlobalVariable(parser.getGlobals());
		factory.setGlobalVariable(getGlobalVariable());
	}
	
	/**
	 * Return the mainStatement of this program.
	 */
	@Basic @Immutable
	protected Statement getMainStatement() {
		return mainStatement;
	}

	/**
	 * Variable registering the main statement of this program.
	 */
	private final Statement mainStatement;
	
	/**
	 * Return the global variables of this program.
	 */
	@Basic @Immutable
	protected GlobalVariable getGlobalVariable() {
		return globalVariable;
	}
	
	/**
	 * Variable registering the global variables of this program.
	 */
	private final GlobalVariable globalVariable;
	
	/**
	 * Execute this program.
	 * 
	 * @effect If the execution of the mainstatement is succesfull (i.e. did not
	 *         throw an exception), then the flags of execution of the
	 *         statements are reset. | getGlobalVariable().resetStatementFlags()
	 * @effect The next turn is started after the execution of this program. |
	 *         worm.getWorld().startNextTurn();
	 * @throws IllegalStateException
	 *             An IllegalStateException is thrown if the factory of this
	 *             program does not have an executing worm assigned to it. |
	 *             factory.getExecutingWorm() == null
	 */
	protected void execute() throws IllegalStateException {
		if (getGlobalVariable().getWorm() == null)
			throw new IllegalStateException(
					"The factory of this program does not have an executing worm.");
		try {
			getMainStatement().execute();
			getGlobalVariable().resetStatementFlags();
		} catch (FailedExecutionException exc) {
		}
		getGlobalVariable().getWorm().getWorld().startNextTurn();
	}

	/**
	 * Set the worm of this program to the given worm.
	 * 
	 * @param worm
	 *          The worm to set.
	 * @effect The worm of the factory is set to the given worm.
	 * 			factory.setExecutingWorm(worm);
	 */
	protected void setWorm(Worm worm) {
		getGlobalVariable().setWorm(worm);
		}


	/**
	 * Check whether this program is well formed.
	 * 
	 * @return	true if and only if the mainstatement of this program is well formed.
	 * 		  |	result == mainStatement.isWellFormed()
	 */
	protected boolean isWellFormed() {
		return getMainStatement().isWellFormed();
	}
	
	/**
	 * Check whether the given GlobalVariable is a valid global for any Program.
	 * 
	 * @param globals
	 *        The globals to check.
	 * @return True if and only true if the globals is an effective globals.
	 *         | result == (globals != null)
	 */
	private static boolean isValidParserGlobals(Map<String,Type> globals){
		return (globals != null);
	}
	
	/**
	 * Check whether the given statement is a valid statement for any Program.
	 * 
	 * @param statement
	 *        The statement to check.
	 * @return True if and only true if the statement is an effective statement.
	 *         | result == (statement != null)
	 */
	@Model
	private static boolean isValidStatement(Statement statement){
		return (statement != null);
	}
	

}
