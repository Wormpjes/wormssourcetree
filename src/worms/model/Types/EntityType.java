package worms.model.Types;

import be.kuleuven.cs.som.annotate.*;
import worms.model.*;

/**
 * A class of entitytypes involving a value.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class EntityType implements Type {
	
	/**
	 * Initialize this new entitytype with the given value.
	 * 
	 * @param entity
	 * 			The value for this new entitytype.
	 */
	public EntityType(CircularEntity entity) {
		this.value = entity;
	}
	
	/**
	 * Initialize a new entitytype with default value.
	 * 
	 * @effect	This new entitytype is initialized as an entitytype with value null.
	 * 		  |	this(null)
	 */
	public EntityType() {
		this(null);
	}
	
	/**
	 * Variable registering the value of this entitytype.
	 */
	private final CircularEntity value;
	
	/**
	 * Return the value of this entitytype.
	 */
	@Override @Basic @Immutable
	public CircularEntity getValue() {
		return this.value;
	}

	/**
	 * Return a string representation of this entitytype.
	 * 
	 * @return 	If the value of this EntityType is a worm,
	 * 			then it returns a string saying it is a worm with the name of the worm.
	 * 		  |	if (value instanceof Worm)
	 * 		  |		then return "Worm: " + ((Worm) value).getName()
	 * 			If the value of this EntityType is a food ration,
	 * 			then it returns a string saying it is a food ration and the position of that food ration.
	 * 		  |	if (value instanceof Food)
	 *		  |		then return "Food ration at position (" + value.getX() + ", " + value.getY() + ")."
	 *			If the value of this EntityType is a projectile,
	 *			then it returns a string saying it is a projectile and the position of that projectile.
	 *		  |	if (value instanceof Projectile)
	 *		  |		then return "Projectile at position (" + value.getX() + ", " + value.getY() + ")."
	 *			If the value is null then it returns a string saying null.
	 *		  |	if (value == null)
	 *		  |		then return "Null"
	 *			Otherwise it returns a string representation of the value of this EntityType.
	 *		  |		return value.toString()
	 */
	@Override
	public String toString() {
		if (value instanceof Worm)
			return "Worm: " + ((Worm) value).getName();
		if (value instanceof Food)
			return "Food ration at position (" + value.getX() + ", " + value.getY() + ").";
		if (value instanceof Projectile)
			return "Projectile at position (" + value.getX() + ", " + value.getY() + ").";
		if (value == null)
			return "Null";
		return value.toString();
	}

	/**
	 * Compare this entitytype to the given other type.
	 * 
	 * @param other
	 * 			The other type to compare this entitytype to.
	 * @return	-1 if the given other type is null.
	 * 		  |	if (other == null)
	 * 		  |		then result == -1
	 * 			0 if the value of this entitytype is equal to the value of the given other type.
	 * 		  |	if (getValue() == other.getValue())
	 * 		  |		then result == 0
	 * 			Otherwise the result is -1
	 * 		  |	result == -1
	 */
	@Override
	public int compareTo(Type other) {
		if (other == null)
			return -1;
		if (getValue() == other.getValue()) 
			return 0;
		return -1;
	}

}
