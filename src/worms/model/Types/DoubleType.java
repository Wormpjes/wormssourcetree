package worms.model.Types;

import be.kuleuven.cs.som.annotate.*;
import worms.util.Util;

/**
 * A class of DoubleTypes involving a value.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class DoubleType implements Type {
	
	/**
	 * Initialize this new DoubleType with the given value.
	 * 
	 * @param value
	 * 			The value for this new DoubleType.
	 */
	public DoubleType(double value) {
		this.value = value;
	}
	
	/**
	 * Initialize this new DoubleType with a default value.
	 * 
	 * @effect	This new DoubleType is initialized as a new DoubleType with value 0.0
	 * 		  |	this(0.0)
	 */
	public DoubleType() {
		this(0.0);
	}
	
	/**
	 * Variable registering the value of this DoubleType.
	 */
	private final Double value;
	
	/**
	 * Return the value of this DoubleType.
	 */
	@Override @Basic @Immutable
	public Double getValue() {
		return this.value;
	}

	/**
	 * Compare this DoubleType to the given other type.
	 * 
	 * @return	If the given other type is not a DoubleType, then it returns -1
	 * 		  |	if (! (other instanceof DoubleType))
	 * 		  |		then result == -1
	 * 			If the value of the given other DoubleType is less than the value of this DoubleType,
	 * 			then it returns -1.
	 * 		  |	if (! Util.fuzzyGreaterThanOrEqualTo(getValue(), ((DoubleType)other).getValue()))
	 * 		  |		then result == -1
	 * 			If the value of the given other DoubleType is equal to the value of this DoubleType,
	 * 			then it returns 0.
	 * 		  |	if (Util.fuzzyEquals(getValue(), ((DoubleType)other).getValue()))
	 * 		  |		then result == 0
	 * 			Otherwise the value of this DoubleType is larger than the value of the other DoubleType
	 * 			and it returns 1.
	 * 		  |	else then return 1
	 */
	@Override
	public int compareTo(Type other) {
		if (! (other instanceof DoubleType))
			return -1;
		if (! Util.fuzzyGreaterThanOrEqualTo(getValue(), ((DoubleType)other).getValue()))
			return -1;
		if (Util.fuzzyEquals(getValue(), ((DoubleType)other).getValue()))
			return 0;
		else
			return 1;
	}

	/**
	 * Return a string representation of this DoubleType.
	 * 
	 * @return	The string of the value.
	 * 		  |	value.toString()
	 */
	@Override
	public String toString() {
		return value.toString();
	}

}
