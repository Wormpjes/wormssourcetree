package worms.model.Types;

/**
 * An interface of Types.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public interface Type {
	
	/**
	 * Return the value of this type.
	 */
	public Object getValue();
	
	/**
	 * Return a string representation of this type.
	 */
	@Override
	public String toString();
	
	/**
	 * Compare this type to the given other type.
	 * 
	 * @param other
	 * 			The other type to compare to.
	 */
	public int compareTo(Type other);
	
}
