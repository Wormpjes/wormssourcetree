package worms.model.Types;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class of BooleanTypes involving a value.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class BooleanType implements Type {

	/**
	 * Initialize this new BooleanType with the given value.
	 * 
	 * @param value
	 * 		  The value for this new BooleanType.
	 */
	public BooleanType(boolean value) {
		this.value = value;
	}
	
	/**
	 * Initialize this new BooleanType with a default value.
	 * 
	 * @effect	This new BooleanType is initialized as a new BooleanType with value false.
	 * 		  |	this(false)
	 */
	public BooleanType() {
		this(false);
	}
	
	/**
	 * Variable registering the value of this BooleanType.
	 */
	private final Boolean value;
	
	/**
	 * Return the value of this BooleanType.
	 */
	@Override @Basic @Immutable
	public Boolean getValue() {
		return this.value;
	}

	/**
	 * Return a string representation of this BooleanType.
	 * 
	 * @return 	the value as a string.
	 * 		  |	value.toString()
	 */
	@Override
	public String toString() {
		return value.toString();
	}

	/**
	 * Compare this BooleanType to the given other type.
	 * 
	 * @return	if the given other type is not a BooleanType, then it returns -1
	 * 		  |	if (! (other instanceof BooleanType))
	 * 		  |		then result == -1
	 * 			if the value of the given other type is equal to the value of this BooleanType,
	 * 			then it returns 0, otherwise it returns -1.
	 * 		  |	if (value == ((BooleanType) other).getValue())
	 * 		  |		then result == 0
	 * 		  |	else then result == -1
	 */
	@Override
	public int compareTo(Type other) {
		if (! (other instanceof BooleanType))
			return -1;
		if (value == ((BooleanType) other).getValue())
			return 0;
		return -1;
	}
}
