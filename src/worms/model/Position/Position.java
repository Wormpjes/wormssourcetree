package worms.model.Position;

import java.util.Random;

import be.kuleuven.cs.som.annotate.*;
import worms.model.World;
import worms.util.Util;

/**
 * A class of positions involving an x-coordinate and a y-coordinate.
 * 
 * @invar The x-coordinate of this position is a valid x-coordinate. 
 * 		| isValidX(getX())
 * @invar The y-coordinate of this position is a valid y-coordinate.
 *      | isValidY(getY())
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 2.0
 * 
 */
@Value
public class Position {

	/**
	 * Initialize a new position with the given x an y coordinate.
	 * 
	 * @param x
	 *            The x-coordinate of the new position.
	 * @param y
	 *            The y-coordinate of the new position.
	 * @effect The x-coordinate of the new position is set to the given x.
	 *       | setX(x)
	 * @effect The y-coordinate of the new position is set to the given y.
	 *       | setY(y)
	 * @throws IllegalArgumentException
	 *             An IllegalArgumentException is thrown if the x value is not a
	 *             valid x. 
	 *        | ! isValidX(x)
	 * @throws IllegalArgumentException
	 *             An IllegalArgumentException is thrown if the y value is not a
	 *             valid y. 
	 *        | ! isValidY(y)
	 */
	public Position(double x, double y) throws IllegalArgumentException {
		if (!isValidX(x))
			throw new IllegalArgumentException();
		this.x = x;
		if (!isValidY(y))
			throw new IllegalArgumentException();
		this.y = y;
	}

	/**
	 * Return the x coordinate of this position.
	 */
	@Basic @Immutable
	public double getX() {
		return x;
	}
	
	/**
	 * Check whether the given x is a valid x-coordinate.
	 * 
	 * @param x
	 *         The x value to check.
	 * @return true if and only if the x value is not NaN. 
	 * 		 | result == ! Double.isNaN(x)
	 */
	public boolean isValidX(double x) {
		return ! Double.isNaN(x);
	}

	/**
	 * Variable registering the x coordinate of this position.
	 */
	private final double x;

	/**
	 * Return the y coordinate of this position.
	 */
	@Basic @Immutable
	public double getY() {
		return y;
	}

	/**
	 * Check whether the given y is a valid y.
	 * 
	 * @param y
	 *         The y value to check.
	 * @return true if and only if the y value is not NaN. 
	 * 		 | result == ! Double.isNan(y)
	 */
	public boolean isValidY(double y) {
		return ! Double.isNaN(y);
	}

	/**
	 * Variable registering the y coordinate of this position.
	 */
	private final double y;
	
	/**
	 * Return a new position which is the result of subtracting the second position of the first position.
	 * 
	 * @param position1
	 * 			The first position.
	 * @param position2
	 * 			The second position.
	 * @return	A new position is returned with as x-coordinate the x-coordinate of the first position
	 * 			minus the x-coordinate of the second position and as y-coordinate the y-coordinate
	 * 			of the first position minus the y-coordinate of the second position.
	 * 		  |	return new Position(position1.getX() - position2.getX(), position1.getY() - position2.getY())
	 */
	public static Position subtract(Position position1, Position position2) {
		return new Position(position1.getX() - position2.getX(),
				position1.getY() - position2.getY());
	}

	/**
	 * Return the column-number of the pixel in which this position is located.
	 * 
	 * @param world
	 *            The world in which this position is located.
	 * @return The x-coordinate of this position divided by the width of a pixel
	 *         in the given world, rounded down to the nearest integer. 
	 *       | (int) Math.floor(getX()/world.pixelWidth())
	 */
	public int getPixelCol(World world) {
		return (int) Math.floor(getX() / world.pixelWidth());
	}

	/**
	 * Return the row-number of the pixel in which this position is located.
	 * 
	 * @param world
	 *            The world in which this position is located.
	 * @return The number of rows in the passablemap of the world subtracted by
	 *         the y-coordinate of this position divided by the height of a
	 *         pixel in the given world and rounded down to the nearest integer.
	 *       | world.getNbRows() - ((int) Math.floor(getY()/world.pixelHeight()) )
	 */
	public int getPixelRow(World world) {
		return (int) Math.floor((world.getNbRows() - Math.floor(getY() / world.pixelHeight())));
	}

	/**
	 * Return the angle between this position and the given other position.
	 * 
	 * @param otherPosition
	 *            The other position.
	 * @return The inverse tangent of the difference in y-coordinates divided by
	 *         the difference in x-coordinates. 2*Pi is added if the angle is
	 *         negative. 
	 *       | angle = Math.atan2(getX() - otherPosition.getX(), getY() - otherPosition.getY()) 
	 *       | if (angle >= 0) 
	 *       | 	then return angle 
	 *       | else
	 *       |	 return angle + 2.0 * Math.PI
	 */
	public double getDirectionToOtherPosition(Position otherPosition) {
		Position temp = subtract(otherPosition,this);
		double angle = Math.atan2(temp.getY(), temp.getX());
		if (Util.fuzzyGreaterThanOrEqualTo(angle, 0.0))
			return angle;
		return angle + (2.0 * Math.PI);
	}

	/**
	 * Return the distance between this position and the given other position.
	 * 
	 * @param otherPosition
	 *            The other position.
	 * @return The squareroot of the sum of the difference in x-coordinates
	 *         squared and the difference in y-coordinates squared. 
	 *       | Math.sqrt( Math.pow( (this.getX() - otherPosition.getX() ), 2) 
	 *       | + Math.pow((this.getY() - otherPosition.getY() ), 2));
	 */
	public double getDistanceToOtherPosition(Position otherPosition) {
		Position temp = subtract(this,otherPosition);
		return Math.sqrt(Math.pow(temp.getX(), 2)
				+ Math.pow(temp.getY(), 2));
	}

	/**
	 * Return a random position within the given world for the given radius.
	 * 
	 * @param world
	 *            The world in which the random position has to be.
	 * @param radius
	 *            The radius for which the position has to be inside the world.
	 * @return The x-coordinate is calculated as a random double in the range
	 *         [radius, world.getWidth() - radius]. The y-coordinate is
	 *         calculates as a random double in the range [radius,
	 *         world.getHeight() - radius]. A new position with the random x and
	 *         y coordinates is returned. 
	 *       | return new Position(radius + random1 * (world.getWidth() - radius), 
	 *       | radius + random2 * (world.getHeight() - radius));
	 */
	public static Position randomPosition(World world, double radius) {
		double random1 = new Random().nextDouble();
		double random2 = new Random().nextDouble();
		return new Position(radius + random1 * (world.getWidth() - radius),
				radius + random2 * (world.getHeight() - radius));
	}

	/**
	 * Return the positions of the corners of the given pixel in the given
	 * world.
	 * 
	 * @param world
	 *            The world in which the pixel is located.
	 * @param row
	 *            The row of the pixel.
	 * @param col
	 *            The column of the pixel.
	 * @return An array of four positions is returned. 
	 * 		 | new Position[] {upperLeft, lowerLeft, upperRight, lowerRight}
	 */
	public static Position[] getPixelCorners(World world, int row, int col) {
		Position upperLeft = new Position(col * world.pixelWidth(),
				(world.getNbRows() - row) * world.pixelHeight());
		Position lowerLeft = new Position(col * world.pixelWidth(),
				(world.getNbRows() - row - 1) * world.pixelHeight());
		Position upperRight = new Position((col + 1) * world.pixelWidth(),
				(world.getNbRows() - row) * world.pixelHeight());
		Position lowerRight = new Position((col + 1) * world.pixelWidth(),
				(world.getNbRows() - row - 1) * world.pixelHeight());
		return new Position[] { upperLeft, lowerLeft, upperRight, lowerRight };
	}

	/**
	 * Return the center of the given pixel in the given world as a new
	 * position.
	 * 
	 * @param world
	 *            The world in which the pixel is located.
	 * @param row
	 *            The row of the pixel.
	 * @param col
	 *            The column of the pixel.
	 * @return The x-coordinate of the new position is equal to the width of a
	 *         pixel in the given world multiplied by the column of the pixel
	 *         plus a half. The y-coordinate of the new position is equal to the
	 *         height of the world lowered by the width of a pixel in the world
	 *         multiplied by the row of the pixel plus a half. 
	 *       | return new Position( (col + 0.5) * world.pixelWidth(), 
	 *       | 						world.getHeight() - (row + 0.5) * world.pixelHeight())
	 */
	public static Position getPixelCenter(World world, int row, int col) {
		return new Position((col + 0.5) * world.pixelWidth(), world.getHeight()
				- (row + 0.5) * world.pixelHeight());
	}

	/**
	 * Check whether this position overlaps with the given pixel in the given
	 * world.
	 * 
	 * @param world
	 *            The world in which the pixel and position are located.
	 * @param row
	 *            The row of the pixel.
	 * @param col
	 *            The column of the pixel.
	 * @param radius
	 *            The radius for which the overlapping of an area is checked.
	 * @return 	True, if the x-coordinate of this position is in the (exlusive) range (corners[1].getX(), corners[3].getX())
	 * 		   	and the y-coordinate of this position is in the (exclusive) range (corners[1].getY() - radius, corners[0].getY() + radius).
	 * 			The array corners is the result of calling the method getPixelCorners(world, row, col),
	 * 			with the given world, row and column.
	 * 		  |	Position[] corners = getPixelCorners(world, row, col)
	 * 		  |	if ( (getX() > corners[1].getX()) && (getX() < corners[3].getX())
	 * 		  |		&& (getY() > corners[1].getY() - radius) && (getY() < corners[0].getY() + radius) )
	 * 		  |		then result == true
	 * 			True, if the x-coordinate of this position is in the (exlusive) range (corners[1].getX() - radius, corners[3].getX() + radius)
	 * 		   	and the y-coordinate of this position is in the (exclusive) range (corners[1].getY(), corners[0].getY()).
	 * 			The array corners is the result of calling the method getPixelCorners(world, row, col),
	 * 			with the given world, row and column.
	 * 		  |	Position[] corners = getPixelCorners(world, row, col)
	 * 		  |	if ( (getX() > corners[1].getX() - radius) && (getX() < corners[3].getX() + radius)
	 * 		  |			&& (getY() > corners[1].getY()) && (getY() < corners[0].getY()) )
	 * 		  |		then result == true
	 * 			Otherwise, true if the distance between this position and any of the four corners of the pixel is less than the given radius.
	 * 		  |	for each corner in getPixelCorners(world, row, col):
	 * 		  |		if getDistanceToOtherPosition(corner) < radius
	 * 	      |			then result == true
	 */
	public boolean areaOverlapsWithPixel(World world, int row, int col,
			double radius) {
		Position[] corners = getPixelCorners(world, row, col);
		if ( (! Util.fuzzyLessThanOrEqualTo(getX(), corners[1].getX())) && (! Util.fuzzyGreaterThanOrEqualTo(getX(), corners[3].getX()))
			 && ((! Util.fuzzyLessThanOrEqualTo(getY(), corners[1].getY() - radius)) && (! Util.fuzzyGreaterThanOrEqualTo(getY(), corners[0].getY() + radius))))
				return true;
		
		if ( (! Util.fuzzyLessThanOrEqualTo(getX(), corners[1].getX() - radius)) && (! Util.fuzzyGreaterThanOrEqualTo(getX(), corners[3].getX() + radius))
				 && ((! Util.fuzzyLessThanOrEqualTo(getY(), corners[1].getY())) && (! Util.fuzzyGreaterThanOrEqualTo(getY(), corners[0].getY()))))
					return true;
		
		for (Position corner : corners)
			if (! Util.fuzzyGreaterThanOrEqualTo(getDistanceToOtherPosition(corner), radius))
				return true;
		return false;
	}
	
	/**
	 * Return a string representation of this position.
	 * 
	 * @return	The x- and y-coordinate of this position between brackets and separated by a comma.
	 * 	      |	return "(" + getX() + " , " + getY() + ")"
	 */
	@Override
	public String toString() {
		return "(" + getX() + " , " + getY() + ")";
	}

}
