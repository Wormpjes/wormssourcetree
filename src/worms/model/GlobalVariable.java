package worms.model;

import java.util.ArrayList;
import java.util.Map;

import be.kuleuven.cs.som.annotate.*;
import worms.model.Types.Type;

/**
 * A class of globalVariable involving the global variables of a program and the executed statements.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class GlobalVariable {
	
	/**
	 * Initialize this new globalVariable with the given map of global variables.
	 * 
	 * @param globalVariable
	 * 			The global variables of the program associated with this globalVariable.
	 */
	public GlobalVariable(Map<String,Type> globalVariable) throws IllegalArgumentException {
		if (! isValidGlobalVariableMap(globalVariable))
			throw new IllegalArgumentException();
		this.globalVariableMap = globalVariable;
	}
	
	/**
	 * Variable registering the worm stored in this globalVariable.
	 */
	private Worm worm;
	
	/**
	 * Variable registering the global variables stored in this globalVariable.
	 */
	private final Map<String,Type> globalVariableMap;
	
	/**
	 * Variable storing the executed statements of the program associated with this globalVariable.
	 */
	private ArrayList<Statement> executedStatements = new ArrayList<Statement>();
	
	/**
	 * Return the global variables stored in this globalVariable.
	 */
	@Basic
	protected Map<String, Type> getGlobalVariableMap() {
		return this.globalVariableMap;
	}
	
	/**
	 * Return a string representation of this object.
	 */
	@Override
	public String toString() {
		return "globalVariable [globalVariable=" + globalVariableMap + "]";
	} 
	
	/**
	 * Add the given statement to the list of executed statements.
	 * 
	 * @param statement
	 * 			The statement to add.
	 */
	protected void addStatement(Statement statement) throws IllegalArgumentException{
		if (!isValidStatement(statement))
			throw new IllegalArgumentException();
		this.executedStatements.add(statement);
	}
	
	/**
	 * Reset the isExecuted-flags of the statements in the list of executed statements stored in this globalVariable.
	 * 
	 * @effect	For eacht statement in the list of executed statements, their isExecuted flag is set to false.
	 * 		  |	for each statement in executedStatements:
	 * 		  |		statement.setIsExecuted(false);
	 * @effect	The list of executed statements is cleared.
	 * 		  |	executedStatements.clear()
	 */
	protected void resetStatementFlags() {
		for (Statement statement:executedStatements) {
			statement.setIsExecuted(false);
		}
		clearStatements();
	}
	
	/**
	 * Clear the list of statements
	 */
	private void clearStatements() {
		executedStatements.clear();
	}
	
    /**
     * Return the worm attached to this globalVariable
     */
	@Basic
	protected Worm getWorm() {
		return worm;
	}

	/**
	 * Set the worm of this program to the given worm.
	 * 
	 * @param worm
	 *        The worm to set.
	 * @throws IllegalArgumentException
	 *         An IllegalArgumentException is thrown if the worm is not
	 *         a valid worm.
	 *         | !isValidWorm(worm)
	 */
	protected void setWorm(Worm worm) throws IllegalArgumentException {
		if(!isValidWorm(worm))
			throw new IllegalArgumentException();
		this.worm = worm;
	}
	
	/**
	 * Check whether the given statement is a valid statement for any globalVariable.
	 * 
	 * @param statement
	 *        The statement to check.
	 * @return True if and only true if the statement is an effective statement and is executed.
	 *         | result == ((statement != null) && (statement.getIsExecuted()))
	 */
	private static boolean isValidStatement(Statement statement){
		return ((statement != null) && (statement.getIsExecuted()));
	}
	
	/**
	 * Check whether the given worm is a valid worm for any globalVariable.
	 * @param worm
	 *        The worm to check.
	 * @return True if and only true if the worm is an effective worm and 
	 *         the worm has an effective program.
	 */
	private static boolean isValidWorm(Worm worm){
		return ((worm !=null) && (worm.getProgram() != null));
	}
	
	/**
	 * Check whether the given globalVariableMap is a valid globalVariableMap for any globalVariable.
	 * @param globalVariableMap
	 *        The globalVariableMap to check.
	 * @return True if and only true if the globalVariableMap is an effective globalVariableMap.
	 */
	private static boolean isValidGlobalVariableMap(Map<String, Type> globalVariableMap){
		return (globalVariableMap != null);
	}
}
