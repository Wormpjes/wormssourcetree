package worms.model;


import java.util.HashSet;
import java.util.Set;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class of teams involving members, a name and a world.
 * 
 * @invar	This team has proper members.
 * 		  |	hasProperMembers()
 * @invar	This team has a proper world.
 * 		  |	hasProperWorld()
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class Team {
	
	/**
	 * Initialize this new team with given world and name.
	 * 
	 * @param 	world
	 * 			The world for this new team.
	 * @param 	name
	 * 			The name for this new team.
	 * @throws 	IllegalArgumentException
	 * 			An IllegalArgumentException is thrown if the name is not a valid name for this new team
	 * 			or if this team can not have the given world as its world.
	 * 		  |	(! isValidName(name)) || (! canHaveAsWorld(world))
	 */
	@Raw
	public Team(World world, String name) throws IllegalArgumentException {
		if (! isValidName(name))
			throw new IllegalArgumentException("Illegal name");
		this.name = name;
		if (! canHaveAsWorld(world)) {
			throw new IllegalArgumentException("Illegal world");}
		this.world = world;
		this.members = new HashSet<Worm>();
	}
	
	/**
	 * Check whether the given name is a valid name for this team.
	 * 
	 * @param 	name
	 * 			The name to check.
	 * @return	true if and only if the name starts with a capital letter 
	 * 			and contains only letters (both upper- and lowercase).
	 * 		  |	result == name.matches("^[A-Z]+[a-zA-Z]+$")
	 */
	protected static boolean isValidName(String name) {
		return (name.matches("^[A-Z]+[a-zA-Z]+$"));
	}
	
	/**
	 * Variable registering the name of this team.
	 */
	private final String name;	
	
	/**
	 * Return the name of this team.
	 */
	@Immutable
	protected final String getName() {
		return this.name;
	}	
	
	/**
	 * Return the set of all the members of this team.
	 */
	@Basic
	protected Set<Worm> getAllMembers() {
		return new HashSet<Worm>(this.members);
	}
	
	/**
	 * Return the set of all the living members of this team.
	 */
	protected Set<Worm> getAllLivingMembers() {
		Set<Worm> allLivingMembers = new HashSet<Worm>();
		for (Worm worm : getAllMembers())
			if (!worm.isTerminated())
				allLivingMembers.add(worm);
		return allLivingMembers;
		
	}
	
	/**
	 * Add the given worm to the set of members attached to this team.
	 * 
	 * @param 	worm
	 * 			The worm to be added.
	 * @effect	The team of the given worm is set to this team.
	 * 		  |	worm.setTeam(this)
	 * @throws 	IllegalArgumentException
	 * 			An IllegalArgumentException is thrown if this team can not have the given worm as one of its members
	 * 			or the given worm already has a team.
	 * 		  | (! canHaveAsMember(worm)) || (worm.getTeam() != null)
	 */
	protected void addAsMember(Worm worm) throws IllegalArgumentException{
		if (! canHaveAsMember(worm)) 
			throw new IllegalArgumentException("Bad worm");
		if (worm.getTeam() != null)
			throw new IllegalArgumentException("The given worm is already in other team.");
		this.members.add(worm);
		worm.setTeam(this);
	}
	
	/**
	 * Remove the given worm from the list of members of this team.
	 * 
	 * @param 	worm
	 * 			The worm to remove.
	 */
	protected void removeAsMember(Worm worm) {
		this.members.remove(worm);
	}
	
	/**
	 * Check whether this team can have the given worm as its member.
	 *
	 * @return	true if and only if the given worm is null
	 * 			or the given worm is in the same world as this team.
	 * 		  |	result == ((worm == null) || (worm.getWorld() == this.getWorld()))
	 */
	@Raw
	protected boolean canHaveAsMember(Worm worm) {
		return (worm == null) || (worm.getWorld() == this.getWorld()); 
	}
	
	/**
	 * Check whether this team has proper members.
	 *
	 * @return	true if and only if each member of this team can be a member of this team
	 * 			and each member has this team as its team.
	 * 		  |	result == (for each member in getMembers():
	 * 		  |				canHaveAsMember(member) && member.getTeam() == this
	 */
	@Model @Raw
	private boolean hasProperMembers() {
		for (Worm member : getAllMembers()) {
			if ( (! canHaveAsMember(member)) || (member.getTeam() != this))
				return false;
		}
		return true;
	}
	
	/**
	 * Check whether this team has the given worm as one of the worms attached to it.
	 * 
	 * @param worm
	 * 		  The worm to check
	 * @return
	 */
	@Basic @Raw
	protected boolean hasAsMember(Worm worm){
		return this.getAllMembers().contains(worm);
	}
	
	/**
	 * Variable registering the members of this team.
	 */
	private Set<Worm> members;

	/**
	 * Return the world of this team.
	 */
	@Basic @Immutable
	protected final World getWorld() {
		return world;
	}
	
	/**
	 * Check whether this team can have the world as its world.
	 *
	 * @param	world
	 * 			The world to check.
	 * @return	False if the given world is null.
	 * 		  | if (world == null)
	 * 		  |		then result == false
	 * 			False if the world does not yet have this team as a team.
	 * 			and the world already has the maximum number of teams allowed.
	 * 		  |	if ((!world.hasAsTeam(this)) && (world.getNbTeams() >= world.MAX_NB_TEAMS))
	 * 		  |		then result == false
	 * 			Otherwise false if the world already contains a different team
	 * 			with the same name as this team.
	 * 		  |	for each team in world.getAllTeams():
	 * 		  |		if (team.getName() == this.getName() && team != this)
	 * 		  |			then result == false
	 */
	@Raw
	protected boolean canHaveAsWorld(World world) {
		if (world == null)
			return false;
		if ((!world.hasAsTeam(this)) && (world.getNbTeams() >= world.MAX_NB_TEAMS))
			return false;
		for (Team team : world.getAllTeams()) {
			if (team.getName() == this.getName() && team != this)
				return false;
		}
		return true;
	}

	/**
	 * Check whether this team has a proper world.
	 *
	 * @return	true if and only if this team can have its world as a world
	 * 			and this team is in the list of all teams of that world.
	 * 		  |	result == ( canHaveAsWorld(getWorld())
	 *        |				&& ((World) getWorld()).getAllTeams().contains(this) )
	 */
	@Raw @Model
	private boolean hasProperWorld() {
		return canHaveAsWorld(getWorld())
				&& ((World) getWorld()).getAllTeams().contains(this);
	}
	
	/**
	 * Variable registering the world of this team.
	 */
	private final World world;


}
