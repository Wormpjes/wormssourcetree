package worms.model;

import java.util.*;

import worms.model.PointAmount.PointAmount;
import worms.model.Position.Position;
import worms.util.Util;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of worms involving a world, weapons, a team, a position, a direction, a radius and a name.
 * 
 * @invar The name of this worm is a valid name for a worm.
 * 		| isValidName(this.getName())
 * @invar The radius of this worm is a valid radius for a worm.
 * 		| isValidRadius(this.getRadius())
 * @invar The maximum amount of action points is equal to the worm's mass rounded to the nearest integer.
 * 		| this.getMaxAPs() == (int) Math.rint(getMass())
 * @invar The maximum amount of hitpoints is equal to the worm's mass rounded to the nearest integer.
 * 		| this.getMaxHPs() == (int) Math.rint(getMass())
 * @invar This worm has proper weapons.
 * 		| hasProperWeapons()
 * @invar This worm has a proper team.
 * 		| hasProperTeam()
 * 
 * @author Olivier Kamers and Christof Luyten 
 * 
 * @version 3.0 
 *
 */
public class Worm extends JumpableEntity {

	/**
	 * Initialize this new worm with given world, position, direction, radius and name.
	 * 
	 * @param	world
	 * 			The world for this new worm.
	 * @param 	position
	 *		  	The position for this new worm.
	 * @param 	direction
	 *        	The direction for this new worm.
	 * @pre   	The given direction must be a valid direction for a worm.
	 *        | isValidDirection(direction)
	 * @param 	radius
	 * 		  	The radius for this new worm.
	 * @param 	name
	 * 		  	The name for this new worm.
	 * @effect	This new worm is initialized as a new jumpable entity with a given world,
	 * 		    position, direction, radius and the constant density of a worm.  
	 * 	      | super(world, position, direction, radius, DENSITY)
	 * @effect	The name of this worm is set to the given name.
	 * 		  | setName(name)
	 * @effect	The amount of action points of this worm is set to the maximum amount of action points.
	 * 		  | setAPs(getMaxAPs())
	 * @effect	The amount of hitpoints of this worm is set to the maximum amount of hitpoints.
	 * 		  | setHPs(getMaxHPs())
	 * @effect	The worm is given two new weapons: a new bazooka and a new rifle.
	 * 	      |	Bazooka bazooka = new Bazooka();
	 * 		  |	Rifle rifle = new Rifle();
	 * 		  | addWeapon(bazooka);
	 * 		  | addWeapon(rifle);
	 * @effect	The first weapon is selected as active weapon for this new worm.
	 * 		  | selectNextWeapon()
	 * @effect	The worm is added to the active team of the given world, if that world has an active team.
	 * 		  |	if (world.getActiveTeam() != null)
	 * 		  | 	world.getActiveTeam().addAsMember(this);
	 */
	@Raw
	public Worm(World world, Position position, double direction, double radius, String name, Program program) throws IllegalArgumentException {
		super(world, position, direction, radius, DENSITY);
		setAPs(getMaxAPs());
		setHPs(getMaxHPs());
		setName(name);
		if (program != null) {
			this.program = program;
			program.setWorm(this);
		}
		Bazooka bazooka = new Bazooka();
		Rifle rifle = new Rifle();
		addWeapon(rifle);
		addWeapon(bazooka);
		selectNextWeapon();
		if (world.getActiveTeam() != null)
			world.getActiveTeam().addAsMember(this);		
	}
	
	/**
	 * Initialize this new worm in the given world at a random adjacent position with a random direction and radius and no program.
	 * The name is initialized to the number of the worm in the given world, starting at zero.
	 * 
	 * @param 	world
	 * 		  	The world for this new worm.
	 * @effect  The worm is initialized with the given world, a random (not necessarily adjacent) position
	 * 			in that world, a random direction, a random radius, 
	 * 			the number of the worm in the world as name,
	 * 			and no program.
	 * 		  | this(world,
	 * 		  | Position.randomPosition(world, Worm.getMinimalRadius()),
	 * 		  |	new Random().nextDouble() * 2.0 * Math.PI,
	 *		  |	Worm.getMinimalRadius() + random.nextDouble() / 5.0,
	 *		  |	"Worm ".concat(Integer.toString(1 + world.getCircularEntitiesOfType(Worm.class).size())),
	 *		  |	null)
	 * @effect	The worm's position is set to a valid adjacent position in the worm's world.
	 * 		  |	this.setPosition(world.getValidPosition(this))
	 */
	public Worm(World world){
		this(world,
			 Position.randomPosition(world, MINIMALRADIUS),
			 world.random.nextDouble() * 2.0 * Math.PI,
			 MINIMALRADIUS + world.random.nextDouble() / 5.0,
			 "Worm ".concat(Integer.toString(1 + world.getCircularEntitiesOfType(Worm.class).size()))
			 , null);
		this.setPosition(world.getValidPosition(this));
	}
	
	/**
	 * Initialize this new worm inside the given world and with the given program.
	 * 
	 * @param 	world
	 * 		  	The world for this new worm.
	 * @param 	program
	 * 			The program for this new worm.
	 * @effect	The worm is initialized inside the given world.
	 * 		  |	this(world)
	 * @effect	If the program is not null, then the executing worm of the program is set to this worm.
	 * 		  |	if (program != null)
	 * 		  |		then program.setWorm(this);
	 */
	public Worm(World world, Program program) {
		this(world);
		this.program = program;
		if (program != null)
			program.setWorm(this);
	}
	
	/**
	 * Constant registering the density of any worm.
	 */
	private final static double DENSITY = 1062;	
	
	/**
	 * Constant registering the maximum divergence.
	 */
	private final static double DIVERGENCELIMIT = 0.7875;
	
	/**
	 * Constant registering the divergence step.
	 */
	private final static double DIVERGENCESTEP = 0.0175;
	
	/**
	 * Constant registering the damage per meter when falling for any worm.
	 */
	private final static double FALLDAMAGE = 3.0;
	
	/**
	 * Constant registering the step of looking for new y-coordinates when falling.
	 */
	private final static double FALLSTEP = 0.1;
	
	/**
	 * Variable registering the minimal radius of this circular entity. For now this is equal to 0.25.
	 */
	public final static double MINIMALRADIUS = 0.25;
	
	/**
	 * Return the name of this worm.
	 */
	@Basic
	public String getName() {
		return name;
	}
	
	/**
	 * Set the name of this worm to a given name.
	 * 
	 * @param  name
	 * 		   The name to set.
	 * @throws IllegalArgumentException
	 * 		   An illegalArgumentException is thrown if the given name is not a valid name.
	 * 		 | ! isValidName(name)
	 * @post   The name of this worm is equal to the given name.
	 * 	     | new.getName() == name
	 */
	protected void setName(String name) throws IllegalArgumentException {
		if (! isValidName(name))
			throw new IllegalArgumentException("Invalid name.");
		this.name = name;
	}	
	
	/**
	 * Check whether a given name is a valid name for the worm.
	 * 
	 * @param  name
	 * 		   The name to check.
	 * @return true if and only if the name is longer than 2 characters, starts with a capital letter
	 * 		   and contains only valid characters: letters (both upper- and lowercase), numbers,
	 * 		   single quotes, double quotes and spaces.
	 * 		 | result == (name.matches("^[A-Z]+[a-zA-Z0-9\'\" ]+$"))		
	 */
	@Model
	private static boolean isValidName(String name) {
		return (name.matches("^[A-Z]+[a-zA-Z0-9\'\" ]+$"));
	}
	
	/**
	 * Variable registering the name of a worm.
	 */
	private String name;
	
	/**
	 * Return the team of this worm.
	 */
	@Basic
	protected Team getTeam() {
		return team;
	}

	/**
	 * Set the team to which this worm is attached to the given team.
	 * 
	 * @param team
	 *        The team to attach this worm.
	 */
	@Raw
	protected void setTeam(@Raw Team team) throws IllegalArgumentException {
		if (! canHaveAsTeam(team))
			throw new IllegalArgumentException("The worm can not have this team.");
		this.team = team;
	}

	
	/**
	 * Return the name of this worm's team or null if this worm is not part of a team.
	 * 
	 * @return 	If the team is not null, then the name of the team is returned.
	 * 		  |	if (team != null)
	 * 		  |		then return team.getName()
	 * 			Otherwise, it returns null.
	 * 		  |	return null
	 */
	public String getTeamName() {
		if (team != null)
			return team.getName();
		else
			return null;
	}
	
	/**
	 * Check whether this worm has a proper team.
	 *
	 * @result	true if and only if this worm can have the team as it's team 
	 * 			and the team is either null or has this worm as one of it's members.
	 * 		  | result ==  ( canHaveAsTeam(getTeam()) 
	 * 		  |				 && ((getTeam() == null) || (getTeam()).hasAsMember(this))) )
	 */
	@Model @Raw
	private boolean hasProperTeam() {
		return (canHaveAsTeam(getTeam()) && ((getTeam() == null) || (getTeam()).hasAsMember(this)));
	}
	
	/**
	 * Check whether this worm can have the team as its team.
	 *
	 * @result	true if and only if the team is null or the team can have this worm as a member.
	 * 		  |	result == (team == null || team.canHaveAsMember(this))
	 */
	@Raw
	protected boolean canHaveAsTeam(Team team) {
		return (team == null || team.canHaveAsMember(this));
	}
	
	/**
	 * Variable registering the team of this worm.
	 */
	private Team team = null;
	
	/**
	 * Return the amount of action points for this worm.
	 */
	@Basic
	public int getAPs() {
		if (this.APs == null)
			return 0;
		return APs.getValue();
	}
	
	/**
	 * Set the amount of action points to the given amount.
	 * 
	 * @param APs
	 * 		  The amount of action points to set.
	 * @post  The amount of action points of this worm is set to zero if the given amount is negative. 
	 * 		  After that the next turn is started, if this worm is the active worm in its world. 
	 * 		  | if (APs < 0) {
	 *		  | 	then new.getAPs() == 0;
	 *		  |		if (getWorld().getActiveWorm() == this)
	 *	      |			then getWorld().startNextTurn()	 
	 *			The amount of action points of this worm is set to the maximum amount of action points if
	 *			the given amount is larger than the maximum amount of action points.
	 *		  | else if (APs > getMaxAPs())
	 *		  | 	then new.getAPs() == getMaxAPs();
	 *			Otherwise the amount of action points of this worm is set to the given amount,
	 *			rounded up to the next integer.
	 *		  |	else 
	 *		  | 	then new.getAPs() == (int) Math.ceil(APs);
	 */
	private void setAPs(double APs) {
		if (APs <= 0) {
			this.APs = new PointAmount((int)APs);
			if (getWorld().getActiveWorm() == this)
				getWorld().startNextTurn();
		}
		else if (APs >= getMaxAPs())
			this.APs = new PointAmount(getMaxAPs());
		else
			this.APs = new PointAmount((int) Math.ceil(APs));
	}
	
	/**
	 * Variable registering the number of action points for this worm.
	 */
	private PointAmount APs;
	
	/**
	 * Return the maximum amount of action points for this worm.
	 * The maximum amount of action points is equal	to the mass of this worm rounded to the nearest integer.
	 * 
	 * @return	The mass of the worm rounded to the nearest integer.	
	 * 		  |	result == Math.round((float)getMass());
	 */
	public int getMaxAPs() {
		return Math.round((float)getMass());
	}
	
	/**
	 * Return the maximum number of action points, calculated with the given radius instead of the worm's current radius.
	 * 
	 * @param 	radius
	 * 			The radius for which the maximum number of action points is calculated.
	 * @return	The mass of this worm, calculated with the given radius, rounded to the nearest integer.
	 */
	private int getMaxAPs(double radius) {
		return Math.round((float)getMass(radius));
	}	
	
	/**
	 * Return the amount of hitpoints for this worm.
	 */
	@Basic
	public int getHPs() {
		if (this.HPs == null)
			return 0;
		return HPs.getValue();
	}
	
	/**
	 * Set the amount of hitpoints to the given amount.
	 * 
	 * @param 	HPs
	 * 		  	The amount of hitpoints to set.
	 * @post  	The worm is terminated if the amount of hitpoints is negative or equal to zero. 
	 * 		  |	if (HPs < 0)
	 *		  |		new.isTerminated() == true;
	 *	 		The amount of hitpoints of this worm is set to the maximum amount of action points if 
	 * 		  	the given amount is larger than or equal to the maximum amount of hitpoints.
	 *		  |	else if (HPs >= getMaxHPs())
	 *		  |		new.getHPs() == getMaxHPs();
	 *			Otherwise the amount of hitpoints of this worm is set to the given amount.
	 *		  | else 
	 *		  |		new.getHPs() == (int) Math.ceil(HPs);
	 */
	protected void setHPs(int HPs) {
		if (Util.fuzzyLessThanOrEqualTo(HPs, 0))
			this.terminate();
		else if (Util.fuzzyGreaterThanOrEqualTo(HPs, getMaxHPs()))
			this.HPs = new PointAmount(getMaxHPs());
		else
			this.HPs =  new PointAmount((int) Math.ceil(HPs));
	}
	
	/**
	 * Variable registering the number of hitpoints for this worm.
	 */
	private PointAmount HPs;
	
	/**
	 * Return the maximum amount of hitpoints for this worm. The maximum amount of hitpoints is equal
	 * 		to the mass rounded to the nearest integer.
	 * 
	 * @return	The mass of the worm rounded to the nearest integer.
	 * 		  | result == Math.round((float)getMass());
	 */
	public int getMaxHPs() {
		return Math.round((float)getMass());
	}	
	
	/**
	 * Return the maximum number of hitpoints, calculated with the given radius instead of the worm's current radius.
	 * 
	 * @param 	radius
	 * 			The radius for which the maximum number of hitpoints is calculated.
	 * @return	The mass of this worm, calculated with the given radius, rounded to the nearest integer.
	 */
	private int getMaxHPs(double radius) {
		return Math.round((float)getMass(radius));
	}
	
	/**
	 * Return the weapons of this worm.
	 */
	@Basic
	private ArrayList<Weapon> getAllWeapons() {
		return new ArrayList<Weapon>(weapons);
	}
	
	/**
	 * Add a weapon to this worm's arsenal.
	 * 
	 * @param 	weapon
	 * 			The weapon to add.
	 * @effect	This worm is added to the weapon.
	 * 		  |	weapon.addWorm(this)
	 * @post	The given weapon is added to the list of weapons of this worm.
	 * 		  |	new.weapons.contains(weapon)
	 * @throws 	IllegalArgumentException
	 * 			An IllegalArgumentException is thrown if this worm can not have 
	 *          the given weapon as one of its weapons or has the given weapon 
	 *          as one of its weapons.
	 * 		  |	! canHaveAsWeapon(weapon) || this.getAllWeapons().contains(weapon)
	 */
	private void addWeapon(Weapon weapon) throws IllegalArgumentException {
		if ((! canHaveAsWeapon(weapon)) || (this.getAllWeapons().contains(weapon)))
			throw new IllegalArgumentException();
		this.weapons.add(weapon);
		weapon.addWorm(this);
	}
	
	/**
	 * Checks if the worm can have the weapon as its weapon.
	 * 
	 * @param	weapon
	 * 		  	The weapon to check.
	 * @return	False if the given weapon is null
	 * 		  |	if (weapon == null)
	 * 		  |		then result == false
	 * 			Otherwise false if the worm currently owns a weapon of the same class.
	 * 		  |	for each weapon in getAllWeapons()
	 * 		  |		if (i.getClass() == weapon.getClass() && i != weapon)
	 * 		  |			then result == false
	 * 			Otherwise true
	 * 		  |	result == true
	 */
	@Raw
	protected boolean canHaveAsWeapon(Weapon weapon) {
		if (weapon == null)
			return false;
		for (Weapon i : getAllWeapons()) {
			if (i.getClass() == weapon.getClass() && i != weapon)
				return false;	
		 }
		return true;
	}
	
	/**
	 * Check whether this worm has proper weapons.
	 * 
	 * @return	true if and only if the worm can have each of its weapon as a weapon,
	 * 			and the worm of the weapon is equal to this worm.
	 * 		  |	result == ( for each weapon in getAllWeapons():
	 * 		  |					canHaveAsWeapon(weapon)  && weapon.getWorm() == this)
	 */
	@Model @Raw
	private boolean hasProperWeapons() {
		for (Weapon weapon : getAllWeapons()){
			if ( !canHaveAsWeapon(weapon) || weapon.getWorm() != this)
				return false;
		}
		return true;
	  }
	
	/**
	 * Check whether this worm has the given weapon as its weapon.
	 * 
	 * @param 	weapon
	 * 			The weapon to check.
	 * @return	true if and only if the list of weapons contains the given weapon.
	 * 		  |	result == this.getAllWeapons().contains(weapon)
	 */
	protected boolean hasAsWeapon(Weapon weapon) {
		return this.getAllWeapons().contains(weapon);
	}
	
	/**
	 * Variable registering the weapons of this worm.
	 */
	private List<Weapon> weapons = new ArrayList<Weapon>();
	
	/**
	 * Return the active weapon of this worm.
	 */
	@Basic
	public Weapon getActiveWeapon() {
		return activeWeapon;
	}
	
	/**
	 * Set the active weapon of this worm to the given weapon.
	 * 
	 * @param 	weapon
	 * 		  	The weapon to set.
	 * @post	The new active weapon is equal to the given weapon.
	 * 		  |	new.getActiveWeapon() == weapon
	 */
	private void setActiveWeapon(Weapon weapon) {
		this.activeWeapon = weapon;
	}
	
	/**
	 * Select the next weapon of this worm.
	 * 
	 * @post	If the current active weapon of this worm is null or the last weapon in the weaponlist,
	 * 		 	then the active weapon is set to the first weapon from the weaponlist.
	 * 	  	  |	if (getActiveWeapon() == null || getAllWeapons().indexOf(getActiveWeapon()) == getAllWeapons().size() - 1)
	 * 		  |		then setActiveWeapon(getAllWeapons().get(0))
	 * 			Otherwise the active weapon of this worm is set to the weapon from the weaponlist
	 * 			with as index the index of the current active weapon plus one.
	 * 		  |	setActiveWeapon(getAllWeapons().get(getAllWeapons().indexOf(activeWeapon) + 1))
	 */
	protected void selectNextWeapon() {
		if (getActiveWeapon() == null || getAllWeapons().indexOf(getActiveWeapon()) == getAllWeapons().size() - 1)
			setActiveWeapon(getAllWeapons().get(0));
		else
			setActiveWeapon(getAllWeapons().get(getAllWeapons().indexOf(activeWeapon) + 1));
	}
	
	/**
	 * Variable registering the active weapon of this worm.
	 */
	private Weapon activeWeapon = null;
	
	/**
	 * Return the program associated with this worm.
	 */
	@Basic
	protected Program getProgram() {
		return this.program;
	}
	
	/**
	 * Check whether this worm has a program.
	 * 
	 * @return true if and only if the program of this worm is not null.
	 * 		 |	result == (getProgram() != null)
	 */
	public boolean hasProgram() {
		return getProgram() != null;
	}
	
	/**
	 * Variable registering the program for this worm.
	 */
	private Program program;
	
	/**
	 * Set the radius of this worm to a given radius.
	 * 
	 * @param 	radius
	 * 			The radius to set.
	 * @throws 	IllegalArgumentException
	 * 		   	An illegalargumentexception is thrown if the given radius is not a valid radius for this worm.
	 * 		  |	! isValidRadius(radius)
	 * @effect	The super method setRadius is called with the argument radius
	 * 		  | super.setRadius(radius)
	 * @effect	If the current amount of action points is greater than the new maximum amount of action points,
	 * 			then the current amount of action points is set to the maximum amount.
	 * 		  | if (Util.fuzzyGreaterThanOrEqualTo(this.getAPs(), getMaxAPs(radius)))
	 * 		  |		this.setAPs(getMaxAPs(radius))
	 * @effect	If the current amount of hitpoints is greater than the new maximum amount of hitpoints,
	 * 			then the current amount of hitpoints is set to the maximum amount.
	 * 		  |	if (Util.fuzzyGreaterThanOrEqualTo(this.getHPs(), getMaxHPs(radius)))
	 * 		  |		this.setAPs(getMaxHPs(radius))
	 */
	@Override
	public void setRadius(double radius) throws IllegalArgumentException {
		if (! isValidRadius(radius))
			throw new IllegalArgumentException();
		else {
			super.setRadius(radius);
			if (Util.fuzzyGreaterThanOrEqualTo(this.getAPs(), getMaxAPs(radius)))
				this.setAPs(getMaxAPs(radius));
			if (Util.fuzzyGreaterThanOrEqualTo(this.getHPs(), getMaxHPs(radius)))
				this.setHPs(getMaxHPs(radius));
		}
	}
	
	/**
	 * Return the mass of this worm, calculated with the given radius instead of the worm's current radius.
	 * 
	 * @param 	radius
	 * 			The radius to use in the calculation of the mass.
	 * @return	The density of this worm multiplied with 4*pi/3 and the given radius cubed.
	 */
	private double getMass(double radius) {
		return (DENSITY * (4.0/3 * Math.PI * Math.pow(radius, 3)));
	}
	
	/**
	 * Check whether the given radius is a valid radius for this circular entity.
	 * 
	 * @param  radius
	 * 		   The radius to check.
	 * @return True if and only if the given radius is finite and larger than or equal to the minimal radius.
	 * 		 | result == (radius >= getMinimalRadius() 
	 * 		 |						&& radius < Double.POSITIVE_INFINITY 
	 * 		 |						&& radius > Double.NEGATIVE_INFINITY)
	 */
	@Model
	protected boolean isValidRadius(double radius) {
		return (Util.fuzzyGreaterThanOrEqualTo(radius, MINIMALRADIUS) &&
				radius < Double.POSITIVE_INFINITY &&
				radius > Double.NEGATIVE_INFINITY);
	}

	/**
	 * Start this worms turn by setting the action points to their maximum and adding 10 hitpoints.
	 * 
	 * @effect	The amount of action points of this worm is set to the maximum amount.
	 * 		  |	setAPs(getMaxAPs())
	 * @effect	The amount of hitpoints of this worm is raised by ten.
	 * 		  | setHPs(getHPs() + 10)
	 */
	protected void startTurn() {
		setAPs(getMaxAPs());
		setHPs(getHPs() + 10);
		if (getProgram() != null) 
			getProgram().execute();
	}
	
	/**
	 * Move the worm in the current direction.
	 * 
	 * @effect	If the new position after the move is not inside the world, then this worm is terminated.
	 * 		  |	if (! getWorld().isInsideWorld(getNewMovePosition(), getRadius()))
	 * 		  |		then terminate()
	 * @effect	The position is set to the new position after the move, if that position is inside the world.
	 * 		  |	setPosition(getNewMovePosition())
	 * @effect	The amount of action points is set to the current amount of action points lowered by the cost of the move.
	 * 		  |	setAPs(getAPs() - getMovingCost(getNewMovePosition()))
	 * @effect	The new position is checked for overlapping food rations.
	 * 		  |	checkForFood()
	 * @throws 	IllegalStateException 
	 * 			An IllegalStateException is thrown if the worm can not move.
	 * 		  |	! canMove()
	 */
	public void move() throws IllegalStateException {
		if (! canMove())
			throw new IllegalStateException();
		Position newPosition = getNewMovePosition();
		if (! getWorld().isInsideWorld(newPosition, getRadius()))
			terminate();
		else {
			setAPs(getAPs() - getMovingCost(newPosition));
			setPosition(newPosition);
			checkForFood();
		}
	}
	
	/**
	 * Calculate the cost of performing a possible move.
	 * 
	 * @param 	newPosition
	 * 		  	The new position after the possible move for which to calculate the cost.
	 * @return	The cost is equal to the absolute value of cosine of the direction to the new position plus
	 * 			the absolute value of four times the sine of this direction. 
	 * 		  |	Math.abs( Math.cos(direction) ) + Math.abs( 4.0 * Math.sin(direction) ) 
	 */
	private double getMovingCost(Position newPosition) {
		double direction = getPosition().getDirectionToOtherPosition(newPosition);
		return ( Math.abs( Math.cos(direction) ) + Math.abs( 4.0 * Math.sin(direction) ) );
	}
	
	/**
	 * Return the new position after a possible move.
	 * The new position is an adjacent position if one can be found and a passable position otherwise.
	 * If none of these can be found, the worm's current position is returned.
	 * 
	 * @return 	Returns a new adjacent position within a distance getRadius() of the current position,
	 * 			in a direction between getDirection +- DIVERGENCELIMIT, if one can be found.
	 * 		  |	getWorld().isAdjacent(result, getRadius()) == true
	 * 		  |		&& this.getPosition().getDistanceToOtherPosition(result) <= getRadius()
	 * 		  |		&& Math.abs(this.getPosition().getDirectionToOtherPosition(result) - getDirection()) <= DIVERGENCELIMIT 
	 * 			If no adjacent position can be found within those ranges, it returns a new passable position in the same ranges.
	 * 		  |	getWorld().isPassable(result, getRadius()) == true
	 * 		  |		&& this.getPosition().getDistanceToOtherPosition(result) <= getRadius()
	 * 		  |		&& Math.abs(this.getPosition().getDirectionToOtherPosition(result) - getDirection()) <= DIVERGENCELIMIT 
	 * 			Otherwise, if no adjacent or passable position can be found, it returns the worm's current position.
	 * 		  |	result.getX() == this.getX() && result.getY() == this.getY()
	 */
	private Position getNewMovePosition() {
		double optimalAdjacentDistance; double optimalPassableDistance;
		Position optimalAdjacentPosition; Position optimalPassablePosition;
		try {
			optimalAdjacentPosition = getNewAdjacentPosition(getDirection());
			optimalAdjacentDistance = optimalAdjacentPosition.getDistanceToOtherPosition(getPosition());
		} catch (NullPointerException exc) {
			optimalAdjacentPosition = new Position(getX(), getY());
			optimalAdjacentDistance = 0;
		}
		try {
			optimalPassablePosition = getNewPassablePosition(getDirection());
			optimalPassableDistance = optimalPassablePosition.getDistanceToOtherPosition(getPosition());
		} catch (NullPointerException exc) {
			optimalPassablePosition = new Position(getX(), getY());
			optimalPassableDistance = 0;
		}
		double optimalDivergence = Math.abs(getDirection() + DIVERGENCELIMIT);
		for ( double divergence = -DIVERGENCELIMIT; Util.fuzzyLessThanOrEqualTo(divergence, DIVERGENCELIMIT); divergence += DIVERGENCESTEP  ) {
			Position newAdjacentPosition = getNewAdjacentPosition(getDirection() + divergence);
			Position newPassablePosition = getNewPassablePosition(getDirection() + divergence);
			double newDivergence = Math.abs(getDirection() + divergence);
			if (newAdjacentPosition != null) {
				double newDistance = getPosition().getDistanceToOtherPosition(newAdjacentPosition);
				if ( Util.fuzzyGreaterThanOrEqualTo(newDistance, optimalAdjacentDistance) 
					&& Util.fuzzyGreaterThanOrEqualTo(newDistance, 0.1)
					&& (! Util.fuzzyLessThanOrEqualTo(newDivergence, optimalDivergence))) {
					optimalAdjacentDistance = newDistance;
					optimalDivergence = newDivergence;
					optimalAdjacentPosition = new Position(newAdjacentPosition.getX(), newAdjacentPosition.getY());
				}
			}
			if (newPassablePosition != null){
				double newDistance = getPosition().getDistanceToOtherPosition(newPassablePosition);
				if (! Util.fuzzyLessThanOrEqualTo(newDistance, optimalPassableDistance) 
						&& Util.fuzzyGreaterThanOrEqualTo(newDistance, 0.1)) {
					optimalPassableDistance = newDistance;
					optimalPassablePosition = new Position(newPassablePosition.getX(), newPassablePosition.getY());
				}
			}
		}
		if ( (optimalAdjacentPosition != null )
				&& Util.fuzzyGreaterThanOrEqualTo(getPosition().getDistanceToOtherPosition(optimalAdjacentPosition), 0.1))
			return optimalAdjacentPosition;
		else
			return optimalPassablePosition;
	}
	
	/**
	 * Return the adjacent position farthest away from the current position in the direction of the given angle.
	 * If no adjacent position can be found, it returns null.
	 * 
	 * @param 	direction
	 * 			The direction in which to find the furthest adjacent position.
	 * @return 	A new position that is adjacent to impassable terrain and at an angle 'direction' from the worm's current position.
	 * 			The new position's distance from the current position is in the range [0.1, getRadius()]
	 * 		  |	getWorld.isAdjacent(result, getRadius())
	 * 		  |		&& getPosition().getDirectionToOtherPosition(result) == direction
	 * 		  |		&& getPosition().getDistanceToOtherPosition(result) >= 0.1
	 * 		  |		&& getPosition().getDistanceToOtherPosition(result) <= getRadius()
	 * 			Returns null if no such position can be found.
	 * 		  |	result == null
	 */
	private Position getNewAdjacentPosition(double direction) {
		for (double newDistance = getRadius(); Util.fuzzyGreaterThanOrEqualTo(newDistance, 0.1); newDistance -= getDistanceStep()) {
			Position newPosition = new Position(getX() + newDistance * Math.cos(direction), getY() + newDistance * Math.sin(direction));
			if (getWorld().isAdjacent(newPosition, getRadius())){
				return newPosition;
			}
		}
		return null; 
	}
	
	/**
	 * Return the passable position farthest away from the current position in the direction of the given angle.
	 * If no adjacent position can be found, it returns null.
	 * 
	 * @param 	direction
	 * 			The direction in which to find the furthest passable position.
	 * @return 	A new position that is passable and at an angle 'direction' from the worm's current position.
	 * 			The new position's distance from the current position is in the range [0.1, getRadius()]
	 * 		  |	getWorld.isPassable(result, getRadius())
	 * 		  |		&& getPosition().getDirectionToOtherPosition(result) == direction
	 * 		  |		&& getPosition().getDistanceToOtherPosition(result) >= 0.1
	 * 		  |		&& getPosition().getDistanceToOtherPosition(result) <= getRadius()
	 * 			Returns null if no such position can be found.
	 * 		  |	result == null
	 */
	private Position getNewPassablePosition(double direction) {
		for (double newDistance = getRadius(); Util.fuzzyGreaterThanOrEqualTo(newDistance, 0.1); newDistance -= getDistanceStep()) {
			Position newPosition = new Position(getX() + newDistance * Math.cos(direction), getY() + newDistance * Math.sin(direction));
			if (getWorld().isPassable(newPosition, getRadius()))
				return newPosition;
		}
		return null;
	}
	
	/**
	 * Return the step size for changing the distance when trying to find a new position.
	 * 
	 * @return 	Either the width or the height of a pixel in this worm's world divided by two,
	 * 			depending on which is the smallest.
	 * 		  |	Math.min(getWorld().pixelHeight() / 2.0 , getWorld().pixelWidth() / 2.0)
	 */
	private double getDistanceStep() {
		return Math.min(Math.min(getWorld().pixelHeight() / 2.0 , getWorld().pixelWidth() / 2.0), getRadius());
	}
	
	/**
	 * Check whether this worm can move.
	 * 
	 * @return True if and only if this worm is not terminated 
	 * 		   and the current amount of action points is greater than or equal to the cost of the move.
	 * 		 | (! isTerminated()) && (Util.fuzzyGreaterThanOrEqualTo(getAPs(), getMovingCost(getNewMovePosition())))
	 */
	public boolean canMove() {
		return (! isTerminated()) && (Util.fuzzyGreaterThanOrEqualTo(getAPs(), getMovingCost(getNewMovePosition())));
	}
	
	/**
	 * Turn this worm over a given angle.
	 * 
	 * @param angle
	 * 		  The angle to add to the current direction of this worm.
	 * @pre	  The worm can turn over the given angle.
	 * 		| canTurn(angle)
	 * @post  The new direction of this worm is equal to its old direction plus the given angle. 
	 * 		  If the sum is negative, two pi is added. Otherwise it is calculated modulo two pi 
	 *        to prevent the sum from being greater than or equal to two pi.
	 * 		| if ( (getDirection() + angle) < 0 )
	 * 		|	new.getDirection() == (this.getDirection() + angle) % (2*Math.PI) + (2*Math.PI)
	 * 		| else
	 * 		|   new.getDirection() == (this.getDirection() + angle) % (2*Math.PI)
	 */
	public void turn(double angle) {
		assert canTurn(angle);
		if ((getDirection() + angle)<0 )
			setDirection((getDirection() + angle) % (2*Math.PI) + (2*Math.PI));
		else
			setDirection(((getDirection() + angle) % (2*Math.PI)));
		setAPs(getAPs() - (Math.ceil(Math.abs(angle)*60/(2*Math.PI))));
	}
	
	/**
	 * Check whether this worm can turn over this given angle.
	 * 
	 * @param  angle
	 * 		   The angle to check.
	 * @return True if and only if the angle is between negative two pi and two pi and the cost of this turn is less than or equal to
	 * 		   the current amount of action points of this worm. The cost of this turn is equal to
	 * 		   sixty times the absolute value of the angle divided by two pi, and rounded up to the next integer.
	 * 		|  result == ((Math.abs(angle) < 2*Math.PI) && 
	 *		|			  (int) Math.ceil(60*Math.abs(angle)/(2*Math.PI)) <= getAPs())
	 */
	public boolean canTurn(double angle) {
		return ((Math.abs(angle) < 2*Math.PI) && 
				(int) Math.ceil(60*Math.abs(angle)/(2*Math.PI)) <= getAPs());
	}
	
	/**
	 * Let the worm jump.
	 * 
	 * @param 	timeStep 
	 * 		  	An elementary time interval during which is assumed
	 *        	that the worm will not completely move through a piece of impassable terrain. 
	 * @effect	The method jump of the superclass JumpableEntity is called with the given argument timeStep.
	 * 		  |	super.jump(timeStep)
	 * @effect	The current amount of action points of this worm is set to zero.
	 * 		  | setAPs(0)
	 * @effect	The worm checks for food at the new position.
	 * 		  |	checkForFood()
	 * @throws 	IllegalStateException
	 * 			An IllegalStateException is thrown if the worm can not jump.
	 * 		  |	! canJump()
	 */
	@Override
	public void jump(double timeStep) throws IllegalStateException  {
		if (! canJump())
			throw new IllegalStateException("This worm can not jump.");
		super.jump(timeStep);
		setAPs(0);
		checkForFood();
	}
	
	/**
	 * Check whether the given position is a valid ending position for a jump.
	 * 
	 * @param	position
	 * 			The position to check.
	 * @return	true if and only if the distance between the current position and the given position 
	 * 			is greater than or equal to the worm's radius and the new position is an adjacent position,
	 * 			or if the new position is impassable,
	 * 			or if the new position is not inside the world.
	 * 		  |	result == 	(Util.fuzzyGreaterThanOrEqualTo(position.getDistanceToOtherPosition(getPosition()), getRadius()) 
	 *		  |				&& (getWorld().isAdjacent(position, getRadius()) 
	 *		  |				|| getWorld().isImpassable(position, getRadius()) 
	 *		  |				|| ! getWorld().isInsideWorld(position, getRadius()) ))
	 */
	protected boolean isValidEndPosition(Position position) {
		return (Util.fuzzyGreaterThanOrEqualTo(position.getDistanceToOtherPosition(getPosition()), getRadius()) 
				&& (getWorld().isAdjacent(position, getRadius()) 
					|| getWorld().isImpassable(position, getRadius()) 
					|| ! getWorld().isInsideWorld(position, getRadius())
					));
	}
	
	/**
	 * Check whether the worm can jump.
	 * 
	 * @return 	True if an only if the worm's number of action points is greater than zero
	 * 			and the worm is not located on impassable terrain.
	 * 		 | result == (! Util.fuzzyLessThanOrEqualTo(getAPs(), 0)
	 * 		 |				&& getWorld().isPassable(getPosition(), getRadius()) )
	 */
	@Model
	public boolean canJump() {
		return (! Util.fuzzyLessThanOrEqualTo(getAPs(), 0)
				&& getWorld().isPassable(getPosition(), getRadius()));
	}

	/**
	 * Return the force of a jump.
	 * 	The force is equal to five times the amount of action points 
	 *	plus the mass of this worm multiplied with the gravitational constant. 
	 * 
	 * @return (5*getAPs()) + (getMass() * G)
	 */
	protected double getForce() {
		return (5*getAPs()) + (getMass() * G);
	}
	
	/**
	 * Shoot a projectile with the current active weapon and the given propulsion yield.
	 * 
	 * @param 	propulsionYield
	 * 		  	The propulsion yield to use for the shot.
	 * @effect	The method shoot from the active weapon is called with the given propulsion yield.
	 * 		  |	getActiveWeapon().shoot(propulsionYield)
	 * @effect	The amount of action points is lowered by the shooting cost of the active weapon.
	 * 		  |	setAPs(getAPs() - getActiveWeapon().getShootingCost())
	 * @throws	IllegalStateException
	 * 			An illegal state exception is thrown if the worm can not shoot.
	 * 		  |	! canShoot()
	 */
	protected void shoot(int propulsionYield) throws IllegalStateException{
		if (! canShoot())
			throw new IllegalStateException();
		getActiveWeapon().shoot(propulsionYield);
		setAPs(getAPs() - getActiveWeapon().getShootingCost());
	}
	
	/**
	 * Check whether this worm can shoot it's current active weapon.
	 * 
	 * @return 	True if and only if the cost of shooting the active weapon is
	 * 		   	less than or equal to this worm's current amount of action points,
	 * 			and the worm is located on passable terrain.
	 * 		  | result == ( Util.fuzzyLessThanOrEqualTo(getActiveWeapon().getShootingCost(), getAPs())
	 * 		  |				&& getWorld().isPassable(getPosition(), getRadius()) )
	 */
	protected boolean canShoot() {
		return Util.fuzzyLessThanOrEqualTo(getActiveWeapon().getShootingCost(), getAPs())
				&& getWorld().isPassable(getPosition(), getRadius());
	}
	
	/**
	 * Let the worm fall until it is back at adjacent ground or until it leaves the game world after which it is terminated.
	 * 
	 * @post	The worm's new position is equal to the position after the fall if it's a valid position.
	 * 		  |	new.getPosition() == findFallingPosition(getPosition())
	 * @post	The worm is terminated if the position after the fall is not inside the world.
	 * 		  |	if (! getWorld().isInsideWorld(newPosition, getRadius()))
	 * 		  |		then terminate()
	 * @effect	The worms new position is set to the position after the fall.
	 * 		  |	setPosition(findFallingPosition(new Position(getX(),getY())))
	 * @effect	The hitpoints are lowered by the damage of the fall multiplied by the distance of the fall,
	 * 			rounded down to the previous integer.
	 * 		  |	setHPs(getHPs() - (int) Math.floor(FALLDAMAGE*distance))
	 * @effect	The method checkForFood is called to see if the worm has to eat a piece of food.
	 * 		  |	checkForFood()
	 * @throws 	IllegalStateException
	 * 		 	An illegal state exception is thrown if the worm can not fall.
	 * 		  |	! canFall()
	 */
	protected void fall() throws IllegalStateException {
		if (! canFall())
			throw new IllegalStateException("This worm can not fall.");
		Position newPosition = findFallingPosition(new Position(getX(),getY())); 
		if (! getWorld().isInsideWorld(newPosition, getRadius())){
			this.terminate();
		}
		else {
			double distance = getPosition().getDistanceToOtherPosition(newPosition);
			setPosition(newPosition);
			setHPs(getHPs() - (int) Math.floor(FALLDAMAGE*distance));
			checkForFood();
		}
	}
	
	/**
	 * Check whether the worm can fall.
	 * 
	 * @return	true if and only if the worm's current position is inside the world but
	 * 			not adjacent to impassable terrain, not impassable
	 * 			and the worm is not terminated.
	 * 		  |	result == (! getWorld().isAdjacent(getPosition(), getRadius())) 
	 * 		  |				&& ! getWorld().isImpassable(getPosition(), getRadius())
	 * 		  |				&& getWorld().isInsideWorld(getPosition(), getRadius()) 
	 * 		  |				&& (! this.isTerminated())
	 */
	protected boolean canFall() {
		return (! getWorld().isAdjacent(getPosition(), getRadius()))
				&& ! getWorld().isImpassable(getPosition(), getRadius())
				&& getWorld().isInsideWorld(getPosition(), getRadius())
				&& (! this.isTerminated());
	}
	
	/**
	 * Return the new position after a fall.
	 * 
	 * @param 	position
	 * 			The position from which the fall started.
	 * @return	The position with the same x coordinate and a new y coordinate
	 * 			so that the position is adjacent to impassable terrain.
	 * 		  |	for newY in range [position.getY(), -getRadius()]
	 * 		  |		if (getWorld().isAdjacent(position, getRadius()))
	 * 		  |			then return position
	 * @return	If no such position is found in that range then the returned position is outside the world.
	 * 		  |	return position.
	 */
	private Position findFallingPosition(Position position) {
		for (double newY = position.getY() ; Util.fuzzyGreaterThanOrEqualTo(newY, -getRadius()); newY -= FALLSTEP*getRadius() ) {
			position = new Position(position.getX(),newY);
			if (getWorld().isAdjacent(position, getRadius()))
				return position;
		}
		return position;
	} 	
	
	/**
	 * Check if the current location contains food and eat that food.
	 * 
	 * @post	All food rations of the worm's world that are overlapping with the worm are eaten.
	 * 			A food ration overlaps with the worm if the distance between both instances
	 * 			is less than of equal to the sum of their radiuses.
	 * 		  |	for each food in getWorld().getCircularEntitiesOfType(Food.class)
	 * 		  |	if ( this.getPosition().getDistanceToOtherPosition(food.getPosition()) <= food.getRadius() + this.getRadius())
	 * 		  |		then eatFood(food)
	 */
	private void checkForFood() {
		for (Food food:this.getWorld().getCircularEntitiesOfType(Food.class))
			if (Util.fuzzyLessThanOrEqualTo(
					this.getPosition().getDistanceToOtherPosition(food.getPosition()), 
					food.getRadius() + this.getRadius()) )
				eatFood(food); 
	}
	
	/**
	 * Eat the given food.
	 * 
	 * @param 	food
	 * 			The food to eat.
	 * @effect	The radius of the worm is set to the current radius 
	 * 			plus the radius multiplied by the growth-factor of the food.
	 * 		  |	setRadius( getRadius() * (1 + food.getGrowthFactor()) );
	 * @effect	The food is terminated.
	 * 		  |	food.terminate()
	 * @post	The radius of this worm is raised by the radius multiplied by the growth factor of the food.
	 * 		  |	new.getRadius() == this.getRadius() * (1 + food.getGrowthFactor())
	 * @post	The food is terminated.
	 * 		  |	food.isTerminated() == true
	 */
	private void eatFood(Food food) {
		setRadius( getRadius() * (1 + food.getGrowthFactor()) );
		food.terminate();
	}
	
	/**
	 * Return a string representation of this worm.
	 * 
	 * @return	"Worm [name=" + getName() + ", team=" + getTeamName() + "]"
	 */
	@Override
	public String toString() {
		return "Worm [name=" + getName() + ", team=" + getTeamName() + "]";
	}


}