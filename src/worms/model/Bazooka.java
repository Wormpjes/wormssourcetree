package worms.model;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class of bazookas involving a name, damage, mass, initial ammount of ammo and shootingcost.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class Bazooka extends Weapon {
	
	/**
	 * Initialize this new bazooka.
	 * 
	 * @effect	This new bazooka is initialized as a new weapon with the mass and initial ammunition of a bazooka.
	 * 		  |	super(MASS, INITIALAMMO)
	 */
	public Bazooka() {
		super(MASS, INITIALAMMO);
	}	
	
	/**
	 * Constant registering the mass of any bazooka.
	 */
	private static final double MASS = 0.3;
	
	/**
	 * Constant registering the initial amount of ammo for any bazooka.
	 */
	private static final double INITIALAMMO = Double.POSITIVE_INFINITY;
	
	/**
	 * Variable registering the name of this bazooka.
	 */
	private final String name = "Bazooka";
	
	/**
	 * Constant registering the damage a projectile shot from this bazooka can do.
	 */
	private int damage = 80;
	
	/**
	 * Constant registering the cost of shooting a bazooka.
	 */
	private double shootingcost = 50;
	
	/**
	 * Return the damage that a projectile shot from this bazooka can do.
	 */
	@Override @Basic @Immutable
	public int getDamage() {
		return this.damage;
	}
	
	/**
	 * Return the cost of shooting a bazooka.
	 */
	@Override @Basic @Immutable
	public double getShootingCost() {
		return shootingcost;
	}

	/**
	 * Return the name of this bazooka.
	 */
	@Override @Basic @Immutable
	public String getName() {
		return name;
	}

	/**
	 * Shoot this bazooka with the given propulsion yield.
	 * 
	 * @effect	A new projectile is initialized with this bazooka as its weapon and the given propulsion yield.
	 * 		  | Projectile projectile = new Projectile(this, propulsionYield)
	 * @effect	The new projectile is set as the active projectile in the world of the worm that owns this bazooka.
	 * 		  |	getWorm().getWorld().setActiveProjectile(projectile)
	 * @effect	The ammunition of this bazooka is lowered by 1.
	 * 		  |	setAmmunition(getAmmunition() - 1)
	 */
	@Override
	protected void shoot(int propulsionYield) throws IllegalArgumentException {
		Projectile projectile = new Projectile(this, propulsionYield);
		getWorm().getWorld().setActiveProjectile(projectile);
		setAmmunition(getAmmunition() - 1);
	}

	/**
	 * Return the force for shooting a projectile from this bazooka.
	 * 
	 * @param propulsionYield
	 * 			The propulsion yield that is used for the shot.
	 * @return	A linear interpolation between 2.5 and 9.5, with the propulsion yield as weight.
	 * 		  |	2.5 + propulsionYield/100*7.0
	 */
	@Override
	protected double getForce(int propulsionYield) {
		return 2.5 + ((double) propulsionYield)/100*7.0;
	}

}