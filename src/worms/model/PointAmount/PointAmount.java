package worms.model.PointAmount;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class of PointAmounts involving a value.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
@Value
public class PointAmount {
	
	/**
	 * Initialize this new PointAmount with the given value.
	 * 
	 * @param 	value
	 * 			The value for this new PointAmount.
	 * @post  	if the given value is less than or equal to zero,
	 * 			then the value is set to zero.
	 * 		  |	if (value < 0)
	 * 		  |		then this.getValue() == 0
	 * 			otherwise the value of this new PointAmount is set to the given value.
	 * 		  |	this.getValue() = value
	 * 
	 */
	public PointAmount(int value) {
		if (value < 0)
			value = 0;
		this.value = value;
	}
	
	/**
	 * Variable registering the value of this PointAmount.
	 */
	private final int value;

	/**
	 * Return the value of this PointAmount.
	 */
	@Basic @Immutable
	public int getValue() {
		return value;
	}

	/**
	 * Return the hashcode for this PointAmount object.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	/**
	 * Check whether this PointAmount is equal to the given other object.
	 * 
	 * @return	true, if the given object is equal to this object.
	 * 		  |	if (this == obj)
	 * 		  |		then result == true
	 * 			false, if the given object is null.
	 * 		  |	if (obj == null)
	 * 		  |		then result == false
	 * 		    false, if the class of the given object is different from this class.
	 * 		  |	if (getClass() != obj.getClass())
	 * 		  |		then result == false
	 * 			otherwise true if and only if the value of this pointamount is equal to the value
	 * 			of the given other pointamount.
	 * 		  |	result == (value == other.value)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PointAmount other = (PointAmount) obj;
		return (value == other.value);
	}



}
