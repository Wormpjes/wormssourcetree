package worms.model;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class of rifles involving a name, damage, mass, initial ammount of ammo and shootingcost.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class Rifle extends Weapon {
	
	/**
	 * Initialize this new rifle.
	 * 
	 * @effect	This new rifle is initialized as a new weapon with the mass and initial ammunition of a rifle.
	 * 		  |	super(MASS, INITIALAMMO)
	 */
	public Rifle() {
		super(MASS, INITIALAMMO);
	}
	
	/**
	 * Constant registering the mass of any rifle.
	 */
	private static final double MASS = 0.01;
	
	/**
	 * Constant registering the initial amount of ammo for any rifle.
	 */
	private static final double INITIALAMMO = Double.POSITIVE_INFINITY;
	
	/**
	 * Variable registering the name of this rifle.
	 */
	private final String name = "Rifle";
	
	/**
	 * Constant registering the damage a projectile shot from this rifle can do.
	 */
	private int damage = 20;
	
	/**
	 * Constant registering the cost of shooting a rifle.
	 */
	private double shootingcost = 10;

	/**
	 * Return the damage that a projectile shot from this rifle can do.
	 */
	@Override @Basic @Immutable
	public int getDamage() {
		return this.damage;
	}
	
	
	
	/**
	 * Return the cost of shooting a rifle.
	 */
	@Override @Basic @Immutable
	public double getShootingCost() {
		return this.shootingcost;
	}

	/**
	 * Return the name of this rifle.
	 */
	@Override @Basic @Immutable
	public String getName() {
		return name;
	}
	
	/**
	 * Shoot this rifle with the given propulsion yield.
	 * 
	 * @effect	A new projectile is initialized with this rifle as its weapon and a propulsion yield of zero.
	 * 		  | Projectile projectile = new Projectile(this, 0)
	 * @effect	The new projectile is set as the active projectile in the world of the worm that owns this rifle.
	 * 		  |	getWorm().getWorld().setActiveProjectile(projectile)
	 * @effect	The ammunition of this rifle is lowered by 1.
	 * 		  |	setAmmunition(getAmmunition() - 1)
	 */
	@Override
	protected void shoot(int propulsionYield) throws IllegalArgumentException {
		Projectile projectile = new Projectile(this, 0);
		getWorm().getWorld().setActiveProjectile(projectile);
		setAmmunition(getAmmunition() - 1);
	}

	/**
	 * Return the force for shooting a projectile from this rifle.
	 * 
	 * @return	This is always equal to 1.5
	 * 		  |	result == 1.5
	 */
	@Override
	protected double getForce(int propulsionYield) {
		return 1.5;
	}

}