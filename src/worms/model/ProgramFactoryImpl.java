package worms.model;

import java.util.LinkedList;
import java.util.List;

import be.kuleuven.cs.som.annotate.*;
import worms.gui.game.IActionHandler;
import worms.model.Exceptions.FailedExecutionException;
import worms.model.Exceptions.IllegalTypeException;
import worms.model.Types.*;
import worms.model.programs.*;

/**
 * A class of program factories involving a handler and GlobalVariable.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class ProgramFactoryImpl implements
		ProgramFactory<Expression<? extends Type>, Statement, Type> {

	/**
	 * Initialize this new program factory with the given action handler.
	 * 
	 * @param handler
	 * 			The actionhandler to use in this factory.
	 */
	public ProgramFactoryImpl(IActionHandler handler) throws IllegalArgumentException {
		if(! isValidHandler(handler))
			throw new IllegalArgumentException();
		this.handler = handler;
	}
	
	/**
	 * Variable registering the action handler user for executing statements in
	 * this factory.
	 */
	private final IActionHandler handler;
	
	/**
	 * Variable registering the GlobalVariable of this programfactory.
	 */
	private GlobalVariable globalVariable;

	/**
	 * Return the GlobalVariable used in this programfactory.
	 */
	@Basic
	protected GlobalVariable getGlobalVariable() {
		return this.globalVariable;
	}

	/**
	 * Set the GlobalVariable of this programfactory to the given GlobalVariable.
	 * 
	 * @param GlobalVariable
	 *            The GlobalVariable to set.
	 */
	protected void setGlobalVariable(GlobalVariable GlobalVariable) throws IllegalArgumentException {
		if (!isValidGlobalVariable(GlobalVariable))
			throw new IllegalArgumentException();
		this.globalVariable = GlobalVariable;
	}
	
	/**
	 * Return the action handler used in this program factory.
	 */
	@Basic
	private IActionHandler getHandler() {
		return this.handler;
	}

	/* expressions */

	/**
	 * Create an expression representing a double literal, with value d
	 * 
	 * @return	An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createDoubleLiteral(int line, int column, final double d) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return The value of this expression is a DoubleType with value d.
			 * 		 |	return new Expression<DoubleType>()
			 */
			@Override
			public DoubleType getValue() {
				return new DoubleType(d);
			}
		};
	}

	/**
	 * Create an expression representing a boolean literal, with value b
	 * 
	 * @return	An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createBooleanLiteral(int line, int column, final boolean b) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return The value of this expression is a BooleanType with value b.
			 * 		 |	return new BooleanType(b)
			 */
			@Override
			public BooleanType getValue() {
				return new BooleanType(b);
			}
		};
	}

	/**
	 * Create an expression representing the logical and operation on two
	 * expressions e1 and e2
	 * 
	 * @return	An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createAnd(int line, int column, final Expression<? extends Type> e1,
			final Expression<? extends Type> e2) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	The value is a new BooleanType with value true if the conditions in e1 and e2 are true,
			 * 			and a booleantype with value false otherwise.
			 * 		  |	if ((boolean) e1.getValue().getValue() && (boolean) e2.getValue().getValue())
			 * 		  |		then return new BooleanType(true)
			 * 		  |	else then return new BooleanType(false)
			 * @throws	IllegalTypeException
			 * 		 	An IllegalTypeException is thrown if the value of e1 or e2 is not a BooleanType.
			 * 		  | ! (e1.getValue() instanceof BooleanType && e2.getValue() instanceof BooleanType)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if (! (e1.getValue() instanceof BooleanType && e2.getValue() instanceof BooleanType))
					throw new IllegalTypeException(
							"The arguments must be exressions containing a BooleanType.");
				if ((boolean) e1.getValue().getValue()
						&& (boolean) e2.getValue().getValue())
					return new BooleanType(true);
				return new BooleanType(false);
			}
		};
	}

	/**
	 * Create an expression representing the logical or operation on two
	 * expressions e1 and e2
	 * 
	 * @return	An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createOr(int line, int column, final Expression<? extends Type> e1,
			final Expression<? extends Type> e2) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new booleantype with value true if the value of the booleantypes stored in e1 or e2 is true,
			 * 			and a booleantype with value false otherwise.
			 * 		  |	if (boolean) e1.getValue().getValue() || (boolean) e2.getValue().getValue())
			 * 		  |		then return new BooleanType(true)
			 * 		  |	else then return new BooleanType(false)
			 * @throws	IllegalTypeException
			 * 		 	An IllegalTypeException is thrown if the value of e1 or e2 is not a BooleanType.
			 * 		  | ! (e1.getValue() instanceof BooleanType && e2.getValue() instanceof BooleanType)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if (! (e1.getValue() instanceof BooleanType && e2.getValue() instanceof BooleanType))
					throw new IllegalTypeException(
							"The arguments must be exressions containing a BooleanType.");
				if ((boolean) e1.getValue().getValue()
						|| (boolean) e2.getValue().getValue())
					return new BooleanType(true);
				return new BooleanType(false);
			}
		};
	}

	/**
	 * Create an expression representing the logical not operation on the
	 * expression e
	 * 
	 * @return	An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createNot(int line, int column, final Expression<? extends Type> e) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new booleantype with value true if the value of the booleantype stored in e is false,
			 * 			and a booleantype with value false otherwise.
			 * 		  |	if (! (boolean) e.getValue().getValue())
			 * 		  |		then return new BooleanType(true)
			 * 		  |	else then return new BooleanType(false)
			 * @throws	IllegalTypeException
			 * 		 	An IllegalTypeException is thrown if the value of e is not a BooleanType.
			 * 		  | ! (e.getValue() instanceof BooleanType)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if (! (e.getValue() instanceof BooleanType))
					throw new IllegalTypeException(
							"The arguments must be exressions containing a BooleanType.");
				if (! (boolean) e.getValue().getValue())
					return new BooleanType(true);
				return new BooleanType(false);
			}
		};
	}

	/**
	 * Create an expression representing the literal 'null'
	 * 
	 * @return An expression instantiated with Type.
	 * 		  |	return new Expression<Type>()
	 */
	@Override
	public Expression<? extends Type> createNull(int line, int column) {
		return new Expression<Type>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	Always null.
			 * 		  |	result == null
			 */
			@Override
			public Type getValue() {
				return null;
			}
		};
	}

	/**
	 * Create an expression representing a reference to the worm that is
	 * executing the program
	 * 
	 * @return An expression instantiated with EntityType.
	 * 		  |	return new Expression<EntityType>()
	 */
	@Override
	public Expression<? extends Type> createSelf(int line, int column) {
		return new Expression<EntityType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new EntityType with as value the worm that is executing this program.
			 * 		  |	return new EntityType(getGlobalVariable().getWorm())
			 */
			@Override
			public EntityType getValue() {
				return new EntityType(getGlobalVariable().getWorm());
			}
		};
	}

	/**
	 * Create an expression to get the x-coordinate of the entity identified by
	 * the expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createGetX(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType with as value the x-coordinate of the entity  identified by the expression e.
			 * 		  |	return new DoubleType(((EntityType) e.getValue()).getValue().getX())
			 * @throws	An IllegalTypeException is thrown if the value of the given entity e is not an EntityType.
			 * 		  |	! (e.getValue() instanceof EntityType)
			 */
			@Override
			public DoubleType getValue() throws IllegalTypeException {
				if (! (e.getValue() instanceof EntityType))
					throw new IllegalTypeException(
							"The expression must be an entity");
				return new DoubleType(((EntityType) e.getValue()).getValue()
						.getX());
			}
		};
	}

	/**
	 * Create an expression to get the y-coordinate of the entity identified by
	 * the expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createGetY(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType with as value the y-coordinate of the entity  identified by the expression e.
			 * 		  |	return new DoubleType(((EntityType) e.getValue()).getValue().getY())
			 * @throws	An IllegalTypeException is thrown if the value of the given entity e is not an EntityType.
			 * 		  |	! (e.getValue() instanceof EntityType)
			 */
			@Override
			public DoubleType getValue() throws IllegalTypeException {
				if (! (e.getValue() instanceof EntityType))
					throw new IllegalTypeException(
							"The expression must be an entity");
				return new DoubleType(((EntityType) e.getValue()).getValue()
						.getY());
			}
		};
	}

	/**
	 * Create an expression to get the radius of the entity identified by the
	 * expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createGetRadius(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType with as value the radius of the entity identified by the expression e.
			 * 		  |	return new DoubleType(((EntityType) e.getValue()).getValue().getRadius())
			 * @throws	An IllegalTypeException is thrown if the value of the given entity e is not an EntityType.
			 * 		  |	! (e.getValue() instanceof EntityType)
			 */
			@Override
			public DoubleType getValue() throws IllegalTypeException {
				if (! (e.getValue() instanceof EntityType))
					throw new IllegalTypeException(
							"The expression must be an entity");
				return new DoubleType(((EntityType) e.getValue()).getValue()
						.getRadius());
			}
		};
	}

	/**
	 * Create an expression to get the direction of the entity identified by the
	 * expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createGetDir(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType with as value the direction of the entity identified by the expression e.
			 * 		  |	return new DoubleType(((JumpableEntity) e.getValue().getValue()).getDirection())
			 * @throws	An IllegalTypeException is thrown if the value of the type identified by the given expression e
			 * 			is not a worm or a projectile.
			 * 		  |	! (e.getValue().getValue() instanceof Worm || e.getValue().getValue() instanceof Projectile)
			 */
			@Override
			public DoubleType getValue() throws IllegalTypeException {
				if (! (e.getValue().getValue() instanceof Worm || e.getValue()
						.getValue() instanceof Projectile))
					throw new IllegalTypeException(
							"The expression must be a jumpable entity");
				return new DoubleType(((JumpableEntity) e.getValue().getValue()).getDirection());
			}
		};
	}

	/**
	 * Create an expression to get the action points of the entity identified by
	 * the expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createGetAP(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType with as value the amount of action points of the entity identified by the expression e.
			 * 		  |	return new DoubleType((double) ((Worm) e.getValue().getValue()).getAPs());
			 * @throws	An IllegalTypeException is thrown if the value of the type identified by the given expression e
			 * 			is not a worm.
			 * 		  |	! (e.getValue().getValue() instanceof Worm)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e.getValue().getValue() instanceof Worm))
					throw new IllegalTypeException(
							"The value of the given expression must be a worm.");
				return new DoubleType(
						(double) ((Worm) e.getValue().getValue()).getAPs());
			}
		};
	}

	/**
	 * Create an expression to get the maximum number of action points of the
	 * entity identified by the expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createGetMaxAP(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType with as value the maximum amount of action points of the entity identified by the expression e.
			 * 		  |	return new DoubleType((double) ((Worm) e.getValue().getValue()).getMaxAPs());
			 * @throws	An IllegalTypeException is thrown if the value of the type identified by the given expression e
			 * 			is not a worm.
			 * 		  |	! (e.getValue().getValue() instanceof Worm)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e.getValue().getValue() instanceof Worm))
					throw new IllegalTypeException(
							"The value of the given expression must be a worm.");
				return new DoubleType(
						(double) ((Worm) e.getValue().getValue()).getMaxAPs());
			}
		};
	}

	/**
	 * Create an expression to get the hit points of the entity identified by
	 * the expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createGetHP(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType with as value amount of hit points of the entity identified by the expression e.
			 * 		  |	return new DoubleType((double) ((Worm) e.getValue().getValue()).getHPs());
			 * @throws	An IllegalTypeException is thrown if the value of the type identified by the given expression e
			 * 			is not a worm.
			 * 		  |	! (e.getValue().getValue() instanceof Worm)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e.getValue().getValue() instanceof Worm))
					throw new IllegalTypeException(
							"The value of the given expression must be a worm.");
				return new DoubleType(
						(double) ((Worm) e.getValue().getValue()).getHPs());
			}
		};
	}

	/**
	 * Create an expression to get the maximum number of hit points of the
	 * entity identified by the expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createGetMaxHP(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType with as value the maximum amount of hit points of the entity identified by the expression e.
			 * 		  |	return new DoubleType((double) ((Worm) e.getValue().getValue()).getMaxHPs());
			 * @throws	An IllegalTypeException is thrown if the value of the type identified by the given expression e
			 * 			is not a worm.
			 * 		  |	! (e.getValue().getValue() instanceof Worm)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e.getValue().getValue() instanceof Worm))
					throw new IllegalTypeException(
							"The value of the given expression must be a worm.");
				return new DoubleType(
						(double) ((Worm) e.getValue().getValue()).getMaxHPs());
			}
		};
	}

	/**
	 * Create an expression to evaluate whether the worm identified by the
	 * expression e belongs to the same team as the worm that is executing the
	 * program
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createSameTeam(int line, int column, final Expression<? extends Type> e) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A BooleanType with value true if the given expression cointains a null value,
			 * 			or if the worm that is executing the program is null.
			 * 		  |	if (e.getValue() == null || getGlobalVariable().getWorm() == null)
			 * 		  |		then return new BooleanType(true)
			 * 			A BooleanType with value false if one or both of the worms are not in a team.
			 * 		  | if (((Worm) e.getValue().getValue()).getTeam() == null || getGlobalVariable().getWorm().getTeam() == null)
			 * 		  |		then return new BooleanType(false)
			 * 			Otherwise a BooleanType with value true if and only if the name of the team of
			 * 			the worm that is executing the program is equal to the name of the team
			 * 			of the worm identified by the given expression.
			 * 		  |	return new BooleanType(((Worm) e.getValue().getValue()).getTeam().getName() 
			 * 		  |							== getGlobalVariable().getWorm().getTeam().getName())
			 * @throws	IllegalTypeException
			 * 			An IllegalTypeException is thrown if the given expression does not refer to a worm.
			 * 		  |	! (e.getValue().getValue() instanceof Worm)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if (e.getValue() == null || getGlobalVariable().getWorm() == null) {
					return new BooleanType(true);
				}
				if (! (e.getValue().getValue() instanceof Worm))
					throw new IllegalTypeException(
							"The given expression must refer to a worm");
				if (((Worm) e.getValue().getValue()).getTeam() == null
						|| getGlobalVariable().getWorm().getTeam() == null)
					return new BooleanType(false);
				return new BooleanType(((Worm) e.getValue().getValue())
						.getTeam().getName() == getGlobalVariable().getWorm().getTeam()
						.getName());
			}
		};
	}

	/**
	 * Create an expression to get the closest object in the direction theta+e,
	 * starting from the position of the worm that is executing the program,
	 * where theta is the current direction of the worm that is executing the
	 * program
	 * 
	 * @return An expression instantiated with EntityType.
	 * 		  |	return new Expression<EntityType>()
	 */
	@Override
	public Expression<? extends Type> createSearchObj(int line, int column, final Expression<? extends Type> e) {
		return new Expression<EntityType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return 	A new EntityType with as value the closest object in the direction theta + e,
			 * 			starting from the position of the worm that is executing the program.
			 * 		  |	new EntityType(getGlobalVariable().getWorm().getWorld().findCircularEntity(
			 *		  |			getGlobalVariable().getWorm().getPosition(),
			 *		  |			(getGlobalVariable().getWorm().getDirection() + (double) e.getValue().getValue() % (2.0 * Math.PI)));
			 * @throws 	IllegalTypeException
			 * 			An IllegalTypeException is thrown if the given expression e does not identify a DoubleType.
			 * 		  |	! (e.getValue() instanceof DoubleType)
			 */
			@Override
			public EntityType getValue() throws IllegalTypeException {
				if (! (e.getValue() instanceof DoubleType))
					throw new IllegalTypeException(
							"The given expression must hold a value of the type DoubleType");
				return new EntityType(getGlobalVariable().getWorm().getWorld()
						.findCircularEntity(
								getGlobalVariable().getWorm().getPosition(),
								(getGlobalVariable().getWorm().getDirection() + (double) e
										.getValue().getValue())
										% (2.0 * Math.PI)));
			}
		};
	}

	/**
	 * Create an expression that evaluates whether the entity identified by the
	 * expression e is a worm
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createIsWorm(int line, int column, final Expression<? extends Type> e) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	If the value of the given expression is null, then the result is a new BooleanType with value false.
			 * 		  |	if (e.getValue() == null)
			 * 		  |		then return new BooleanType(false)
			 * 			Otherwise the result is a new BooleanType with value true if and only if the value of the type identified
			 * 			by the given expression is a worm.
			 * 		  |	return new BooleanType(e.getValue().getValue() instanceof Worm)
			 */
			@Override
			public BooleanType getValue() {
				if (e.getValue() == null)
					return new BooleanType(false);
				return new BooleanType(e.getValue().getValue() instanceof Worm);
			}
		};
	}

	/**
	 * Create an expression that evaluates whether the entity identified by the
	 * expression e is a food ration
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createIsFood(int line, int column, final Expression<? extends Type> e) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	If the value of the given expression is null, then the result is a new BooleanType with value false.
			 * 		  |	if (e.getValue() == null)
			 * 		  |		then return new BooleanType(false)
			 * 			Otherwise the result is a new BooleanType with value true if and only if the value of the type identified
			 * 			by the given expression is a food ration.
			 * 		  |	return new BooleanType(e.getValue().getValue() instanceof Food)
			 */
			@Override
			public BooleanType getValue() {
				if (e.getValue() == null)
					return new BooleanType(false);
				return new BooleanType(e.getValue().getValue() instanceof Food);
			}
		};
	}

	/**
	 * Create an expression that evaluates to the value of the variable with the
	 * given name
	 * 
	 * @return An expression instantiated with Type.
	 * 		  |	return new Expression<Type>()
	 */
	@Override
	public Expression<? extends Type> createVariableAccess(final int line, final int column,
			final String name) {
		return new Expression<Type>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	The value in the map of global variables in the GlobalVariable associated with this factory,
			 * 			with as key the given name.
			 * 		  |	getGlobalVariable().getGlobalVariableMap().get(name)
			 */
			@Override
			public Type getValue() {
				return getGlobalVariable().getGlobalVariableMap().get(name);
			}
		};
	}

	/**
	 * OPTIONAL METHOD This method is only relevant for students that choose to
	 * work on static type checking. You may ignore this method if you want, and
	 * only implement the version of this method without the type argument. - If
	 * you do not use this method, return null. - Otherwise, return null from
	 * the other createVariableAccess method.
	 * 
	 * Create an expression that evaluates to the value of the variable with the
	 * given name.
	 * 
	 * The given type is the type of the variable with the given name, as
	 * determined while parsing the variable declarations in the program.
	 * 
	 * If the variable with the given name was not declared, the given type is
	 * null.
	 * 
	 * @return This method always returns null.
	 * 		 | result == null
	 */
	@Override
	public Expression<? extends Type> createVariableAccess(final int line, final int column,
			final String name, final Type type) {
		return null;
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * less than the value of the expression e2
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createLessThan(int line, int column, final Expression<? extends Type> e1,
			final Expression<? extends Type> e2) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new BooleanType with value true if and only if invoking the method compareTo
			 * 			of the value of e1 to the value of e2 returns -1.
			 * 		  | if ((e1.getValue()).compareTo(e2.getValue()) == -1)
			 * 		  |		then return new BooleanType(true)
			 * 		  |	else then return new BooleanType(false)
			 */
			@Override
			public BooleanType getValue() {
				if ((e1.getValue()).compareTo(e2.getValue()) == -1)
					return new BooleanType(true);
				return new BooleanType(false);
			}
		};
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * greater than the value of the expression e2
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createGreaterThan(int line, int column,
			final Expression<? extends Type> e1, final Expression<? extends Type> e2) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new BooleanType with value true if and only if invoking the method compareTo
			 * 			of the value of e1 to the value of e2 returns 1.
			 * 		  | if ((e1.getValue()).compareTo(e2.getValue()) == 1)
			 * 		  |		then return new BooleanType(true)
			 * 		  |	else then return new BooleanType(false)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if ((e1.getValue()).compareTo(e2.getValue()) == 1)
					return new BooleanType(true);
				return new BooleanType(false);
			}
		};
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * less than or equal to the value of the expression e2
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createLessThanOrEqualTo(int line, int column,
			final Expression<? extends Type> e1, final Expression<? extends Type> e2) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new BooleanType with value false if and only if invoking the method compareTo
			 * 			of the value of e1 to the value of e2 returns 1.
			 * 		  | if ((e1.getValue()).compareTo(e2.getValue()) == 1)
			 * 		  |		then return new BooleanType(false)
			 * 		  |	else then return new BooleanType(true)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if ((e1.getValue()).compareTo(e2.getValue()) == 1)
					return new BooleanType(false);
				return new BooleanType(true);
			}
		};
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * greater than or equal to the value of the expression e2
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createGreaterThanOrEqualTo(int line, int column,
			final Expression<? extends Type> e1, final Expression<? extends Type> e2) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new BooleanType with value false if and only if invoking the method compareTo
			 * 			of the value of e1 to the value of e2 returns -1.
			 * 		  | if ((e1.getValue()).compareTo(e2.getValue()) == -1)
			 * 		  |		then return new BooleanType(false)
			 * 		  |	else then return new BooleanType(true)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if ((e1.getValue()).compareTo(e2.getValue()) == -1)
					return new BooleanType(false);
				return new BooleanType(true);
			}
		};
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * equal to the value of the expression e2
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createEquality(int line, int column, final Expression<? extends Type> e1,
			final Expression<? extends Type> e2) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	If the values of both expressions are null,
			 * 			then a new BooleanType with value true is returned.
			 * 		  |	if (e1.getValue() == null && e2.getValue() == null)
			 * 		  |		then return new BooleanType(true)
			 * 			If the value of either e1 or e2 is null,
			 * 			then a new BooleanType with value false is returned.
			 * 		  |	if (e1.getValue() == null || e2.getValue() == null)
			 * 		  |		then return new BooleanType(false)
			 * 			Otherwise a BooleanType with value true is returned if and only if
			 * 			invoking the method compareTo of the value of e1 to the value of e2 returns 0.
			 * 		  | if (e1.getValue()).compareTo(e2.getValue()) == 0)
			 * 		  |		then return new BooleanType(true)
			 * 		  |	else then return new BooleanType(false)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if (e1.getValue() == null && e2.getValue() == null)
					return new BooleanType(true);
				if (e1.getValue() == null || e2.getValue() == null)
					return new BooleanType(false);
				if ((e1.getValue()).compareTo(e2.getValue()) == 0)
					return new BooleanType(true);
				return new BooleanType(false);
			}
		};
	}

	/**
	 * Create an expression that checks whether the value of expression e1 is
	 * not equal to the value of the expression e2
	 * 
	 * @return An expression instantiated with BooleanType.
	 * 		  |	return new Expression<BooleanType>()
	 */
	@Override
	public Expression<? extends Type> createInequality(int line, int column,
			final Expression<? extends Type> e1, final Expression<? extends Type> e2) {
		return new Expression<BooleanType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	If the values of both expressions are null,
			 * 			then a new BooleanType with value false is returned.
			 * 		  |	if (e1.getValue() == null && e2.getValue() == null)
			 * 		  |		then return new BooleanType(false)
			 * 			If the value of either e1 or e2 is null,
			 * 			then a new BooleanType with value true is returned.
			 * 		  |	if (e1.getValue() == null || e2.getValue() == null)
			 * 		  |		then return new BooleanType(true)
			 * 			Otherwise a BooleanType with value true is returned if and only if
			 * 			invoking the method compareTo of the value of e1 to the value of e2  does not return 0.
			 * 		  | if (e1.getValue()).compareTo(e2.getValue()) != 0)
			 * 		  |		then return new BooleanType(true)
			 * 		  |	else then return new BooleanType(false)
			 */
			@Override
			public BooleanType getValue() throws IllegalTypeException {
				if (e1.getValue() == null && e2.getValue() == null)
					return new BooleanType(false);
				if (e1.getValue() == null || e2.getValue() == null)
					return new BooleanType(true);
				if ((e1.getValue()).compareTo(e2.getValue()) != 0)
					return new BooleanType(true);
				return new BooleanType(false);
			}
		};
	}

	/**
	 * Create an expression that represents the addition of the value of
	 * expression e1 and the value of the expression e2
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createAdd(int line, int column, final Expression<? extends Type> e1,
			final Expression<? extends Type> e2) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType is returned with as value the addition of the values of the DoubleTypes
			 * 			identified by e1 and e2.
			 * 		  |	return new DoubleType((Double) e1.getValue().getValue() + (Double) e2.getValue().getValue())
			 * @throws 	IllegalTypeException
			 * 			An IllegalTypeException is thrown if e1 or e2 does not identify a DoubleType.
			 * 		  |	! (e1.getValue() instanceof DoubleType && e2.getValue() instanceof DoubleType)
			 */
			@Override
			public DoubleType getValue() throws IllegalTypeException {
				if (! (e1.getValue() instanceof DoubleType && e2.getValue() instanceof DoubleType))
					throw new IllegalTypeException(
							"The arguments must be Expressions containing a Double");
				return new DoubleType((Double) e1.getValue().getValue()
						+ (Double) e2.getValue().getValue());
			}
		};
	}

	/**
	 * Create an expression that represents the subtraction of the value of
	 * expression e1 and the value of the expression e2
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createSubtraction(int line, int column,
			final Expression<? extends Type> e1, final Expression<? extends Type> e2) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType is returned with as value the subtraction of the values of the DoubleTypes
			 * 			identified by e1 and e2.
			 * 		  |	return new DoubleType((Double) e1.getValue().getValue() - (Double) e2.getValue().getValue())
			 * @throws 	IllegalTypeException
			 * 			An IllegalTypeException is thrown if e1 or e2 does not identify a DoubleType.
			 * 		  |	! (e1.getValue() instanceof DoubleType && e2.getValue() instanceof DoubleType)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e1.getValue() instanceof DoubleType && e2.getValue() instanceof DoubleType))
					throw new IllegalTypeException(
							"The arguments must be Expressions containing a Double");
				return new DoubleType((Double) e1.getValue().getValue()
						- (Double) e2.getValue().getValue());
			}
		};
	}

	/**
	 * Create an expression that represents the multiplication of the value of
	 * expression e1 and the value of the expression e2
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createMul(int line, int column, final Expression<? extends Type> e1,
			final Expression<? extends Type> e2) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType is returned with as value the multiplication of the values of the DoubleTypes
			 * 			identified by e1 and e2.
			 * 		  |	return new DoubleType((Double) e1.getValue().getValue() * (Double) e2.getValue().getValue())
			 * @throws 	IllegalTypeException
			 * 			An IllegalTypeException is thrown if e1 or e2 does not identify a DoubleType.
			 * 		  |	! (e1.getValue() instanceof DoubleType && e2.getValue() instanceof DoubleType)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e1.getValue() instanceof DoubleType && e2.getValue() instanceof DoubleType))
					throw new IllegalTypeException(
							"The arguments must be Expressions containing a Double");
				return new DoubleType((Double) e1.getValue().getValue()
						* (Double) e2.getValue().getValue());
			}
		};
	}

	/**
	 * Create an expression that represents the division of the value of
	 * expression e1 and the value of the expression e2
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createDivision(int line, int column, final Expression<? extends Type> e1,
			final Expression<? extends Type> e2) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType is returned with as value the division of the values of the DoubleTypes
			 * 			identified by e1 and e2.
			 * 		  |	return new DoubleType((Double) e1.getValue().getValue() / (Double) e2.getValue().getValue())
			 * @throws 	IllegalTypeException
			 * 			An IllegalTypeException is thrown if e1 or e2 does not identify a DoubleType.
			 * 		  |	! (e1.getValue() instanceof DoubleType && e2.getValue() instanceof DoubleType)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e1.getValue() instanceof DoubleType && e2.getValue() instanceof DoubleType))
					throw new IllegalTypeException(
							"The arguments must be Expressions containing a Double");
				return new DoubleType((Double) e1.getValue().getValue()
						/ (Double) e2.getValue().getValue());
			}
		};
	}

	/**
	 * Create an expression that represents the square root of the value of
	 * expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createSqrt(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType is returned with as value the square root of the value of the DoubleType
			 * 			identified by e.
			 * 		  |	return new DoubleType(Math.sqrt((Double) e.getValue().getValue()))
			 * @throws 	IllegalTypeException
			 * 			An IllegalTypeException is thrown if e does not identify a DoubleType.
			 * 		  |	! (e.getValue() instanceof DoubleType)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e.getValue() instanceof DoubleType))
					throw new IllegalTypeException(
							"The expression must be an Expression containing a Double");
				return new DoubleType(Math.sqrt((Double) e.getValue().getValue()));
			}
		};
	}

	/**
	 * Create an expression that represents the sine of the value of expression
	 * e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createSin(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType is returned with as value the sine of the value of the DoubleType
			 * 			identified by e.
			 * 		  |	return new DoubleType(Math.sin((Double) e.getValue().getValue()))
			 * @throws 	IllegalTypeException
			 * 			An IllegalTypeException is thrown if e does not identify a DoubleType.
			 * 		  |	! (e.getValue() instanceof DoubleType)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e.getValue() instanceof DoubleType))
					throw new IllegalTypeException(
							"The expression must be a DoubleConstantExpression");
				return new DoubleType(
						Math.sin((Double) e.getValue().getValue()));
			}
		};
	}

	/**
	 * Create an expression that represents the cosine of the value of
	 * expression e
	 * 
	 * @return An expression instantiated with DoubleType.
	 * 		  |	return new Expression<DoubleType>()
	 */
	@Override
	public Expression<? extends Type> createCos(int line, int column, final Expression<? extends Type> e) {
		return new Expression<DoubleType>() {
			
			/**
			 * Return the value of this expression.
			 * 
			 * @return	A new DoubleType is returned with as value the cosine of the value of the DoubleType
			 * 			identified by e.
			 * 		  |	return new DoubleType(Math.cos((Double) e.getValue().getValue()))
			 * @throws 	IllegalTypeException
			 * 			An IllegalTypeException is thrown if e does not identify a DoubleType.
			 * 		  |	! (e.getValue() instanceof DoubleType)
			 */
			@Override
			public DoubleType getValue() {
				if (! (e.getValue() instanceof DoubleType))
					throw new IllegalTypeException(
							"The expression must be a DoubleConstantExpression");
				return new DoubleType(
						Math.cos((Double) e.getValue().getValue()));
			}
		};
	}

	/* actions */

	/**
	 * Create a statement that represents a turn of the worm executing the
	 * program by the value of the angle expression
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createTurn(final int line, final int column,
			final Expression<? extends Type> angle) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement was not executed yet, then it it calls the method turn on the handler.
			 * 		  |	if (! this.getIsExecuted())
			 * 		  |		then getHandler().turn(getGlobalVariable().getWorm(), (Double) angle.getValue().getValue())
			 * 			If the handler returns true, then the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	if (getHandler().turn(getGlobalVariable().getWorm(), (Double) angle.getValue().getValue()))
			 * 		  |		then { 	this.setIsExecuted(true)
			 * 		  |				getGlobalVariable().addStatement(this) } 
			 * @throws	IllegalTypeException
			 * 			An IllegalTypeException is thrown if the expression angle does not identify a DoubleType.
			 * 		  |	! (angle.getValue() instanceof DoubleType)
			 * @throws	FailedExecutionException
			 * 			A FailedExecutionException is thrown if invoking the turn method on the action handler returns false.
			 * 		 |  ! getHandler().turn(getGlobalVariable().getWorm(), (Double) angle.getValue().getValue())
			 */
			@Override
			public void execute() throws IllegalTypeException, FailedExecutionException {
				if (!this.getIsExecuted()) {
					if (! (angle.getValue() instanceof DoubleType))
						throw new IllegalTypeException(
								"The given angle must be a DoubleConstantExpression");
					if (!getHandler().turn(getGlobalVariable().getWorm(),
							(Double) angle.getValue().getValue())) {
						throw new FailedExecutionException(this);
					} else {
						this.setIsExecuted(true);
					    getGlobalVariable().addStatement(this);
					}
				}
			}

			/**
			 * Check whether this statement contains an actionstatement.
			 * 
			 * @return Always true.
			 * 		 | result == true
			 */
			@Override
			public boolean containsActionStatement() {
				return true;
			}
		};
	}

	/**
	 * Create a statement that represents a move of the worm executing the
	 * program
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createMove(int line, int column) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement was not executed yet, then it it calls the method move on the handler.
			 * 		  |	if (! this.getIsExecuted())
			 * 		  |		then getHandler().move(getGlobalVariable().getWorm())
			 * 			If the handler returns true, then the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	if (getHandler().move(getGlobalVariable().getWorm()))
			 * 		  |		then { 	this.setIsExecuted(true)
			 * 		  |				getGlobalVariable().addStatement(this) } 
 			 * @throws	FailedExecutionException
			 * 			A FailedExecutionException is thrown if invoking the move method on the action handler returns false.
			 * 		 | ! getHandler().move(getGlobalVariable().getWorm())
			 */
			@Override
			public void execute() {
				if (!this.getIsExecuted()) {
					if (!getHandler().move(getGlobalVariable().getWorm()))
						throw new FailedExecutionException(this);
					else{
						this.setIsExecuted(true);
					    getGlobalVariable().addStatement(this);
			    	}
				}
			}

			/**
			 * Check whether this statement contains an actionstatement.
			 * 
			 * @return Always true.
			 * 		 | result == true
			 */
			@Override
			public boolean containsActionStatement() {
				return true;
			}
		};
	}

	/**
	 * Create a statement that represents a jump of the worm executing the
	 * program
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createJump(int line, int column) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement was not executed yet, then it it calls the method jump on the handler.
			 * 		  |	if (! this.getIsExecuted())
			 * 		  |		then getHandler().jump(getGlobalVariable().getWorm())
			 * 			If the handler returns true, then the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	if (getHandler().jump(getGlobalVariable().getWorm()))
			 * 		  |		then { 	this.setIsExecuted(true)
			 * 		  |				getGlobalVariable().addStatement(this) } 
			 * @throws	FailedExecutionException
			 * 			A FailedExecutionException is thrown if invoking the jump method on the action handler returns false.
			 * 		 | ! getHandler().jump(getGlobalVariable().getWorm())
			 */
			@Override
			public void execute() {
				if (!this.getIsExecuted()) {
					if (!getHandler().jump(getGlobalVariable().getWorm()))
						throw new FailedExecutionException(this);
					else {
						this.setIsExecuted(true);
					    getGlobalVariable().addStatement(this);
					}
				}
			}

			/**
			 * Check whether this statement contains an actionstatement.
			 * 
			 * @return Always true.
			 * 		 | result == true
			 */
			@Override
			public boolean containsActionStatement() {
				return true;
			}
		};
	}

	/**
	 * Create a statement that represents toggling the weapon of the worm
	 * executing the program
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createToggleWeap(int line, int column) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement was not executed yet, then it it calls the method toggleWeapon on the handler.
			 * 		  |	if (! this.getIsExecuted())
			 * 		  |		then getHandler().toggleWeapon(getGlobalVariable().getWorm())
			 * 			If the handler returns true, then the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	if (getHandler().toggleWeapon(getGlobalVariable().getWorm()))
			 * 		  |		then { 	this.setIsExecuted(true)
			 * 		  |				getGlobalVariable().addStatement(this) }  
			 * @throws	FailedExecutionException
			 * 			A FailedExecutionException is thrown if invoking the toggleWeapon method on the action handler returns false.
			 * 		 | ! getHandler().toggleWeapon(getGlobalVariable().getWorm())
			 */
			@Override
			public void execute() {
				if (!this.getIsExecuted()) {
					if (!getHandler().toggleWeapon(getGlobalVariable().getWorm()))
						throw new FailedExecutionException(this);
					else {
						this.setIsExecuted(true);
						getGlobalVariable().addStatement(this);
					}
				}
			}

			/**
			 * Check whether this statement contains an actionstatement.
			 * 
			 * @return Always true.
			 * 		 | result == true
			 */
			@Override
			public boolean containsActionStatement() {
				return true;
			}
		};
	}

	/**
	 * Create a statement that represents firing the current weapon of the worm
	 * executing the program, where the propulsion yield is given by the yield
	 * expression
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createFire(int line, int column, final Expression<? extends Type> yield) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 *  
			 * @effect	If this statement was not executed yet, then it it calls the method fire on the handler.
			 * 		  |	if (! this.getIsExecuted())
			 * 		  |		then getHandler().fire(getGlobalVariable().getWorm(),(int) Math.floor((Double) yield.getValue().getValue()))
			 * 			If the handler returns true, then the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	if (getHandler().fire(getGlobalVariable().getWorm(),(int) Math.floor((Double) yield.getValue().getValue()))
			 * 		  |		then { 	this.setIsExecuted(true)
			 * 		  |				getGlobalVariable().addStatement(this) }  
			 * @throws	IllegalTypeException
			 * 			An IllegalTypeException is thrown if the expression yield does not identify a DoubleType.
			 * 		  |	! (yield.getValue() instanceof DoubleType)
			 * @throws	FailedExecutionException
			 * 			A FailedExecutionException is thrown if invoking the fire method on the action handler returns false.
			 * 		 |  ! getHandler().fire(getGlobalVariable().getWorm(), (int) Math.floor((Double) yield.getValue().getValue()))
			 */
			@Override
			public void execute() {
				if (!this.getIsExecuted()) {
					if (! (yield.getValue() instanceof DoubleType))
						throw new IllegalTypeException(
								"The yield must be a DoubleConstantExpression");
					if (!getHandler().fire(getGlobalVariable().getWorm(),
							(int) Math.floor((Double) yield.getValue().getValue())))
						throw new FailedExecutionException(this);
					else {
						this.setIsExecuted(true);
						getGlobalVariable().addStatement(this);
					}
				}
			}

			/**
			 * Check whether this statement contains an actionstatement.
			 * 
			 * @return Always true.
			 * 		 | result == true
			 */
			@Override
			public boolean containsActionStatement() {
				return true;
			}
		};
	}

	/**
	 * Create a statement that represents no action of a worm
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createSkip(int line, int column) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * The execution of this statement does nothing.
			 */
			@Override
			public void execute() {
				// Do nothing
			}

			/**
			 * Check whether this statement contains an actionstatement.
			 * 
			 * @return Always true.
			 * 		 | result == true
			 */
			@Override
			public boolean containsActionStatement() {
				return true;
			}
		};
	}

	/* other statements */

	/**
	 * Create a statement that represents the assignment of the value of the rhs
	 * expression to a variable with the given name
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createAssignment(final int line, final int column,
			final String variable, final Expression<? extends Type> rhs) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement is not executed yet, then it adds the value of the rhs expression
			 * 			to the map getGlobalVariableMap() in the GlobalVariable, with as key the given string variable.
			 * 			The flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	if (! this.getIsExecuted())
			 * 		  |		then { 	getGlobalVariable().getGlobalVariableMap().put(variable, rhs.getValue())
			 * 		  |				this.setIsExecuted(true)
			 *		  |				getGlobalVariable().addStatement(this) }
			 */
			@Override
			public void execute() {
				if (!this.getIsExecuted()) {
					getGlobalVariable().getGlobalVariableMap().put(variable, rhs.getValue());
					this.setIsExecuted(true);
					getGlobalVariable().addStatement(this);
				}
			}
		};
	}

	/**
	 * Create a statement that represents the conditional execution of the
	 * statements then or otherwise, depending on the value of the condition
	 * expression
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createIf(int line, int column, final Expression<? extends Type> condition,
			final Statement then, final Statement otherwise) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement has not been executed yet and 
			 * 			the value of the BooleanType identified by the condition is true,
			 * 			then the statement 'then' is executed.
			 * 		  |	if (!this.getIsExecuted())
			 * 		  |		then if (((BooleanType) condition.getValue()).getValue())
			 * 		  |				then then.execute()
			 * 		  	Otherwise the statement 'otherwise' is executed.
			 * 		  |		else then otherwise.execute()
			 * 			In both cases the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	this.setIsExecuted(true)
			 * 		  |	getGlobalVariable().addStatement(this)
			 */
			@Override
			public void execute() {
				if (! (condition.getValue() instanceof BooleanType))
				throw new IllegalTypeException(
						"The condition must contain a booleantype.");
				if (!this.getIsExecuted()) {
					if (((BooleanType) condition.getValue()).getValue())
						then.execute();
					else
						otherwise.execute();
					this.setIsExecuted(true);
					getGlobalVariable().addStatement(this);
				}
			}
			
			/**
			 * Check whether this statement contains an action statement.
			 * 
			 * @return	true if and only if the then- or otherwise-statement contains an action statement.
			 * 		  |	result == (then.containsActionStatement() || otherwise.containsActionStatement())
			 */
			@Override
			public boolean containsActionStatement() {
				return then.containsActionStatement()
						|| otherwise.containsActionStatement();
			}
			
			/**
			 * Check whether this statement is well formed.
			 * 
			 * @return	true if and only if the then- and otherwise-statements are well formed.
			 * 		  |	result == (then.isWellFormed() && otherwise.isWellFormed())
			 */
			@Override
			public boolean isWellFormed() {
				return then.isWellFormed() && otherwise.isWellFormed();
			}
			
			/**
			 * Method to call after executing this statement. 
			 * 
			 * @effect	The flag indicating whether the then statement has been executed is set to false.
			 * 		  |	then.setIsExecuted(false)
			 * @effect	The flag indicating whether the then otherwise has been executed is set to false.
			 * 		  |	otherwise.setIsExecuted(false)
			 * @effect	The super method to set this statement as executed is called.
			 * 		  |	super.setIsExecuted(value)
			 */
			@Override
			protected void setIsExecuted(boolean value) {
				then.setIsExecuted(false);
				otherwise.setIsExecuted(false);
				super.setIsExecuted(value);
			}
		};
	}

	/**
	 * Create a statement that represents the repeated execution of the body
	 * statement, as long as the value of the condition expression evaluates to
	 * true
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createWhile(int line, int column,
			final Expression<? extends Type> condition, final Statement body) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement has not been executed yet, then the body is executed and isExecuted flag of the body is set to false
			 * 			while the value of the BooleanType identified by the expression condition is true.
			 * 		  |	if (!this.getIsExecuted())
			 * 		  |		then while (((BooleanType) condition.getValue()).getValue()) {
			 * 		  |			body.execute()
			 * 		  |			body.setIsExecuted(false) }
			 * 			After the while loop has finished, the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	this.setIsExecuted(true)
			 * 		  |	getGlobalVariable().addStatement(this)
			 */
			@Override
			public void execute() {
				if (!this.getIsExecuted()) {
					while (((BooleanType) condition.getValue()).getValue()) {
						body.execute();
						body.setIsExecuted(false);
					}
					this.setIsExecuted(true);
					getGlobalVariable().addStatement(this);
				}
			}

			/**
			 * Check whether this statement contains an actionstatement.
			 * 
			 * @return True if and only if the body of this statement contains an actionstatement.
			 * 		 | result == body.containsActionStatement()
			 */
			@Override
			public boolean containsActionStatement() {
				return body.containsActionStatement();
			}
			
			/**
			 * Check whether this statement is well formed.
			 * 
			 * @return	true if and only if the body is well formed.
			 * 		  |	result == (body.isWellFormed())
			 */
			@Override
			public boolean isWellFormed() {
				return body.isWellFormed();
			}
		};
	}

	/**
	 * Create a statement that represents the repeated execution of the body
	 * statement, where for each execution the value of the variable with the
	 * given name is set to a different object of the given type.
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createForeach(int line, int column,
			final ForeachType type, final String variableName,
			final Statement body) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement has not been executed yet, then depending on the given type,
			 * 			either all worms, all food or all circular entities
			 * 			are added to an arraylist. If the given type is none of those three,
			 * 			then the arraylist is a new empty arraylist.
			 * 		  |	if (!this.getIsExecuted())
			 * 		  |		then switch (type) {
			 * 		  |			case WORM: objects = world.getCircularEntitiesOfType(Worm.class)
			 * 		  |			case FOOD: objects = world.getCircularEntitiesOfType(Food.class)
			 * 		  |			case ANY: objects = world.getAllCircularEntities()
			 * 		  |			default: objects = new ArrayList<CircularEntity>()
			 * 			After that there is an iteration of the elements
			 * 			in that arraylist and for each element the value of the given variable is set
			 * 			to that element and the body is executed. After the execution of the body,
			 * 			the flag indicating whether that body has been executed yet is set to false.
			 * 		  |	for each object in objects: {
			 * 		  |		getGlobalVariable().getGlobalVariableMap().put(variableName, new EntityType(object))
			 * 		  |		body.execute()
			 * 		  |		body.setIsExecuted(false) }
			 * 			After the iteration finished, the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	this.setIsExecuted(true)
			 * 		  |	getGlobalVariable().addStatement(this)
			 */
			@Override
			public void execute() {
				if (!this.getIsExecuted()) {
					World world = getGlobalVariable().getWorm().getWorld();
					LinkedList<? extends CircularEntity> objects;
					switch (type) {
					case WORM:
						objects = (LinkedList<? extends CircularEntity>) world.getCircularEntitiesOfType(Worm.class);
						break;
					case FOOD:
						objects = (LinkedList<? extends CircularEntity>) world.getCircularEntitiesOfType(Food.class);
						break;
					case ANY:
						objects = (LinkedList<? extends CircularEntity>) world.getAllCircularEntities();
						break;
					default:
						objects = new LinkedList<CircularEntity>();
					}
					for (CircularEntity object : objects) {
						getGlobalVariable().getGlobalVariableMap().put(variableName, new EntityType(object));
						body.execute();
						body.setIsExecuted(false);
					}
					this.setIsExecuted(true);
					getGlobalVariable().addStatement(this);
				}
			}
			
			/**
			 * Check whether this statement is well formed.
			 * 
			 * @return	true if and only if the body does not contain an action statement.
			 * 		  |	result == (! body.containsActionStatement())
			 */
			@Override
			public boolean isWellFormed() {
				return ! body.containsActionStatement();
			}

			/**
			 * Check whether this statement contains an actionstatement.
			 * 
			 * @return True if and only if the body of this statement contains an actionstatement.
			 * 		 | result == body.containsActionStatement()
			 */
			@Override
			public boolean containsActionStatement() {
				return body.containsActionStatement();
			}
		};
	}

	/**
	 * Create a statement that represents the sequential execution of the given
	 * statements
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createSequence(int line, int column,
			final List<Statement> statements) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this sequence statement has not been executed yet, then
			 * 			each statement in the list of statements of this sequence is executed.
			 * 		  |	if (!this.getIsExecuted())
			 * 		  |		then for each statement in statements:
			 * 		  |			statement.execute()
			 * 			After that, the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	this.setIsExecuted(true)
			 * 		  |	getGlobalVariable().addStatement(this)
			 */
			@Override
			public void execute() {
				if (!this.getIsExecuted()) {
					for (Statement statement : statements)
						statement.execute();
					this.setIsExecuted(true);
					getGlobalVariable().addStatement(this);
				}
			}
			
			/**
			 * Check whether this statement contains an action statement.
			 * 
			 * @return	true if and only if at least one of the statements in the list of statements
			 * 			contains an actionstatement.
			 * 		  |	for each statement in statements:
			 * 		  |		if statement.containsActionStatement()
			 * 		  |			then result == true
			 */
			@Override
			public boolean containsActionStatement() {
				for (Statement statement : statements)
					if (statement.containsActionStatement())
						return true;
				return false;
			}
			
			/**
			 * Check whether this statement is well formed.
			 * 
			 * @return true is and only if each statement in this sequence of statements is well formed.
			 * 		 |	for each statement in statements:
			 * 		 |		if (! statement.isWellFormed())
			 * 		 |			then result == false
			 */
			@Override
			public boolean isWellFormed() {
				for (Statement statement : statements)
					if (! statement.isWellFormed())
						return false;
				return true;
			}
			
			/**
			 * Method to call after executing this statement. 
			 * 
			 * @effect	For each statement in the list of statements for this sequence, the flag
			 * 			indicating whether that statement has been executed is set to false.
			 * 		  |	for each statement in statements:
			 * 		  |		statement.setIsExecuted(false)
			 * @effect	The super method to set this statement as executed is called.
			 * 		  |	super.setIsExecuted(value)
			 */
			@Override
			protected void setIsExecuted(boolean value) {
				for (Statement statement : statements)
					statement.setIsExecuted(false);
				super.setIsExecuted(value);
			}
		};
	}

	/**
	 * Create a statement that represents printing out the value of the
	 * expression e
	 * 
	 * @return 	Returns a new statement of which the execute method executes the requested action.
	 * 		 |	return new Statement()
	 */
	@Override
	public Statement createPrint(int line, int column, final Expression<? extends Type> e) {
		return new Statement() {
			
			/**
			 * Execute this statement.
			 * 
			 * @effect	If this statement has not been executed yet, then the print method is called on the handler
			 * 			with the string representation of the value of the given expression as argument.
			 * 		  |	if (! this.getIsExecuted())
			 * 		  |		getHandler().print(e.getValue().toString())
			 *			After that, the flag indicating whether this statement has been executed is set to true,
			 * 			and this statement is added to the list of executed statement in the GlobalVariable.
			 * 		  |	this.setIsExecuted(true)
			 * 		  |	getGlobalVariable().addStatement(this)
			 * 			
			 */
			@Override
			public void execute() {
				if (! this.getIsExecuted()) {
					getHandler().print(e.getValue().toString());
					this.setIsExecuted(true);
					getGlobalVariable().addStatement(this);
				}
			}
		};
	}

	/* types */

	/**
	 * Returns an object that represents the type of a global variable with
	 * declared type 'double'.
	 * 
	 * @return A new DoubleType with default value.
	 * 		 |	return new DoubleType()
	 */
	@Override
	public Type createDoubleType() {
		return new DoubleType();
	}

	/**
	 * Returns an object that represents the type of a global variable with
	 * declared type 'boolean'.
	 * 
	 * @return A new BooleanType with default value.
	 * 		 |	return new BooleanType()
	 */
	@Override
	public Type createBooleanType() {
		return new BooleanType();
	}

	/**
	 * Returns an object that represents the type of a global variable with
	 * declared type 'entity'.
	 * 
	 * @return A new EntityType with default value.
	 * 		 |	return new EntityType()
	 */
	@Override
	public Type createEntityType() {
		return new EntityType();
	}
	
	/**
	 * Check whether the given handler is a valid handler for any Factory.
	 * 
	 * @param handler
	 *        The handler to check.
	 * @return True if and only if the handler is an effective handler.
	 *         | result == (handler != null)
	 */
	@Raw
	private static boolean isValidHandler(IActionHandler handler){
		return (handler != null);
	}
	
	/**
	 * Check whether the given GlobalVariable is a valid global for any Factory.
	 * 
	 * @param GlobalVariable
	 *        The GlobalVariable to check.
	 * @return True if and only if the GlobalVariable is an effective GlobalVariable.
	 *         | result == (GlobalVariable != null)
	 */
	private static boolean isValidGlobalVariable(GlobalVariable GlobalVariable){
		return (GlobalVariable != null);
	}

}
