package worms.model;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class of weapons involving a mass, a worm and ammunition.
 * 
 * @invar   This weapon has a proper worm.
 * 		  |	hasProperWorm() 
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public abstract class Weapon {

	/**
	 * Initialize this new weapon with the given projectile mass and ammunition.
	 * 
	 * @param 	ammunition
	 * 			The ammunition for this new weapon.
	 * @effect	The ammunition is set to the given amount.
	 * 		  |	setAmmunition(ammunition)
	 */
	protected Weapon(double mass, double ammunition) {
		this.mass = mass;
		setAmmunition(ammunition);
	}
	
	/**
	 * Return the damage this weapon can do.
	 */
	@Basic
	abstract int getDamage();
	
	/**
	 * Return the cost of shooting this weapon.
	 */
	@Basic
	abstract double getShootingCost();
	
	/**
	 * Shoot a new projectile from this weapon with the given propulsion yield.
	 * 
	 * @param 	propulsionYield
	 * 			The propulsion yield used to calculate the force of the shot.
	 */
	abstract void shoot(int propulsionYield) throws IllegalArgumentException;
	
	/**
	 * Return the radius of a projectile shot from this weapon.
	 */
	double getRadius() {
		return Math.pow(3.0/(4.0*Math.PI)*getMass()/Projectile.DENSITY, 1.0/3);
	}
	
	/**
	 * Return the force of a projectile shot from this weapon.
	 */
	abstract double getForce(int propulsionYield);
	
	/**
	 * Return the mass of a projectile shot from this weapon.
	 */
	@Basic
	public double getMass() {
		return mass;
	}
	
	/**
	 * Variable registering the mass of this weapon's projectiles.
	 */
	private double mass;
	
	/**
	 * Return the name of this weapon.
	 */
	@Basic
	abstract String getName();
	
	/**
	 * Return the ammunition for this weapon.
	 */
	@Basic
	public double getAmmunition() {
		return this.ammunition;
	}
	
	/**
	 * Set the ammunition of this weapon to the given amount.
	 * 
	 * @param 	ammunition
	 * 			The amount of ammunition to set.
	 * @post	The new amount of ammunition of this weapon is equal to the given amount.
	 * 		  |	new.getAmmunition() == ammunition
	 */
	protected void setAmmunition(double ammunition) {
		this.ammunition = ammunition;
	}
	
	/**
	 * Variable registering the ammunition for this weapon.
	 */
	private double ammunition;
	
	/**
	 * Set the given worm as the owner of this weapon.
	 * 
	 * @param 	worm
	 * 			The worm to set.
	 * @throws 	IllegalArgumentException
	 * 			An IllegalArgumentException is thrown if this weapon can not have the given worm as its owner.
	 * 		  |	! canHaveAsWorm(worm)
	 */
	protected void addWorm(Worm worm) throws IllegalArgumentException{
		if (! canHaveAsWorm(worm))
			throw new IllegalArgumentException();
		this.worm = worm;
	}
	
	/**
	 * Check whether this weapon can have the given worm as its worm.
	 * 
	 * @param 	worm
	 * 			The worm to check.
	 * @return	True if and only if the worm is not null and the worm 
	 *          can have this weapon as its weapon.
	 * 		  |	return ((worm != null) && (worm.canHaveAsWeapon(this)))
	 */
	@Raw
	protected boolean canHaveAsWorm(Worm worm) {
		return ((worm != null) && (worm.canHaveAsWeapon(this)));
	}
	
	/**
	 * Check whether this weapon has a proper worm.
	 * 
	 * @return	true if and only if this weapon can have its worm as its worm and the worm has this weapon.
	 * 		  |	result == canHaveAsWorm(this.getWorm()) 
     *		  |			  &&  ((Worm)getWorm()).hasAsWeapon(this))
	 */
	@Model @Raw
	private boolean hasProperWorm() {
		return (canHaveAsWorm(this.getWorm()) 
				&& ((Worm)getWorm()).hasAsWeapon(this));
	}
	
	/**
	 * Return the worm that owns this weapon.
	 */
	@Basic
	protected Worm getWorm() {
		return worm;
	}
	
	/**
	 * Variable registering the worm that owns this weapon.
	 */
	private Worm worm;

}
