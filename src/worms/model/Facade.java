package worms.model;

import java.util.Collection;
import java.util.Random;

import worms.gui.game.IActionHandler;
import worms.model.Position.Position;
import worms.model.Types.Type;
import worms.model.programs.*;
import worms.model.Program;

/**
 * A class that implements IFacade to link the GUI to the selfmade classes.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 3.0
 *
 */
public class Facade implements IFacade {

	/**
	 * Create and add an empty team with the given name to the given world.
	 */
	@Override
	public void addEmptyTeam(World world, String newName) throws ModelException {
		try {
			world.addAsTeam(newName);
		} catch (IllegalArgumentException exc) {
			throw new ModelException("This world can not have this new team.");
		}
	}

	/**
	 * Create and add a new food ration to the given world. The food must be
	 * placed at a random adjacent location.
	 */
	@Override
	public void addNewFood(World world) throws ModelException {
		try {
			new Food(world);
		} catch (Exception e) {
			throw new ModelException("Could not add food.");
		}
	}

	/**
	 * Returns whether or not the given worm can fall down
	 */
	@Override
	public boolean canFall(Worm worm) {
		return worm.canFall();
	}

	/**
	 * Returns whether or not the given worm is allowed to move.
	 */
	@Override
	public boolean canMove(Worm worm) {
		return worm.canMove();
	}

	/**
	 * Returns whether or not the given worm can turn by the given angle.
	 */
	@Override
	public boolean canTurn(Worm worm, double angle) {
		return worm.canTurn(angle);
	}

	/**
	 * Create a new food ration that is positioned at the given location in the
	 * given world.
	 * 
	 * @param world
	 *            The world in which to place the created food ration
	 * @param x
	 *            The x-coordinate of the position of the new food ration (in
	 *            meter)
	 * @param y
	 *            The y-coordinate of the position of the new food ration (in
	 *            meter)
	 */
	@Override
	public Food createFood(World world, double x, double y) {
		return new Food(world, new Position(x, y));
	}

	/**
	 * Creates a new world.
	 * 
	 * @param width
	 *            The width of the world (in meter)
	 * 
	 * @param height
	 *            The height of the world (in meter)
	 * 
	 * @param passableMap
	 *            A rectangular matrix indicating which parts of the terrain are
	 *            passable and impassable. This matrix is derived from the
	 *            transparency of the pixels in the image file of the terrain.
	 *            passableMap[r][c] is true if the location at row r and column
	 *            c is passable, and false if that location is impassable. The
	 *            elements in the first row (row 0) represent the pixels at the
	 *            top of the terrain (i.e., largest y-coordinates). The elements
	 *            in the last row (row passableMap.length-1) represent pixels at
	 *            the bottom of the terrain (smallest y-coordinates). The
	 *            elements in the first column (column 0) represent the pixels
	 *            at the left of the terrain (i.e., smallest x-coordinates). The
	 *            elements in the last column (column passableMap[0].length-1)
	 *            represent the pixels at the right of the terrain (i.e.,
	 *            largest x-coordinates).
	 * 
	 * @param random
	 *            A random number generator, seeded with the value obtained from
	 *            the command line or from GUIOptions, that can be used to
	 *            randomize aspects of the world in a repeatable way.
	 * 
	 * @return The world.
	 */
	@Override
	public World createWorld(double width, double height,
			boolean[][] passableMap, Random random) throws ModelException {
		try {
			return new World(width, height, passableMap, random);
		} catch (Exception e) {
			throw new ModelException("Create world failed.");
		}
	}

	/**
	 * Makes the given worm fall down until it rests on impassable terrain
	 * again.
	 */
	@Override
	public void fall(Worm worm) throws ModelException {
		try {
			worm.fall();
		} catch (IllegalStateException e) {
			throw new ModelException("This worm can not fall.");
		}
	}

	/**
	 * Returns the current number of action points of the given worm.
	 */
	@Override
	public int getActionPoints(Worm worm) {
		return worm.getAPs();
	}

	/**
	 * Returns the active projectile in the world, or null if no active
	 * projectile exists.
	 */
	@Override
	public Projectile getActiveProjectile(World world) {
		return world.getActiveProjectile();
	}

	/**
	 * Returns the active worm in the given world (i.e., the worm whose turn it
	 * is).
	 */
	@Override
	public Worm getCurrentWorm(World world) {
		return world.getActiveWorm();
	}

	/**
	 * Returns all the food rations in the world.
	 */
	@Override
	public Collection<Food> getFood(World world) {
		return world.getCircularEntitiesOfType(Food.class);
	}

	/**
	 * Returns the current number of hit points of the given worm.
	 */
	@Override
	public int getHitPoints(Worm worm) {
		return worm.getHPs();
	}

	/**
	 * Returns the location on the jump trajectory of the given projectile after
	 * a time t.
	 * 
	 * @return An array with two elements, with the first element being the
	 *         x-coordinate and the second element the y-coordinate
	 */
	@Override
	public double[] getJumpStep(Projectile projectile, double t)
			throws ModelException {
		try {
			Position position = projectile.jumpStep(t);
			return new double[] { position.getX(), position.getY() };
		} catch (IllegalStateException exc) {
			throw new ModelException("This projectile can not jump.");
		}
	}

	/**
	 * Returns the location on the jump trajectory of the given worm after a
	 * time t.
	 * 
	 * @return An array with two elements, with the first element being the
	 *         x-coordinate and the second element the y-coordinate
	 */
	@Override
	public double[] getJumpStep(Worm worm, double t) throws ModelException {
		try {
			Position position = worm.jumpStep(t);
			return new double[] { position.getX(), position.getY() };
		} catch (IllegalStateException exc) {
			throw new ModelException("This worm can not jump.");
		}
	}

	/**
	 * Determine the time that the given projectile can jump until it hits the
	 * terrain, hits a worm, or leaves the world. The time should be determined
	 * using the given elementary time interval.
	 * 
	 * @param projectile
	 *            The projectile for which to calculate the jump time.
	 * 
	 * @param timeStep
	 *            An elementary time interval during which you may assume that
	 *            the projectile will not completely move through a piece of
	 *            impassable terrain.
	 * 
	 * @return The time duration of the projectile's jump.
	 */
	@Override
	public double getJumpTime(Projectile projectile, double timeStep) {
		return projectile.jumpTime(timeStep);
	}

	/**
	 * Determine the time that the given worm can jump until it hits the terrain
	 * or leaves the world. The time should be determined using the given
	 * elementary time interval.
	 * 
	 * @param worm
	 *            The worm for which to calculate the jump time.
	 * 
	 * @param timeStep
	 *            An elementary time interval during which you may assume that
	 *            the worm will not completely move through a piece of
	 *            impassable terrain.
	 * 
	 * @return The time duration of the worm's jump.
	 */
	@Override
	public double getJumpTime(Worm worm, double timeStep) throws ModelException {
		try {
			return worm.jumpTime(timeStep);
		} catch (Exception exc) {
			throw new ModelException("Jump failed");
		}
	}

	/**
	 * Returns the mass of the given worm.
	 */
	@Override
	public double getMass(Worm worm) {
		return worm.getMass();
	}

	/**
	 * Returns the maximum number of action points of the given worm.
	 */
	@Override
	public int getMaxActionPoints(Worm worm) {
		return worm.getMaxAPs();
	}

	/**
	 * Returns the maximum number of hit points of the given worm.
	 */
	@Override
	public int getMaxHitPoints(Worm worm) {
		return worm.getMaxHPs();
	}

	/**
	 * Returns the minimal radius of the given worm.
	 */
	@Override
	public double getMinimalRadius(Worm worm) {
		return Worm.MINIMALRADIUS;
	}

	/**
	 * Returns the name the given worm.
	 */
	@Override
	public String getName(Worm worm) {
		return worm.getName();
	}

	/**
	 * Returns the current orientation of the given worm (in radians).
	 */
	@Override
	public double getOrientation(Worm worm) {
		return worm.getDirection();
	}

	/**
	 * Returns the radius of the given food ration.
	 */
	@Override
	public double getRadius(Food food) {
		return food.getRadius();
	}

	/**
	 * Returns the radius of the given projectile.
	 */
	@Override
	public double getRadius(Projectile projectile) {
		return projectile.getRadius();
	}

	/**
	 * Returns the radius of the given worm.
	 */
	@Override
	public double getRadius(Worm worm) {
		return worm.getRadius();
	}

	/**
	 * Returns the name of the weapon that is currently active for the given
	 * worm, or null if no weapon is active.
	 */
	@Override
	public String getSelectedWeapon(Worm worm) {
		return worm.getActiveWeapon().getName();
	}

	/**
	 * Returns the name of the team of the given worm, or returns null if this
	 * worm is not part of a team.
	 */
	@Override
	public String getTeamName(Worm worm) {
		return worm.getTeamName();
	}

	/**
	 * Returns the name of a single worm if that worm is the winner, or the name
	 * of a team if that team is the winner. This method should null if there is
	 * no winner.
	 */
	@Override
	public String getWinner(World world) {
		return world.getWinner();
	}

	/**
	 * Returns all the worms in the given world
	 */
	@Override
	public Collection<Worm> getWorms(World world) {
		return world.getCircularEntitiesOfType(Worm.class);
	}

	/**
	 * Returns the x-coordinate of the given food ration.
	 */
	@Override
	public double getX(Food food) {
		return food.getX();
	}

	/**
	 * Returns the x-coordinate of the given projectile.
	 */
	@Override
	public double getX(Projectile projectile) {
		return projectile.getX();
	}

	/**
	 * Returns the x-coordinate of the current location of the given worm.
	 */
	@Override
	public double getX(Worm worm) {
		return worm.getX();
	}

	/**
	 * Returns the y-coordinate of the given food ration.
	 */
	@Override
	public double getY(Food food) {
		return food.getY();
	}

	/**
	 * Returns the y-coordinate of the given projectile.
	 */
	@Override
	public double getY(Projectile projectile) {
		return projectile.getY();
	}

	/**
	 * Returns the y-coordinate of the current location of the given worm.
	 */
	@Override
	public double getY(Worm worm) {
		return worm.getY();
	}

	/**
	 * Returns whether or not the given food ration is alive (active), i.e., not
	 * eaten.
	 */
	@Override
	public boolean isActive(Food food) {
		return !food.isTerminated();
	}

	/**
	 * Returns whether the given projectile is still alive (active).
	 */
	@Override
	public boolean isActive(Projectile projectile) {
		return !projectile.isTerminated();
	}

	/**
	 * Checks whether the given circular region of the given world, defined by
	 * the given center coordinates and radius, is passable and adjacent to
	 * impassable terrain.
	 * 
	 * @param world
	 *            The world in which to check adjacency
	 * @param x
	 *            The x-coordinate of the center of the circle to check
	 * @param y
	 *            The y-coordinate of the center of the circle to check
	 * @param radius
	 *            The radius of the circle to check
	 * 
	 * @return True if the given region is passable and adjacent to impassable
	 *         terrain, false otherwise.
	 */
	@Override
	public boolean isAdjacent(World world, double x, double y, double radius) {
		return world.isAdjacent(new Position(x, y), radius);
	}

	/**
	 * Returns whether the given worm is alive
	 */
	@Override
	public boolean isAlive(Worm worm) {
		return !worm.isTerminated();
	}

	/**
	 * Returns whether the game in the given world has finished.
	 */
	@Override
	public boolean isGameFinished(World world) {
		return world.isGameFinished();
	}

	/**
	 * Checks whether the given circular region of the given world, defined by
	 * the given center coordinates and radius, is impassable.
	 * 
	 * @param world
	 *            The world in which to check impassability
	 * @param x
	 *            The x-coordinate of the center of the circle to check
	 * @param y
	 *            The y-coordinate of the center of the circle to check
	 * @param radius
	 *            The radius of the circle to check
	 * 
	 * @return True if the given region is impassable, false otherwise.
	 */
	@Override
	public boolean isImpassable(World world, double x, double y, double radius) {
		return world.isImpassable(new Position(x, y), radius);
	}

	/**
	 * Make the given projectile jump to its new location. The new location
	 * should be determined using the given elementary time interval.
	 * 
	 * @param projectile
	 *            The projectile that needs to jump
	 * 
	 * @param timeStep
	 *            An elementary time interval during which you may assume that
	 *            the projectile will not completely move through a piece of
	 *            impassable terrain.
	 */
	@Override
	public void jump(Projectile projectile, double timeStep)
			throws ModelException {
		try {
			projectile.jump(timeStep);
		} catch (IllegalStateException exc) {
			throw new ModelException("This projectile can not jump.");
		}
	}

	/**
	 * Make the given worm jump to its new location. The new location should be
	 * determined using the given elementary time interval.
	 * 
	 * @param worm
	 *            The worm that needs to jump
	 * 
	 * @param timeStep
	 *            An elementary time interval during which you may assume that
	 *            the worm will not completely move through a piece of
	 *            impassable terrain.
	 */
	@Override
	public void jump(Worm worm, double timeStep) throws ModelException {
		try {
			worm.jump(timeStep);
		} catch (IllegalStateException exc) {
			throw new ModelException("This worm can not jump.");
		}

	}

	/**
	 * Moves the given worm according to the rules in the assignment.
	 */
	@Override
	public void move(Worm worm) throws ModelException {
		try {
			worm.move();
		} catch (IllegalStateException e) {
			throw new ModelException("This worm can not move");
		}
	}

	/**
	 * Renames the given worm.
	 */
	@Override
	public void rename(Worm worm, String newName) throws ModelException {
		try {
			worm.setName(newName);
		} catch (IllegalArgumentException exc) {
			throw new ModelException("Invalid name for this worm");
		}
	}

	/**
	 * Activates the next weapon for the given worm
	 */
	@Override
	public void selectNextWeapon(Worm worm) {
		worm.selectNextWeapon();

	}

	/**
	 * Sets the radius of the given worm to the given value.
	 */
	@Override
	public void setRadius(Worm worm, double newRadius) throws ModelException {
		try {
			worm.setRadius(newRadius);
		} catch (IllegalArgumentException exc) {
			throw new ModelException("Illegal radius.");
		}
	}

	/**
	 * Makes the given worm shoot its active weapon with the given propulsion
	 * yield.
	 */
	@Override
	public void shoot(Worm worm, int yield) throws ModelException {
		try {
			worm.shoot(yield);
		} catch (IllegalStateException e) {
			throw new ModelException("This worm can not shoot.");
		}
	}

	/**
	 * Starts a game in the given world.
	 */
	@Override
	public void startGame(World world) throws ModelException {
		try {
			world.startGame();
		} catch (IllegalStateException e) {
			throw new ModelException("Can not start game.");
		}
	}

	/**
	 * Starts the next turn in the given world
	 */
	@Override
	public void startNextTurn(World world) throws ModelException {
		world.startNextTurn();
	}

	/**
	 * Turns the given worm by the given angle.
	 */
	@Override
	public void turn(Worm worm, double angle) {
		worm.turn(angle);
	}

	/**
	 * Create and add a new worm to the given world. The new worm must be placed
	 * at a random adjacent location. The new worm can have an arbitrary (but
	 * valid) radius and direction. The new worm may (but isn't required to)
	 * have joined a team. The worm must behave according to the provided
	 * program, or is controlled by the player if the given program is null.
	 */
	@Override
	public void addNewWorm(World world, Program program) throws ModelException{
		try {
			Worm worm = new Worm(world, program);
		} catch (Exception e) {
			throw new ModelException("Can not add worm.");
		}
	}

	/**
	 * Create a new worm that is positioned at the given location in the given
	 * world, looks in the given direction, has the given radius and the given
	 * name.
	 * 
	 * @param world
	 *            The world in which to place the created worm
	 * @param x
	 *            The x-coordinate of the position of the new worm (in meter)
	 * @param y
	 *            The y-coordinate of the position of the new worm (in meter)
	 * @param direction
	 *            The direction of the new worm (in radians)
	 * @param radius
	 *            The radius of the new worm (in meter)
	 * @param name
	 *            The name of the new worm
	 * @param program
	 *            The program that defines this worm's behavior, or null if this
	 *            worm is controlled by the player
	 */
	@Override
	public Worm createWorm(World world, double x, double y, double direction,
			double radius, String name, Program program) throws ModelException{
		try {
			return new Worm(world, new Position(x, y), direction, radius, name, program);
		} catch (Exception | AssertionError e) {
			throw new ModelException("Worm could not be created.");
		}
	}

	/**
	 * Try to parse the given program. You can use an instance of the
	 * worms.model.programs.ProgramParser.
	 * 
	 * When the program is executed, the execution of an action statement must
	 * call the corresponding method of the given action handler. This executes
	 * the action as if a human player has initiated it, and will eventually
	 * call the corresponding method on the facade.
	 * 
	 * @param programText
	 *            The program text to parse
	 * @param handler
	 *            The action handler on which to execute commands
	 * 
	 * @return the outcome of parsing the program, which can be a success or a
	 *         failure. You can create a ParseOutcome object by means of its two
	 *         static methods, success and failure.
	 */
	@Override
	public ParseOutcome<?> parseProgram(String programText,
			IActionHandler handler) throws ModelException{
		ProgramFactoryImpl factory = new ProgramFactoryImpl(handler);
		ProgramParser<Expression<? extends Type>, Statement, Type> parser = new ProgramParser<>(factory);
		parser.parse(programText);
		if (parser.getErrors().isEmpty()) {
			Program program;
			try {
				program = new Program(parser, factory);
			} catch (Exception e) {
				throw new ModelException("Can not construct program.");
			}
			return ParseOutcome.success(program);
		} else{
			return ParseOutcome.failure(parser.getErrors());
		}
	}

	/**
	 * Returns whether or not the given worm is controlled by a program.
	 * 
	 * @return true if the given worm is controlled by a program, false
	 *         otherwise
	 */
	@Override
	public boolean hasProgram(Worm worm) {
		return worm.hasProgram();
	}

	/**
	 * Returns whether or not the given program is well-formed. A program is
	 * well-formed if a for-each statement does not (directly or indirectly)
	 * contain one or more action statements.
	 * 
	 * @param program
	 *            The program to check
	 * 
	 * @return true if the program is well-formed; false otherwise
	 */
	@Override
	public boolean isWellFormed(Program program) {
		if (program == null)
			return true;
		return program.isWellFormed();
	}

}
