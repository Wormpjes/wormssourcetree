package worms.model;

import worms.model.Types.Type;

/**
 * An interface of expressions, parameterized in the type T of it's value. 
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public interface Expression<T extends Type> {
		
	/**
	* Return the value of this expression.
	*/
	public T getValue();	
	
}
