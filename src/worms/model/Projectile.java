package worms.model;

import java.util.ArrayList;
import java.util.LinkedList;

import be.kuleuven.cs.som.annotate.*;
import worms.model.Position.Position;
import worms.util.Util;

/**
 * A class of projectiles involving a weapon, propulsionYield, position, radius, direction and density.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class Projectile extends JumpableEntity {
	
	/**
	 * Initialize this new projectile with a given weapon and propulsion yield.
	 * 
	 * @param 	weapon
	 * 		  	The weapon from which this projectile was shot.
	 * @param 	propulsionYield
	 * 		 	The propulsion yield of the shot.
	 * @effect	This new projectile is initialized as a new jumpable entity with arguments: 
	 * 			world of the weapon, the position of the projectile,
	 * 			direction of the worm that owns the weapon and the radius of the weapon's projectiles.
	 * 			The position of the projectile is set to the position in the direction of the worm 
	 * 			and at a distance equal to the worm's radius from the worm's position.
	 * 		  |	super(weapon.getWorm().getWorld(),
	 *		  |	new Position(weapon.getWorm().getPosition().getX() + weapon.getWorm().getRadius() * Math.cos(weapon.getWorm().getDirection()),
	 *		  |		  	  	 weapon.getWorm().getPosition().getY() + weapon.getWorm().getRadius() * Math.sin(weapon.getWorm().getDirection())),
	 *		  |	weapon.getWorm().getDirection(),
	 *		  |	weapon.getRadius(),
	 *		  |	DENSITY);
	 */
	public Projectile(Weapon weapon, int propulsionYield) {
		super(weapon.getWorm().getWorld(),
			  new Position(weapon.getWorm().getPosition().getX() + weapon.getWorm().getRadius() * Math.cos(weapon.getWorm().getDirection()),
					  	   weapon.getWorm().getPosition().getY() + weapon.getWorm().getRadius() * Math.sin(weapon.getWorm().getDirection())),
			  weapon.getWorm().getDirection(),
			  weapon.getRadius(),
			  DENSITY);
		this.weapon = weapon;
		this.propulsionYield = propulsionYield;
	}
	
	/**
	 * Variable registering the weapon from which this projectile has been shot.
	 */
	private final Weapon weapon;
	
	/**
	 * Variable registering the propulsion yield of this projectile.
	 */
	private final int propulsionYield;
	
	/**
	 * Constant registering the density of a projectile.
	 */
	protected final static double DENSITY = 7800;
	
	/**
	 * Return the weapon from which this projectile has been shot.
	 */
	@Basic
	public Weapon getWeapon() {
		return this.weapon;
	}
	
	/**
	 * Return the propulsion yield of this projectile.
	 */
	@Basic @Immutable
	private final int getPropulsionYield() {
		return this.propulsionYield;
	}
	
	/**
	 * Return the force which is used to calculate the trajectory of the jump.
	 * 
	 * @effect	The force is calculated by this projectile's weapon.
	 * 		  |	getWeapon().getForce(getPropulsionYield())
	 * @return	The force as calculated by this projectile's weapon.
	 * 	 	  |	getWeapon().getForce(getPropulsionYield())
	 */
	protected double getForce() {
		return getWeapon().getForce(getPropulsionYield()) ;
	}
	
	/**
	 * Check whether the given position is a valid position to end the jump.
	 * 
	 * @return	true if and only if the position is impassable, outside of the world
	 * 			or would cause an overlap of the projectile and at least one worm in the world.
	 * 		  | result == 	( getWorld().isImpassable(position, getRadius() )
	 * 		  |				|| ( ! getWorld().isInsideWorld(position, getRadius()) )
	 * 		  |				|| ( for (Worm worm:getWorld().getCircularEntitiesOfType(Worm.class))
	 * 		  |							Util.fuzzyLessThanOrEqualTo(
	 *	      |									position.getDistanceToOtherPosition(worm.getPosition()),
	 *		  |									this.getRadius() + worm.getRadius()) 
	 *		  | 				)
	 */
	protected boolean isValidEndPosition(Position position) {
		if ( getWorld().isImpassable(position, getRadius()) || ( ! getWorld().isInsideWorld(position, getRadius()) ) )
			return true;
		for (Worm worm:getWorld().getCircularEntitiesOfType(Worm.class))
			if ( Util.fuzzyLessThanOrEqualTo(
							position.getDistanceToOtherPosition(worm.getPosition()),
							this.getRadius() + worm.getRadius()) && worm != this.getWeapon().getWorm())
				return true;
		return false;
	}
	
	/**
	 * Let the projectile jump with a given timestep during which is assumed
	 * the projectile doesn't hit a worm or impassable terrain.
	 * If the projectile hits a worm after the jump, then the worm is damaged.
	 * After the jump, the projectile is terminated.
	 * 
	 * @effect	The super method jump is called with the given timestep.
	 * 		  |	super.jump(timeStep)
	 * @effect	The overlapping worm is damaged, if the projectile hits it.
	 * 		  | doDamage(worm)
	 * @effect	The projectile is terminated.
	 * 		  |	terminate()
	 * @throws	IllegalStateException
	 * 			An illegalstateexception is thrown if the projectile can not jump.
	 */
	@Override
	public void jump(double timeStep) throws IllegalStateException {
		if (! canJump())
			throw new IllegalStateException();
		super.jump(timeStep);
		if (! isTerminated()) {
			Position positionAfterJump = getPosition();
			LinkedList<Worm> wormList = (LinkedList<Worm>) getWorld().getCircularEntitiesOfType(Worm.class);
			for (Worm worm:wormList)
				if (Util.fuzzyLessThanOrEqualTo(worm.getPosition().getDistanceToOtherPosition(positionAfterJump),
												worm.getRadius() + this.getRadius()))
					doDamage(worm);
			terminate();
		}
	}
	
	/**
	 * Lower the given worm's hitpoints by the amount of damage 
	 * of the weapon from which this projectile was shot. 
	 * 
	 * @param 	worm
	 * 		  	The worm that is to be damaged.
	 * @effect	The hitpoints of the given worm are lowered by the damage of the weapon from which this projectile was shot.
	 * 		  |	worm.setHPs(worm.getHPs() - getWeapon().getDamage())
	 */
	private void doDamage(Worm worm) {
		worm.setHPs(worm.getHPs() - getWeapon().getDamage());
	}
	
	/**
	 * Check whether the projectile can jump.
	 * 
	 * @return	True if this projectile is attached to the active worm in the world.
	 * 		  |	result == (this.getWeapon().getWorm() == this.getWorld().getActiveWorm())
	 */
	@Override
	protected boolean canJump() {
		return (this.getWeapon().getWorm() == this.getWorld().getActiveWorm());
	}
	
	
	/**
	 * Terminate this projectile.
	 * 
	 * @effect	The active projectile of this projectile's world is set to null.
	 * 		  |	getWorld().setActiveProjectile(null)
	 * @effect	This projectile is removed from the list of circular entities of its world.
	 * 		  |	getWorld().removeCircularEntity(this)
	 */
	@Override
	public void terminate() {
		getWorld().setActiveProjectile(null);
		getWorld().removeCircularEntity(this);
		super.terminate();
	}
	
	/**
	 * Check whether the given radius is a valid radius for this projectile.
	 * 
	 * @return	if the weapon is null, it returns true if and only if the radius is greater than zero.
	 * 		  |	if (weapon == null)
	 * 		  |		then result == radius > 0;
	 * 			Otherwise it returns true if and only if the radius is equal to the radius of the weapon's projectiles.
	 * 		  |	result == Util.fuzzyEquals(radius, weapon.getRadius())
	 */
	@Override
	public boolean isValidRadius(double radius) {
		if (weapon == null)
			return radius > 0;
		return Util.fuzzyEquals(radius, weapon.getRadius());
	}
	
}

