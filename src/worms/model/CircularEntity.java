package worms.model;

import worms.model.Position.Position;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of circular entities involving a termination status, position, radius and world.
 * 
 * @invar	This circular entity has a proper position.
 * 		  |	hasProperPosition()
 * @invar	This circular entity has a proper world.
 * 		  |	hasProperWorld()
 * @invar	The radius of this circular entity is a valid radius.
 * 		  |	isValidRadius(getRadius())
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 2.0
 *
 */
public abstract class CircularEntity {
	
	/**
	 * Initialize this new circular entity inside the given world with the given position and radius.
	 * 
	 * @param 	world
	 * 			The world for new this circular entity.
	 * @param 	position
	 * 			The position of this new circular entity.
	 * @param 	radius
	 * 			The radius of this new circular entity.
	 * @effect	This circular entity is added to the given world.
	 * 		  |	world.addCircularEntity(this)
	 * @effect	The position of this new circular entity is set to the given position.
	 * 		  |	setPosition(position)
	 * @effect	The radius of this new circular entity is set to the given radius.
	 * 		  |	setRadius(radius)
	 * @throws 	IllegalArgumentException
	 * 			An IllegalArgumentException is thrown if the new circular entity can not have this world as its world.
	 * 	      |	! this.canHaveAsWorld(world)
	 */
	protected CircularEntity(World world, Position position, double radius) throws IllegalArgumentException {
		if (! this.canHaveAsWorld(world))
			throw new IllegalArgumentException();
		this.world = world;
		world.addCircularEntity(this);
		setPosition(position);
		setRadius(radius);
	}
	
	/**
	 * Set the position of this circular entity to the given position.
	 * 
	 * @param 	position
	 * 			The position to set.
	 * @post	The new position has the same x- and y-coordinate as the given position.
	 * 		  |	new.getX() == position.getX() && new.getY() == position.getY()
	 */
	protected void setPosition(Position position) throws IllegalArgumentException {
		if (! canHaveAsPosition(position))
			throw new IllegalArgumentException("Invalid position");
		this.position = new Position(position.getX(),position.getY());
	}
	
	/**
	 * Return the position of this circular entity.
	 */
	@Basic
	public Position getPosition() {
		return new Position(position.getX(),position.getY());
	}
	
	/**
	 * Check whether this circular entity can have the given position as its position.
	 * 	
	 * @param 	position
	 * 			The position to check.
	 * @return	always true.
	 * 		  |	result == true
	 */
	protected boolean canHaveAsPosition(Position position) {
		return true;
	}
	
	/**
	 * Check whether this circular entity has a proper position.
	 * 
	 * @return 	true if and only if this circular entity can have its position as position.
	 * 		  |	result == (canHaveAsPosition(getPosition()))
	 */
	protected boolean hasProperPosition() {
		return canHaveAsPosition(getPosition());
	}
	
	/**
	 * Variable registering the position of this circular entity.
	 */
	private Position position;
	
	/**
	 * Return the x-coordinate of this circular entity.
	 * 	The x-coordinate expresses the position of the circular entity with respect to the x-axis.
	 */
	public double getX() {
		return position.getX();
	}
	
	/**
	 * Return the y-coordinate of this circular entity.
	 * 	The y-coordinate expresses the position of the circular entity with respect to the y-axis.
	 */
	public double getY() {
		return position.getY();
	}
	
	/**
	 * Check whether the given coordinate is a valid coordinate for this circular entity.
	 * 
	 * @param  coordinate
	 * 		   The coordinate to check.
	 * @return true if and only if the given coordinate is not NaN.
	 * 		 | result == (! Double.isNaN(coordinate))
	 */
	@Model
	protected static boolean isValidCoordinate(double coordinate) {
		return (! Double.isNaN(coordinate));
	}
	
	/**
	 * Set the radius of this circular entity to the given radius.
	 * 
	 * @param  radius
	 * 		   The new radius for this circular entity.
	 * @post   The radius of this circular entity is equal to the given radius.
	 *       | new.getRadius() == radius
	 * @throws IllegalArgumentException
	 * 		   Throws an exception if the given radius is not a valid radius for this circular entity.
	 *       | ! isValidRadius(radius)
	 */
	protected void setRadius(double radius) throws IllegalArgumentException {
		if (! isValidRadius(radius))
			throw new IllegalArgumentException("The given radius is not a valid radius.");
		this.radius = radius;
	}
	
	/**
	 * Return the radius of this circular entity.
	 */
	@Basic
	public double getRadius() {
		return radius;
	}
	
	/**
	 * Check whether the given radius is a valid radius for this circular entity.
	 * 
	 * @param 	radius
	 * 			The radius to check.
	 */
	abstract boolean isValidRadius(double radius);
	
	/**
	 * Variable registering the radius of this circular entity.
	 */
	protected double radius;
	
	/**
	 * Check whether this circular entity is terminated.
	 */
	@Basic
	public boolean isTerminated() {
		return isTerminated;
	}
	
	/**
	 * Terminate this circular entity.
	 * 
	 * @effect	This circular entity is removed from its world.
	 * 		  |	getWorld().removeCircularEntity(this)
	 * @post	This circular entity is terminated.
	 * 		  |	new.isTerminated() == true
	 */
	public void terminate()  {
		getWorld().removeCircularEntity(this);
		isTerminated = true;
	}
	
	/**
	 * Variable registering whether this circular entity has been terminated.
	 */
	private boolean isTerminated = false;
	
	/**
	 * Return the world of this circular entity.
	 */
	@Basic
	public World getWorld() {
		return world;
	}
	
	/**
	 * Check whether this circular entity can have the given world as its world.
	 * 
	 * @param 	world
	 * 			The world to check.
	 * @return	true if and only if the world is not null 
	 * 			and if this is a projectile, the world does not contain any other projectile.
	 * 		  |	result == (world != null) && (!(this instanceof Projectile
			  |      	&& (!world.getCircularEntitiesOfType(Projectile.class).contains(
			  |			this)) && (world.getCircularEntitiesOfType(Projectile.class).size() > 0))
	 */
	@Raw
	protected boolean canHaveAsWorld(World world) {
		if (world == null)
			return false;
		if ((this instanceof Projectile)
				&& (!world.getCircularEntitiesOfType(Projectile.class).contains(
						this))
				&& (world.getCircularEntitiesOfType(Projectile.class).size() > 0))
			return false;
		return true;
	}
	
	
	/**
	 * Variable registering the world of this circular entity.
	 */
	private World world;
	
	/**
	 * Check whether this circular entity has a proper world.
	 * 
	 * @return	true if and only if this circular entity can have its world as its world
	 * 			and the world contains this circular entity.
	 * 		  |	result == canHaveAsWorld(getWorld()) 
	 *        |			  && (((World) getWorld()).getAllCircularEntities().contains(this))
	 */
	protected boolean hasProperWorld() {
		return canHaveAsWorld(getWorld()) 
				&& ((this.getWorld()).getAllCircularEntities().contains(this));
	}
	
}
