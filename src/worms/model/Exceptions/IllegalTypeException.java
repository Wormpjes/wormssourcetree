package worms.model.Exceptions;

/**
 * A class of IllegalTypeExceptions.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class IllegalTypeException extends RuntimeException {

	/**
	 * Initialize this new IllegalTypeException with the given message.
	 * 
	 * @param message
	 * 			The message for this new IllegalTypeException.
	 * @effect	This IllegalTypeException is initialized as a new RuntimeException
	 * 			with the given message.
	 * 		  |	super(message)
	 */
	public IllegalTypeException(String message) {
		super(message);
	}
	
	/**
	 * Variable registering the serialVersionUID of any IllegalTypeException.
	 */
	private static final long serialVersionUID = 1L;
}