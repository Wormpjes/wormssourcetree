package worms.model.Exceptions;

import worms.model.Statement;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of FailedExecutionExceptions involving a statement registering which statement failed.
 * 
 * @author Olivier Kamers && Christof Luyten
 * 
 * @version 1.0
 *
 */
public class FailedExecutionException extends RuntimeException {
	
	/**
	 * Initialize this new FailedExecutionException with the given failed statement.
	 * 
	 * @param failedStatement
	 * 			The statement which failed to execute.
	 */
	public FailedExecutionException(Statement failedStatement) {
		this.failedStatement = failedStatement;
	}
	
	/**
	 * Return the statement which failed to execute.
	 */
	@Basic
	public Statement getFailedStatement() {
		return this.failedStatement;
	}

	/**
	 * Variable registering the statement which failed to execute.
	 */
	private Statement failedStatement;
	
	/**
	 * Variable registering the serialVersionUID of any FailedExecutionException.
	 */
	private static final long serialVersionUID = 1L;
}
