package worms.model;

import worms.model.Position.Position;
import be.kuleuven.cs.som.annotate.Basic;

/**
 * A class of food rations involving a growthfactor.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 * 
 */
public class Food extends CircularEntity {

	/**
	 * Initialize a new food ration in the given world at the given position.
	 * 
	 * @param world
	 *            The world to which the food reason is to be added.
	 * @param position
	 *            The position of this new food ration.
	 * @effect This new food ration is initialized as a new circular entity in
	 *         the given world, at the given position and with the constant
	 *         radius of any food ration. | super(world, position, RADIUS)
	 */
	public Food(World world, Position position) {
		super(world, position, RADIUS);
	}

	/**
	 * Construct a new food at a random adjacent position in the given world.
	 * 
	 * @param world
	 *            The world to which the food reason is to be added.
	 * @effect This new food ration is initialized as a new food ration inside
	 *         the given world at a random position. | this(world,
	 *         Position.randomPosition(world, RADIUS))
	 * @effect The position of this food ration is set to a valid position
	 *         inside the world. |
	 *         this.setPosition(world.getValidPosition(this))
	 */
	public Food(World world) {
		this(world, Position.randomPosition(world, RADIUS));
		this.setPosition(world.getValidPosition(this));
	}

	/**
	 * Variable registering the growth factor of a food ration.
	 */
	private final double growthFactor = 0.1;

	/**
	 * Variable registering the radius of a food ration.
	 */
	protected final static double RADIUS = 0.20;

	/**
	 * Return the growth factor of this food ration.
	 */
	@Basic
	public double getGrowthFactor() {
		return growthFactor;
	}

	/**
	 * Check whether the given radius is a valid radius.
	 * 
	 * @return true if and only if the given radius is equal to the
	 *         minimalradius. | result == (radius == RADIUS)
	 */
	@Override
	public boolean isValidRadius(double radius) {
		return radius == RADIUS;
	}

}
