package worms.model;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class of statements involving a line, column and action handler.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 * 
 */
public abstract class Statement implements Cloneable {

	/**
	 * Check whether this statement has been executed already.
	 */
	@Basic
	protected boolean getIsExecuted() {
		return isExecuted;
	}

	/**
	 * Set the isExecuted flag of this statement to the given value.
	 * 
	 * @param value
	 *            The value to set.
	 * @effect If the value is true, then this statement is added to the list of
	 *         executed statements in the globals.
	 *       | factory.getGlobals().addStatement(this);  
	 */
	protected void setIsExecuted(boolean value) {
		if (value == true) {
			this.isExecuted = true;
		} else {
			isExecuted = false;
		}
	}
	
	/**
	 * Variable registering whether this statement has been executed already.
	 */
	private boolean isExecuted;


	/**
	 * Execute this statement.
	 */
	protected abstract void execute();
	
	/**
	 * Check whether this statement is wellformed.
	 * 
	 * @return 	Always true if the statement is not a foreach statement.
	 * 			And true if the statement is a foreach statement and does not include an action statement.
	 */
	protected boolean isWellFormed() {
		return true;
	}
	
	/**
	 * Check whether this statement contains an action statement.
	 * 
	 * @return 	Always false, except if the method is overriden because the implemented
	 * 			statement does contain an action statement. Then this method returns true. 
	 */
	protected boolean containsActionStatement() {
		return false;
	}

}
