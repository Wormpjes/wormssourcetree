package worms.model;

import worms.model.Position.Position;
import worms.util.Util;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of jumpable entities involving a direction and density.
 * 
 * @invar	This jumpable entity has a valid direction.
 * 		  |	isValidDirection(getDirection())
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 2.0
 *
 */
public abstract class JumpableEntity extends CircularEntity {
	
	/**
	 * Initialize this new jumpable entity in the given world, with the given position,
	 * direction, radius and density.
	 * 
	 * @param 	world
	 * 			The world in which the new jumpable entity is located.
	 * @param 	position
	 * 			The position of the new jumpable entity.
	 * @param 	direction
	 * 			The direction of the new jumpable entity.
	 * @param 	radius
	 * 			The radius of the new jumpable entity.
	 * @param 	density
	 * 			The density of the new jumpable entity.
	 * @effect	This new jumpable entity is initialized as a new circular entity
	 * 			with the given world, position and radius.
	 * 		  |	super(world, position, radius)
	 * @effect	The direction of this jumpable entity is set to the given direction.
	 * 		  |	setDirection(direction)
	 */
	protected JumpableEntity(World world, Position position, double direction, double radius, double density) {
		super(world, position, radius);
		setDirection(direction);
		this.DENSITY = density;
	}

	/**
	 * Set the direction of the jumpable entity to a given direction.
	 * 
	 * @param direction
	 * 		  The new direction of this jumpable entity.
	 * @pre   The given direction must be a valid direction for a jumpable entity.
	 * 	    | isValidDirection(direction) 
	 * @post  The direction of this jumpable entity is equal to the given direction.
	 * 		| new.getDirection() == direction
	 */
	@Model
	protected void setDirection(double direction) {
		assert isValidDirection(direction);
		this.direction = direction;
	}
	
	/**
	 * Return the direction of this jumpable entity.
	 * 	The direction of a jumpable entity determines in which way it can move and jump.
	 */
	@Basic
	public double getDirection() {
		return direction;
	}
	
	/**
	 * Check whether the given direction is a valid direction for any jumpable entity.
	 * 
	 * @param  direction
	 * 		   The direction to check.
	 * @return True if and only if the given direction is smaller than
	 * 		   two times pi and larger than or equal to zero.
	 * 		 | result == ((direction >= 0) && (direction < 2*Math.PI))
	 */
	@Model
	protected static boolean isValidDirection(double direction) {
		return Util.fuzzyGreaterThanOrEqualTo(direction, 0) && (direction < 2*Math.PI);
	}
	
	/**
	 * Variable registering the direction of this jumpable entity.
	 */
	private double direction;
	
	/**
	 * Return the density of any jumpable entity.
	 */
	@Basic @Immutable
	protected final double getDensity() {
		return DENSITY;
	}
	
	/**
	 * Variable registering the density of any jumpable entity.
	 */
	private final double DENSITY;
	
	/**
	 * Let the jumpable entity jump.
	 * 
	 * @param 	timeStep
	 * 			The elementary time interval during which is assumed the entity
	 * 			does not overlap with impassable terrain.
	 * @throws 	IllegalStateException
	 * 			An illegal state exception is thrown if the entity can not jump.
	 * @post	If the end position is not inside the world, then the entity is terminated.
	 * 		  |	if (! getWorld().isInsideWorld(endPosition, getRadius()))
	 * 		  |		then terminate()
	 * 			Otherwise the position is set to the end position.
	 * 		  |	else
	 * 		  |		then setPosition(endPosition)
	 */
	public void jump(double timeStep){
		if (! canJump())
			throw new IllegalStateException();
		Position endPosition = getEndPosition(timeStep);
		if (! getWorld().isInsideWorld(endPosition, getRadius()))
			terminate();
		else
			setPosition(endPosition);			
	}
	
	/**
	 * Return the end position of the jump.
	 * 
	 * @param 	timeStep
	 * 		  	The elementary time interval during which is assumed the entity
	 * 			does not overlap with impassable terrain.
	 * @return 	The resulting position is a valid end position for the jump.
	 * 		  |	isValidEndPosition(result)
	 */
	private Position getEndPosition(double timeStep) {
		double deltaT = timeStep;
		while(true){
			Position newPossiblePosition = jumpStep(deltaT);
			if (isValidEndPosition(newPossiblePosition))
				return newPossiblePosition;
			deltaT += timeStep;
		}
	}
	
	/**
	 * Check whether the given position is a valid ending position for the jump.
	 */
	abstract boolean isValidEndPosition(Position position);
	
	/**
	 * Return the time it takes for the jumpable entity to complete the jump.
	 * 
	 * @return	If the entity can not jump, then it returns zero.
	 * 		  | if (! canJump())
	 * 		  |		then return 0
	 * 			Otherwise the x-coordinate of the endposition minus the current x-coordinate,
	 * 			divided by the x-component of the initial velocity is returned.
	 * 		  |	v0x = getInitialVelocity()*Math.cos(getDirection())
	 * 		  | return (endPosition.getX() - getX()) / v0x
	 */	
	public double jumpTime(double timeStep) {
		if (! canJump())
			return 0;
		double deltaT = timeStep;
		boolean hasFoundEndPosition = false;
		while(! hasFoundEndPosition){
			Position newPossiblePosition = jumpStep(deltaT);
			if (isValidEndPosition(newPossiblePosition))
				hasFoundEndPosition = true;
			deltaT += timeStep;
		}
		return deltaT;
	}
	
	/**
	 * Return the position of the jumpable entity during a jump at a given time t.
	 * 
	 * @param  	t
	 * 		   	The time for which the step is checked.
	 * @return 	A new position with as x-coordinate the x-coordinate of this jumpable entity
	 * 			plus the initial velocity multiplied by	the cosine of the direction of this jumpable entity and the time,
	 * 		   	and as y-coordinatethe the y-coordinate of this jumpable entity
	 * 			plus the initial velocity multiplied by	the sine of the direction of this jumpable entity and the time,
	 * 		  	minus the time squared multiplied by G and divided by two.
	 * 		  |	return new Position(getX() + getInitialVelocity()*Math.cos(getDirection())*t,
	 *		  |	 					getY() + getInitialVelocity()*Math.sin(getDirection())*t - 0.5 * G * Math.pow(t, 2));
	 * @throws	IllegalStateException
	 * 		  	An IllegalStateException is thrown if the jumpable entity can not jump.
	 * 		  | ! canJump()
	 */
	public Position jumpStep(double t) throws IllegalStateException {
		if (! canJump())
			throw new IllegalStateException();
		return new Position(getX() + getInitialVelocity()*Math.cos(getDirection())*t,
			 	 			getY() + getInitialVelocity()*Math.sin(getDirection())*t - 0.5 * G * Math.pow(t, 2));
	}
	
	/**
	 * Check whether this jumpable entity can jump.
	 */
	protected abstract boolean canJump();
	
	/**
	 * Return the force of the jump.
	 */
	abstract double getForce();
	
	/**
	 * Return the initial velocity of a jump.
	 * 	The initial velocity is equal to half the force of the jump divided by the mass of this jumpable entity.
	 * 
	 * @return 	The force of the jump divided by the mass of the jumpable entity and divided by two.
	 * 	      |	(getForce()/getMass()) * 0.5
	 */
	@Model
	protected double getInitialVelocity() {
		return (getForce()/getMass()) * 0.5;
	}
	
	/**
	 * Return the distance covered in a jump.
	 * 	The distance covered in a jump is equal to the initial velocity squared,
	 *  multiplied by the sine of two times the direction of this jumpable entity 
	 *  and divided by the gravitational constant G.
	 * 
	 * @return 	The initial velocity squared, multiplied by the sine of two times the direction of this jumpable entity,
	 * 			divided by the gravitational constant G.
	 * 		  |	( Math.pow(getInitialVelocity(), 2) * Math.sin(2.0*getDirection()) ) / G
	 */
	@Model
	private double getDistance() {
		return ( Math.pow(getInitialVelocity(), 2) * Math.sin(2.0*getDirection()) ) / G;
	}
	
	/**
	 * Variable registering the gravitational constant.
	 */
	protected final double G = 9.80665;
	
	/**
	 * Return the mass of this jumpable entity. The mass of this jumpable entity is equal to the density multiplied by four,
	 * 		pi, the radius to the third power and divided by three.
	 * 
	 * @return density multiplied by four, pi, the radius to the third power and divided by three.
	 * 		 | result == (getDensity() * (4.0/3 * Math.PI * Math.pow(getRadius(), 3)))
	 */
	public double getMass() {
		return (getDensity() * (4.0/3 * Math.PI * Math.pow(getRadius(), 3)));
	}

}
