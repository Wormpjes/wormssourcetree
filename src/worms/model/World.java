package worms.model;

import java.util.*;

import be.kuleuven.cs.som.annotate.*;
import worms.model.Position.Position;
import worms.util.Util;

/**
 * A class of Worlds involving a width, height, passable regions, teams, worms,
 * projectiles and food.
 * 
 * @invar The width of this world must be a valid width. 
 * 		| isValidWidth(getWidth())
 * @invar The height of this world must be a valid height.
 *      | isValidHeight(getHeight())
 * @invar All circular entities must be proper circular entities.
 *      | hasProperCircularEntities()
 * @invar All teams must be proper teams. 
 * 		| hasProperTeams()
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 * 
 */

public class World {

	/**
	 * Initialize a new world with the given width, height, passablemap and
	 * random instance.
	 * 
	 * @param width
	 *            The width for this new world.
	 * @param height
	 *            The height for this new world.
	 * @param passableMap
	 *            The matrix with the map of passable regions for this new
	 *            world.
	 * @param random
	 *            The random instance.
	 * @throws IllegalArgumentException
	 *             An IllegalArgumentException is thrown if the width or height
	 *             are not valid for this world. 
	 *       | ! isValidWidth(width) || ! isValidHeigth(height)
	 */
	public World(double width, double height, boolean[][] passableMap,
			Random random) throws IllegalArgumentException {
		if (!isValidWidth(width) || !isValidHeigth(height))
			throw new IllegalArgumentException();
		this.width = width;
		this.height = height;
		this.passableMap = passableMap;
		this.random = random;
	}

	/**
	 * A random instance for creating random numbers.
	 */
	protected Random random;
	
	/**
	 * Variable registering the maximum number of teams allowed in this world.
	 */
	protected final double MAX_NB_TEAMS = 10;
	
	/**
	 * Variable registering the active worm in this world.
	 */
	private Worm activeWorm;
	
	/**
	 * Variable registering the circular entities in this world.
	 */
	private List<CircularEntity> allCircularEntities = new LinkedList<CircularEntity>();

	
	/**
	 * Variable registering the teams in this world.
	 */
	private Set<Team> teams = new HashSet<Team>();
	
	/**
	 * Variable registering the active team in this world.
	 */
	private Team activeTeam;

	/**
	 * Variable registering the map of passable regions for this world
	 */
	private final boolean[][] passableMap;	

	/**
	 * Variable registering whether the game has been started.
	 */
	private boolean gameStarted = false;
	
	/**
	 * Variable registering the active projectile in this world.
	 */
	private Projectile activeProjectile = null;
	
	/**
	 * Variable registering the width of this world.
	 */
	private final double width;

	/**
	 * Variable registering the height of this world.
	 */
	private final double height;
	
	/**
	 * Constant registering the value for which direction to another entity may
	 * differ from the given angle.
	 */
	private final double EPSILONANGLE = 0.05;

	/**
	 * Variable registering the upperboand for the width of this world.
	 */
	private static final double WIDTHUPPERBOUND = Double.MAX_VALUE;
	
	/**
	 * Variable registering the upperbound for the height of this world.
	 */
	private static double HEIGHTUPPERBOUND = Double.MAX_VALUE;

	/**
	 * Return the width of this world.
	 */
	@Basic @Immutable
	public final double getWidth() {
		return width;
	}


	/**
	 * Return the height of this world.
	 */
	@Basic @Immutable
	public final double getHeight() {
		return height;
	}
	
	/**
	 * Set the active projectile in this world to the given projectile.
	 * 
	 * @param projectile
	 *            The projectile to set.
	 */
	protected void setActiveProjectile(Projectile projectile) {
		activeProjectile = projectile;
	}

	/**
	 * Return the active projectile in this world.
	 */
	@Basic
	public Projectile getActiveProjectile() {
		return activeProjectile;
	}
	
	/**
	 * Return all the circular entities in this world.
	 */
	@Basic
	public List<CircularEntity> getAllCircularEntities() {
		return new LinkedList<CircularEntity>(allCircularEntities);
	}

	/**
	 * Return the circular entities of a given type in this world.
	 * 
	 * @param type
	 *            The class of which the returned circular entities should be.
	 * @return A list of all circular entities of the given class. 
	 * 		 | for each object in getAllCircularEntities() 
	 * 		 | if object instanceof type 
	 * 		 |	 then result.add(object)
	 */
	public <Type extends CircularEntity> List<Type> getCircularEntitiesOfType(
			Class<Type> type) {
		List<Type> resultList = new LinkedList<Type>();
		for (CircularEntity object : getAllCircularEntities())
			if (type.isInstance(object))
				resultList.add(type.cast(object));
		return resultList;
	}
	
	/**
	 * Return the name of the winning worm or winning team and null if there is
	 * no winner yet.
	 * 
	 * @return If there is only one worm left than that worm's name is returned.
	 *         | if ( wormList.size() == 1) 
	 *         | 	then return wormList.get(0).getName() 
	 *         Otherwise it returns null if there are at least two worms with a different team name,
	 *         and the team name if all remaining worms are in the same team. 
	 *         | if (for each worm in wormList: worm.getTeamName() == firstWormTeamName) 
	 *         | 	then return firstWormTeamName 
	 *         | else 
	 *         | 	return null
	 */
	public String getWinner() {
		LinkedList<Worm> wormList = (LinkedList<Worm>) getCircularEntitiesOfType(Worm.class);
		if (wormList.size() == 1)
			return wormList.get(0).getName();
		else {
			String firstWormTeamName = wormList.get(0).getTeamName();
			for (Worm worm : wormList)
				if (worm.getTeamName() != firstWormTeamName)
					return null;
			return firstWormTeamName;
		}
	}

	/**
	 * Return the map of passable regions for this world.
	 */
	@Basic
	@Immutable
	private final boolean[][] getPassableMap() {
		return passableMap;
	}

	/**
	 * Return the position that is in the middle of the map.
	 * 
	 * @return A new position with as x-coordinate the width of this world
	 *         divided by two and as y-coordinate the height of this world
	 *         divided by two. 
	 *       | new Position(getWidth() / 2.0, getHeight() / 2.0)
	 */
	@Immutable
	private final Position getMiddlePosition() {
		return new Position(getWidth() / 2.0, getHeight() / 2.0);
	}

	/**
	 * Return the number of rows in the map of passable regions.
	 * 
	 * @return The length of the array passableMap. 
	 * 		 | passableMap.length
	 */
	@Immutable
	public	final int getNbRows() {
		return passableMap.length;
	}

	/**
	 * Return the number of columns in the map of passable regions.
	 * 
	 * @return The lengths of the first row in the passablemap. 
	 * 		|  passableMap[0].length
	 */
	@Immutable
	protected final int getNbCols() {
		return passableMap[0].length;
	}
	
	/**
	 * Return all teams in this world.
	 */
	@Basic
	protected Set<Team> getAllTeams() {
		return new HashSet<Team>(this.teams);
	}	

	/**
	 * Return the active team in this world.
	 */
	@Basic
	protected Team getActiveTeam() {
		return activeTeam;
	}

	/**
	 * Set the active team in this world to the given team.
	 * 
	 * @param team
	 *            The team to set.
	 * @post The new active team is equal to the given team.
	 *     | new.getActiveTeam() == team
	 */
	private void setActiveTeam(Team team) {
		activeTeam = team;
	}

	/**
	 * Return the active worm in this world.
	 */
	@Basic
	protected Worm getActiveWorm() {
		return activeWorm;
	}

	/**
	 * Return a valid position for this circular entity. A position is valid if
	 * it's adjacent to impassable terrain and the position itself is passable.
	 * 
	 * @param object
	 *            The circular entity for which a valid possition is to be
	 *            found.
	 * @return Returns a new position that is adjacent to impassable terrain on
	 *         a randomly chosen side of the world. 
	 *       | isAdjacent(result, object.getRadius()) == true
	 */
	public Position getValidPosition(CircularEntity object) {
		Position currentPosition = Position.randomPosition(this,
				object.getRadius());
		switch (random.nextInt(4)) {
		case 0: // Left
			currentPosition = new Position(object.getRadius(),
					currentPosition.getY());
			break;
		case 1: // Right
			currentPosition = new Position(getWidth() - object.getRadius(),
					currentPosition.getY());
			break;
		case 2: // Bottom
			currentPosition = new Position(currentPosition.getX(),
					object.getRadius());
			break;
		case 3: // Top
			currentPosition = new Position(currentPosition.getX(), getHeight()
					- object.getRadius());
			break;
		}
		double directionToCenter = currentPosition
				.getDirectionToOtherPosition(getMiddlePosition());
		while ((!isAdjacent(currentPosition, object.getRadius()))
				&& isInsideWorld(currentPosition, object.getRadius())) {
			currentPosition = new Position(currentPosition.getX()
					+ Math.cos(directionToCenter) * object.getRadius(),
					currentPosition.getY());
			currentPosition = new Position(currentPosition.getX(),
					currentPosition.getY() + Math.sin(directionToCenter)
							* object.getRadius());
		}
		if (isAdjacent(currentPosition, object.getRadius()))
			return currentPosition;
		return getValidPosition(object);
	}
	
	/**
	 * Check whether the given width is a valid width for this world.
	 * 
	 * @param width
	 *            The width to check.
	 * @return true if and only if the width is in the range [0, WIDTHUPPERBOUND] 
	 * 		 | result == (width <= WIDTHUPPERBOUND && width >= 0)
	 */
	@Model
	private static boolean isValidWidth(double width) {
		return Util.fuzzyLessThanOrEqualTo(width, WIDTHUPPERBOUND) 
				&& Util.fuzzyGreaterThanOrEqualTo(width, 0);
	}
	
	/**
	 * Check whether the given height is a valid height for this world.
	 * 
	 * @param height
	 *            The height to check.
	 * @return true if and only if the height is in the range [0, HEIGHTUPPERBOUND] 
	 * 		 | result == (height <= HEIGHTUPPERBOUND && height >= 0)
	 */
	@Model
	private static boolean isValidHeigth(double height) {
		return Util.fuzzyLessThanOrEqualTo(height, HEIGHTUPPERBOUND) 
				&& Util.fuzzyGreaterThanOrEqualTo(height, 0);
	}


	/**
	 * Add the given circular entity to this world.
	 * 
	 * @param object
	 *            The circular entity to add.
	 * @throws IllegalArgumentException
	 *             An IllegalArgumentException is thrown if this world can not
	 *             have the given object as one of its objects, or the object
	 *             can not be added to this world. 
	 *         | ! this.canHaveAsCircularEntity(object) || ! this.canAddCircularEntity(object)
	 */
	protected void addCircularEntity(CircularEntity object)
			throws IllegalArgumentException {
		if ((!this.canHaveAsCircularEntity(object)) || (!this.canAddCircularEntity(object)))
			throw new IllegalArgumentException();
		allCircularEntities.add(object);
	}

	/**
	 * Check whether the given circular entity can be added to this world.
	 * 
	 * @param object
	 *            The circular entity to check.
	 * @return false if the game has started and the given object is either a
	 *         worm or a food ration, or if this world already contains this
	 *         object. True otherwise. 
	 *       | result == ! (this.gameStarted == true && (object instanceof Worm || object instanceof Food))
	 */
	protected boolean canAddCircularEntity(CircularEntity object) {
		return !((this.gameStarted && (object instanceof Worm || object instanceof Food)) || this
				.getAllCircularEntities().contains(object));
	}

	/**
	 * Remove the given circular entity from this world.
	 * 
	 * @param object
	 *            The circular entity to remove.
	 * @effect If the given object is the active worm in this world, then the
	 *         next turn is started. 
	 *       | if (object == getActiveWorm()) 
	 *       | 	 then startNextTurn()
	 */
	protected void removeCircularEntity(CircularEntity object) {
		if (object == getActiveWorm())
			startNextTurn();
		allCircularEntities.remove(object);
	}

	/**
	 * Check whether the given circular entity can be part of this world.
	 * 
	 * @param object
	 *            The circular entity to check.
<<<<<<< HEAD
	 * @return 	true if and only if the given object is null
	 * 			or the object can have this world as its world.
	 * 		  |	result == ((object == null) || (object.canHaveAsWorld(this)))
=======
	 * @return true if and only if the object is null or the object can have
	 *         this world as its world.
	 * 		 | return == (object == null) || (object.canHaveAsWorld(this))

>>>>>>> bf398ba4e66705d07444c2588b8d46bbc7f18a32
	 */
	private boolean canHaveAsCircularEntity(CircularEntity object) {
		return (object == null) || (object.canHaveAsWorld(this));
	}

	/**
	 * Check whether this world has proper circular entities.
	 * 
	 * @return true if and only if this world can have all of its circular
	 *         entities as a circular entity. 
	 *       | result == (for each circularentity in this.getAllCircularEntities(): 
	 *       |	  this.canHaveAsCircularEntity(object))
	 */
	@Model
	private boolean hasProperCircularEntities() {
		for (CircularEntity object : this.getAllCircularEntities()) {
			if (!this.canHaveAsCircularEntity(object))
				return false;
			if (object.getWorld() != this)
				return false;
			}
		return true;
	}

	

	/**
	 * Return the circular entity closest to the given position in the given
	 * direction theta.
	 * 
	 * @param position
	 *            The position to check.
	 * @param theta
	 *            The angle to check.
	 * @return The returned circular entity is null if no circular entity was found in the direction theta,
	 * 			and is the closest to the position if there was an entity found.
	 * 		 |	result == null ||
	 * 		 |	( Util.fuzzyEquals(position.getDirectionToOtherPosition(result.getPosition()), theta, EPSILONANGLE)
	 * 		 |		&& for each entity in getAllCircularEntities():
	 * 		 |				result.getDistanceToOtherPosition(position) 
	 * 		 |				<= entity.getPosition().getDistanceToOtherPosition(position) )			
	 */
	protected CircularEntity findCircularEntity(Position position, double theta) {
		double currentShortestDistance = Double.POSITIVE_INFINITY;
		double newDirection;
		double newDistance;
		CircularEntity currentClosestEntity = null;
		for (CircularEntity entity : getAllCircularEntities()) {
			newDistance = position.getDistanceToOtherPosition(entity
					.getPosition());
			newDirection = position.getDirectionToOtherPosition(entity
					.getPosition());
			if (Util.fuzzyEquals(newDirection, theta, EPSILONANGLE)
					&& Util.fuzzyLessThanOrEqualTo(newDistance,
							currentShortestDistance)
					&& !Util.fuzzyLessThanOrEqualTo(newDistance, 0.0)) {
				currentShortestDistance = newDistance;
				currentClosestEntity = entity;
			}
		}
		return currentClosestEntity;
	}

	/**
	 * Add a new empty team with a given name to this world.
	 * 
	 * @param name
	 *            The name for this new team.
	 * @effect	The new team is set as the active team in this world.
	 * 		  |	setActiveTeam(newTeam)
	 */
	protected void addAsTeam(String name) {
		Team newTeam = new Team(this, name);
		teams.add(newTeam);
		setActiveTeam(newTeam);
	}
	
	/**
	 * Check whether this world has the given team as one of its teams.
	 * 
	 * @return	true if and only if the set of teams in this world contains the given team.
	 * 		  |	result == getAllTeams().contains(team)
	 */
	@Raw @Basic
	protected boolean hasAsTeam(Team team){
		return getAllTeams().contains(team);
	}
	
	/**
	 * Return the amount of teams in this world.
	 * 
	 * @return 	The size of the set of teams in this world.
	 * 		  |	getAllTeams().size()
	 */
	protected int getNbTeams(){
		return getAllTeams().size();
	}

	/**
	 * Check whether this world has proper teams.
	 * 
	 * @return true if and only if this world can have each team as a team and
	 *         the teams world is this world. 
	 *       | result == for each team in getAllTeams() 
	 *       | 				team.getWorld() == this
	 */
	@Model
	private boolean hasProperTeams() {
		if (this.getAllTeams().isEmpty())
			return true;
		for (Team team : this.getAllTeams()) {
			if (team.getWorld() != this)
				return false;
		}
		return true;
	}

	/**
	 * Select the next worm as active worm and start that worm's turn.
	 * 
	 * @post The new active worm is the next worm in the list, or the first one
	 *       if the previous active worm was null or the last worm. 
	 *     | if (worms.indexOf(this.getActiveWorm()) == worms.size() - 1 || this.getActiveWorm == null) 
	 *     | 	then worms.indexOf(new.getActiveWorm) == 0 
	 *     | 	else 
	 *     | 		then worms.indexOf(new.getActiveWorm) == worms.indexOf(this.getActiveWorm()) + 1
	 * @effect The active worm's turn is started. 
	 * 	   | activeWorm.startTurn()
	 */
	private void selectNextWorm() throws IllegalStateException {
		List<Worm> worms = getCircularEntitiesOfType(Worm.class);
		if (worms.size() == 0)
			throw new IllegalStateException("No worms in this world.");
		int activeWormIndex = worms.indexOf(activeWorm);
		if (activeWormIndex == -1 || activeWormIndex == worms.size() - 1)
			activeWorm = worms.get(0);
		else
			activeWorm = worms.get(activeWormIndex + 1);
		activeWorm.startTurn();
	}

	/**
	 * Check whether the given position with a given radius is adjacent to
	 * impassable terrain.
	 * 
	 * @param position
	 *            The position to check.
	 * @param radius
	 *            The radius for which adjacency is to be checked.
	 * @return true if and only if the circular area with radius 1.1 times the
	 *         given radius around the given position is impassable and the
	 *         circular area with the given radius around the given position is not impassable. 
	 *       | result == ( isImpassable(position, 1.1*radius) && isPassable(position, radius) )
	 */
	public boolean isAdjacent(Position position, double radius) {
		return isImpassable(position, 1.1 * radius)
				&& isPassable(position, radius);
	}

	/**
	 * Return whether the given position is passable.
	 * 
	 * @param position
	 *            The position to check.
	 * @param radius
	 *            The radius to check.
	 * @return Returns true if and only if the area defined by the given
	 *         position and radius is not impassable and false if and only if
	 *         the position is impassable. 
	 *       | result == ! isImpassable(position, radius)
	 */
	public boolean isPassable(Position position, double radius) {
		return ! isImpassable(position, radius);
	}

	/**
	 * Check whether the given position with the given radius contains
	 * impassable terrain.
	 * 
	 * @param position
	 *            The position to check.
	 * @param radius
	 *            The radius for which impassability is checked.
	 * @return True if and only if at least one of the pixels that are
	 *         overlapped by the circular area defined by the given position and
	 *         radius, is marked as 'false' in the passableMap of this world.
	 *         Returns false if no such overlapping pixel is found. 
	 *       | result == for some pixel in passableMap 
	 *       |		position.areaOverlapsWithPixel(this, pixelRow, pixelCol, radius)
	 *       | 		&& passableMap[pixelRow][pixelCol] == false
	 */
	public boolean isImpassable(Position position, double radius) {
		Position leftUpperBound = new Position(position.getX() - radius,
				position.getY() + radius);
		int minPixelCol = leftUpperBound.getPixelCol(this) - 1;
		int minPixelRow = leftUpperBound.getPixelRow(this) - 1;
		Position rightLowerBound = new Position(position.getX() + radius,
				position.getY() - radius);
		int maxPixelCol = rightLowerBound.getPixelCol(this) + 1;
		int maxPixelRow = rightLowerBound.getPixelRow(this) + 1;
		for (int pixelCol = minPixelCol; pixelCol <= maxPixelCol; pixelCol++) {
			for (int pixelRow = minPixelRow; pixelRow <= maxPixelRow; pixelRow++) {
				try {
					if (! passableMap[pixelRow][pixelCol]
						&& position.areaOverlapsWithPixel(this, pixelRow, pixelCol, radius))
						return true;
				} catch (IndexOutOfBoundsException exc) {
				}
			}
		}
		return false;
	}

	/**
	 * Return the width of a pixel.
	 * 
	 * @return The width of the world divided by the number of columns. 
	 *       | getWidth()/getNbCols()
	 */
	public final double pixelWidth() {
		return getWidth() / getNbCols();
	}

	/**
	 * Return the height of a pixel.
	 * 
	 * @return The height of the world divided by the number of rows.
	 *       | getHeight()/getNbRows()
	 */
	public final double pixelHeight() {
		return getHeight() / getNbRows();
	}

	/**
	 * Check whether the given position with the given radius is inside of the
	 * world.
	 * 
	 * @param position
	 *            The position to check.
	 * @param radius
	 *            The radius for which the position is checked.
	 * @return true if and only if the x-coordinate of the given position is in
	 *         the range [radius, getWidth() - radius] and the y-coordinate of
	 *         the given position is in the range [radius, getHeight - radius] 
	 *       | result == (position.getX() >= radius) && (position.getX() <= getWidth() - radius) 
	 *       | && (position.getY() >= radius) && (position.getY() <= getHeight() - radius)
	 */
	protected boolean isInsideWorld(Position position, double radius) {
		return (Util.fuzzyGreaterThanOrEqualTo(position.getX(), radius)
				&& Util.fuzzyLessThanOrEqualTo(position.getX(),
						(getWidth() - radius))
				&& Util.fuzzyGreaterThanOrEqualTo(position.getY(), radius) && Util
					.fuzzyLessThanOrEqualTo(position.getY(),
							(getHeight() - radius)));
	}

	/**
	 * Start the game.
	 * 
	 * @effect If this world contains less than two worms, then one or two are
	 *         added to make the total number of worms two. | for (int i =
	 *         getCircularEntitiesOfType(Worm.class).size(); i < 2; i++) | new
	 *         Worm(this);
	 * @effect The next (in this case the first) worm is selected. |
	 *         selectNextWorm()
	 * @throws IllegalStateException
	 *           An IllegalStateException is thrown if the game can not start.
	 *       | ! canStart()
	 */
	public void startGame() throws IllegalStateException {
		for (int i = getCircularEntitiesOfType(Worm.class).size(); i < 2; i++) {
			new Worm(this);
		}
		if (! canStart())
			throw new IllegalStateException();
		this.gameStarted = true;
		selectNextWorm();
	}

	/**
	 * Check whether the game can start.
	 * 
	 * @return true if and only if the world contains only worms and food, and
	 *         this world contains at least two worms. 
	 *       | result == for object in getAllCircularEntities() 
	 *       | 				(object instanceof Worm) || (object instanceof Food) 
	 *       | 			&& getCircularEntitiesOfType(Worm.class).size() >= 2
	 * 
	 */
	private boolean canStart() {
		for (CircularEntity object : getAllCircularEntities())
			if ( ! (object instanceof Worm || object instanceof Food) )
				return false;
		return getCircularEntitiesOfType(Worm.class).size() >= 2;
	}

	/**
	 * Check whether the game is finished.
	 * 
	 * @return true if and only if the winner is not null. 
	 * 		 | result == (getWinner() != null)
	 */
	public boolean isGameFinished() {
		return getWinner() != null;
	}

	/**
	 * Start the next turn.
	 * 
	 * @effect The next worm is selected. 
	 * 		 | selectNextWorm()
	 * @effect If the active projectile in this world is not null, then the
	 *         active projectile is terminated. 
	 *       | if (activeProjectile != null)
	 *       | 	 then activeProjectile.terminate()
	 */
	public void startNextTurn() {
		if (activeProjectile != null)
			activeProjectile.terminate();
		selectNextWorm();
	}


}
