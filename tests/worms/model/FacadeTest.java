package worms.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.gui.game.IActionHandler;
import worms.model.programs.ParseOutcome;
import worms.model.programs.ParseOutcome.Success;
import worms.util.Util;

/**
 * A test case for testing Facade methods that aren't (fully) covered in the other tests.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class FacadeTest {
	
	private static final double EPS = Util.DEFAULT_EPSILON;

	private IFacade facade;

	private Random random;

	private World world;
	
	private double timeStep = 0.0001;

	// X X X X X
	// X . . . .
	// X . . . .
	// X . . . .
	// X X X X X
	private boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false, false }, 
			{ false, true, true, true, true },
			{ false, true, true, true, true },
			{ false, true, true, true, true },
			{false, false, false, false, false }};

	@Before
	public void setup() {
		facade = new Facade();
		random = new Random(7357);
		world = facade.createWorld(5.0, 5.0, passableMap, random);
	}
	private Worm worm1;


	@Before
	/**
	 * Initialize worms:
	 */
	public void setUp() throws Exception {
		worm1 = facade.createWorm(world, 1.26, 1.26, (Math.PI/2), 0.25, "Test1", null);
	}
	
	@Test
	public void testJumpStep() {
		facade.startNextTurn(world);
		worm1.selectNextWeapon();
		Projectile projectile = new Projectile(worm1.getActiveWeapon(),50);
		assertTrue(Util.fuzzyEquals(facade.getRadius(projectile),0.02094));
		assertTrue(Util.fuzzyEquals(facade.getJumpTime(projectile, EPS),0.29,0.003));
		//fuzzyEquals aangepast zodat posities kunnen vergeleken worden.
		assertTrue(Util.fuzzyEquals(facade.getJumpStep(projectile, 
				facade.getJumpTime(projectile, EPS)),new double[] {1.26, 3.99}));
		
	}
	
	@Test(expected = ModelException.class)
	public void testJumpStep2() {
		IActionHandler handler = new SimpleActionHandler(facade);
		World world2 = facade.createWorld(100.0, 100.0, new boolean[][] { {true}, {false} }, random);
	    ParseOutcome<?> outcome = facade.parseProgram("double x; while (x < 1.5) {\nx := x + 0.1;\n}\n turn x;", handler);
		assertTrue(outcome.isSuccess());
		Program program = ((Success)outcome).getResult();
		Worm worm3 = facade.createWorm(world2, 50.0, 50.51, 0, 0.5, "Test", null);
		facade.addNewWorm(world2, null);
		facade.startGame(world2);
		facade.jump(worm3, timeStep);
		facade.getJumpStep(worm3, timeStep);
	}
	
	@Test
	public void testParseProgramErrors() {
		IActionHandler handler = new SimpleActionHandler(facade);
		String programText = "double x; while (x < 1.5) {\nx := x + 0.1;\n}\n turn x"; //missing ";" at the end of the program
		ParseOutcome<?> outcome = facade.parseProgram(programText, handler);
		assertFalse(outcome.isSuccess());
		
	}
	
	@Test
	public void testParseProgramEmptyString() {
		IActionHandler handler = new SimpleActionHandler(facade);
		String programText = "";
		ParseOutcome<?> outcome = facade.parseProgram(programText, handler);
		assertFalse(outcome.isSuccess());
	}
	
	@Test
	public void testProgramNotWellformed() {
		IActionHandler handler = new SimpleActionHandler(facade);
		String programText = "double x;\n entity w;\n foreach(worm, w) do {\nfire 100;\n} ;";
		ParseOutcome<?> outcome = facade.parseProgram(programText, handler);
		assertTrue(outcome.isSuccess());
		assertFalse(((Program)outcome.getResult()).isWellFormed());
	}
}
