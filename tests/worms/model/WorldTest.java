
package worms.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.*;

import worms.model.Position.Position;
import worms.util.Util;

/**
 * A test case for testing a world's public methods.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
public class WorldTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	Random random = new Random(7357);
	
	Facade facade = new Facade();
	
	// X X X X
	// . . . .
	// . . . .
	// X X X X
	boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false }, 
			{ true, true, true, true },
			{ true, true, true, true }, 
			{ false, false, false, false } };
	
	World world1 = new World(4, 4, passableMap, random);
	World world2 = new World(4, 4, passableMap, random);

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	private Worm wormRadius1;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		wormRadius1 = new Worm(world1, new Position(2,2), 0, 1, "Radius1", null);
		world1.addAsTeam("TeamAlpha");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Create a world with valid arguments.
	 */
	@Test
	public void test_Initialize_World_Valid_Case() {
		facade.createWorld(10, 10, passableMap, random);
	}
	
	/**
	 * Create a world with a negative width.
	 */
	@Test(expected = ModelException.class)
	public void test_Initialize_World_Invalid_Width() {
		facade.createWorld(-10, 10, passableMap, random);
	}
	
	/**
	 * Create a world with a negative height.
	 */
	@Test(expected = ModelException.class)
	public void test_Initialize_World_Invalid_Height() {
		facade.createWorld(10, -10, passableMap, random);
	}
	
	/**
	 * Get a valid position for a worm with radius 1.
	 */
	@Test
	public void test_Get_Valid_Position() {
		Position validPosition = world1.getValidPosition(wormRadius1);
		assertTrue(world1.isAdjacent(validPosition, wormRadius1.getRadius()));
	}
	
	/**
	 * Get the winner of the game: the winner is the only worm alive.
	 */
	@Test
	public void test_Get_Winner_Single_Worm() {
		assertEquals(wormRadius1.getName(), world1.getWinner());
	}
	
	/**
	 * Get the winner of the game: the winner is the only team with alive worms.
	 */
	@Test
	public void test_Get_Winner_Team() {
		facade.addEmptyTeam(world2, "WinningTeam");
		facade.startGame(world2);
		assertTrue(facade.isGameFinished(world2));
		assertEquals("WinningTeam", facade.getWinner(world2));
	}
	
	/**
	 * Get the winner of the game: no winner yet.
	 */
	@Test
	public void test_Get_Winner_No_Winner() {
		facade.startGame(world2);
		assertFalse(facade.isGameFinished(world2));
		assertEquals(null, facade.getWinner(world2));
	}
	
	/**
	 * No winner, but two worms from the same team.
	 */
	@Test
	public void test_Get_Winner_No_Winner_2() {
		facade.addEmptyTeam(world2, "TeamOne");
		facade.addNewWorm(world2, null);
		facade.addNewWorm(world2, null);
		facade.addEmptyTeam(world2, "TeamTwo");
		facade.addNewWorm(world2, null);
		assertFalse(facade.isGameFinished(world2));
		assertEquals(null, facade.getWinner(world2));
	}
	
	/**
	 * Try to add the worm to the world when it is already in the world.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void test_Add_Circular_Entity_Invalid_Case() {
		world1.addCircularEntity(wormRadius1);
	}
	
	/**
	 * Add a new team to the world, valid case: new team with name "TeamBeta".
	 */
	@Test
	public void test_Add_Team_Valid_Case() {
		facade.addEmptyTeam(world1, "TeamBeta");
	}
	
	/**
	 * Add a new team to the world, invalid case: 
	 * team with the same name ("TeamAlpha") already exists in the world.
	 */
	@Test(expected = ModelException.class)
	public void test_Add_Team_Already_Exists() {
		facade.addEmptyTeam(world1, "TeamAlpha");
	}
	
	/**
	 * Try to add a new team with an invalid name.
	 */
	@Test(expected = ModelException.class)
	public void test_Add_Team_Invalid_Name() {
		facade.addEmptyTeam(world1, "Team1");
	}
	
	/**
	 * Try to add a team when the maximum is reached.
	 */
	@Test(expected = ModelException.class)
	public void test_Add_Team_Maximum_Reached() {
		facade.addEmptyTeam(world1, "TeamB");
		facade.addEmptyTeam(world1, "TeamC");
		facade.addEmptyTeam(world1, "TeamD");
		facade.addEmptyTeam(world1, "TeamE");
		facade.addEmptyTeam(world1, "TeamF");
		facade.addEmptyTeam(world1, "TeamG");
		facade.addEmptyTeam(world1, "TeamH");
		facade.addEmptyTeam(world1, "TeamI");
		facade.addEmptyTeam(world1, "TeamJ");;
		facade.addEmptyTeam(world1, "TeamTooMuch");
	}
	
	/**
	 * Try to add a worm after the game started.
	 */
	@Test(expected = ModelException.class)
	public void test_Add_Worm_After_Game_Started() {
		facade.startGame(world1);
		facade.createWorm(world1, 10, 10, 0, 1, "I'm too late", null);
	}
	
	/**
	 * Try to add food after the game started.
	 */
	@Test(expected = ModelException.class)
	public void test_Add_Food_After_Game_Started() {
		facade.startGame(world1);
		facade.addNewFood(world1);
	}
	
	/**
	 * Test if the game can start with a projectile inside.
	 */
	@Test(expected = ModelException.class)
	public void test_Start_Game_With_Projectile() {
		facade.selectNextWeapon(wormRadius1);
		facade.shoot(wormRadius1, 0);
		facade.startGame(world1);
	}
	
	/**
	 * Get the active projectile.
	 */
	@Test
	public void test_Get_Active_Projectile() {
		facade.selectNextWeapon(wormRadius1);
		facade.shoot(wormRadius1, 0);
		assertTrue(Util.fuzzyEquals(facade.getActiveProjectile(world1).getRadius(),0.02094));
		assertEquals(world1.getCircularEntitiesOfType(Projectile.class).get(0), 
					facade.getActiveProjectile(world1));
	}
	
	/**
	 * Get the active worm.
	 */
	@Test
	public void test_Get_Active_Worm() {
		facade.startGame(world1);
		assertEquals(wormRadius1, facade.getCurrentWorm(world1));
	}
	
	/**
	 * Start the next turn.
	 */
	@Test
	public void test_Start_Next_Turn() {
		new Worm(world1);
		facade.startGame(world1);
		facade.startNextTurn(world1);
		assertEquals(world1.getCircularEntitiesOfType(Worm.class).get(1), world1.getActiveWorm());
	}
	
	/**
	 * Start the next turn when a projectile is alive.
	 */
	@Test
	public void test_Start_Next_Turn_With_Projectile() {
		facade.selectNextWeapon(wormRadius1);
		facade.shoot(wormRadius1, 10);
		assertTrue(facade.getActiveProjectile(world1) != null);
		facade.startNextTurn(world1);
		assertEquals(null, facade.getActiveProjectile(world1));
	}
	
	/**
	 * Remove the worm with radius 1 from the world.
	 */
	@Test
	public void test_Remove_Circular_Entity() {
		world1.removeCircularEntity(wormRadius1);
		assertFalse(world1.getAllCircularEntities().contains(wormRadius1));
	}
	
	/**
	 * Remove the active worm from the world.
	 */
	@Test
	public void test_Remove_Active_Worm() {
		Worm worm2 = new Worm(world1);
		facade.startGame(world1);
		world1.removeCircularEntity(wormRadius1);
		assertEquals(worm2, world1.getActiveWorm());
	}
	
	@Test
	public void testFood1() {
		Position position = new Position(2.0,2.5);
		Food food = new Food(world1,position);
		assertTrue(facade.getX(food) == 2.0);
		assertTrue(facade.getY(food) == 2.5);
		assertTrue(facade.getFood(world1).contains(food));
		assertTrue(facade.getRadius(food) == 0.20);
		assertTrue(facade.isActive(food));
		food.terminate();
		assertTrue(!facade.isActive(food));
	}
	
	@Test
	public void testFood2() {
		facade.createFood(world1, 2.0, 2.0);
		facade.addNewFood(world1);
	}
	
}
