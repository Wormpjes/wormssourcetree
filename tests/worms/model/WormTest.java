package worms.model;
 
import static org.junit.Assert.*;

import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import worms.gui.game.IActionHandler;
import worms.model.programs.ParseOutcome;
import worms.model.programs.ParseOutcome.Success;
import worms.util.Util;

/**
 * A test case for testing a worm's public methods.
 * 
 * @author Olivier Kamers and Christof Luyten
 * 
 * @version 1.0
 *
 */
@SuppressWarnings("unused")
public class WormTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	private Random random = new Random(7357);
	
	private static final double EPS = Util.DEFAULT_EPSILON;
	
	// X X X X
	// . . . .
	// . . . .
	// X X X X
	boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false }, 
			{ true, true, true, true },
			{ true, true, true, true }, 
			{ false, false, false, false } };
	
	boolean[][] passableMapTrue = new boolean[][] {
			{ true, true, true, true }, 
			{ true, true, true, true },
			{ true, true, true, true }, 
			{ true, true, true, true } };
	
	Facade facade = new Facade();
	
	World world1 = new World(4, 4, passableMap, random);
	
	World world2 = new World(4, 4, passableMapTrue, random);
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	private Worm WormJoske;
	private Worm WormFred_FacingDown;
	private Worm WormAlbert_FacingUp;
	private Worm WormInvalidMove;
	private Worm WormDirectionZero;
	private Worm WormFall;
	private Worm WormOut;
	private Worm WormZeroPosition;
	private World WorldNull = null;
	
	private double timeStep = 0.0001;

	@Before
	/**
	 * Initialize worms:
	 * 		WormInvalidMove is a worm with not enough APs to move
	 * 		WormJoske is a worm at position (5.0, -10.3) with direction Pi (facing left) and radius 0.25
	 * 		WormFred_FacingDown is a worm at position (5.0, -10.3) with direction 3/2*pi (facing down) and radius 0.50
	 * 		WormAlbert_FacingUp is a worm at position (0, 0) with direction 2.1048 and radius 0.5
	 * 		WormDirectionZero is a worm at position (1,1) with direction 0 and radius 0.5
	 * 		WormFall is a worm in world2 at position (2,2), direction 0 and radius 0.5
	 * 		WormOut is a worm is world2 at a position outside the world: (-2,-2)
	 */
	public void setUp() throws Exception {
		WormInvalidMove = facade.createWorm(world1, 1.4, 2.5, 2.1048, 0.25, "I have little APs", null);
		WormJoske = facade.createWorm(world1, 1.4, 2.5, Math.PI, 0.25, "Joske", null);
		WormFred_FacingDown = facade.createWorm(world1, 3.0, 1.3, 3.0/2*Math.PI, 0.50,"FacingDown", null);
		WormAlbert_FacingUp = facade.createWorm(world1, 1.4, 2.5, 2.1048, 0.5, "FacingUp", null);
		WormDirectionZero = facade.createWorm(world1, 1, 1, 0, 0.5, "ZeroDirection", null);
		WormFall = facade.createWorm(world2, 2, 2, 0, 0.5, "Fall", null);
		WormOut = facade.createWorm(world2, -2, -2, 0, 0.5, "Out", null);
		WormZeroPosition = facade.createWorm(world1, 0, 0, 0, 0.5, "ZeroPosition", null);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
// **************************Initialisation worm**************************
	
	/**
	 * Test for initializing a valid worm.
	 */
	@Test
	public final void test_Initialize_Worm_Valid() {
		world1.addAsTeam("Superteam");
		final Worm WormValid = facade.createWorm(world1,1.0, 2.0, 3.0, 0.3, "I'm Valid", null);
		assertTrue(WormValid.getWorld()==world1);
		assertTrue(Util.fuzzyEquals(1.0, WormValid.getX()));
		assertTrue(Util.fuzzyEquals(2.0, WormValid.getY()));
		assertTrue(Util.fuzzyEquals(3.0, facade.getOrientation(WormValid)));
		assertTrue(Util.fuzzyEquals(0.3, facade.getRadius(WormValid)));
		assertTrue(facade.getMaxActionPoints(WormValid)== facade.getActionPoints(WormValid));
		assertTrue(facade.getMaxHitPoints(WormValid)== facade.getHitPoints(WormValid));
		assertTrue(facade.getName(WormValid)=="I'm Valid");	
		assertTrue(Util.fuzzyEquals((1062.0 * (4.0/3 * Math.PI * Math.pow(WormValid.getRadius(), 3))), WormValid.getMass()));
		assertTrue(facade.getTeamName(WormValid)=="Superteam");
	}
	
	/**
	 * Test for initializing a valid worm.
	 */
	@Test
	public final void test_Initialize_Worm_Valid2() {
		final Worm WormValid = new Worm(world1);
		assertTrue(WormValid.hasProperWorld());
		assertTrue(Worm.isValidCoordinate(WormValid.getX()));
		assertTrue(Worm.isValidCoordinate(WormValid.getY()));
		assertTrue(Worm.isValidDirection(WormValid.getDirection()));
		assertTrue(WormValid.isValidRadius(WormValid.getRadius()));
		assertTrue(WormValid.getMaxAPs()== WormValid.getAPs());
		assertTrue(WormValid.getMaxHPs()== WormValid.getHPs());
		assertTrue(WormValid.getTeamName()==null);		
	}
	
	/**
	 * Test for initializing a worm in an invalid world (null).
	 */
	@Test(expected = ModelException.class)
	public final void test_Initialize_Worm_Invalid_World() {
		final Worm WormInvalidX =facade.createWorm(WorldNull,2.0, 2.0, 3.0, 0.3,  "I'm Invalid", null);
	}
	
	/**
	 * Test for initializing a worm with an invalid X-coordinate.
	 */
	@Test(expected = ModelException.class)
	public final void test_Initialize_Worm_Invalid_X_Coordinate() {
		final Worm WormInvalidX =facade.createWorm(world1, Double.NaN, 2.0, 3.0, 0.3,  "I'm Invalid", null);
	}
	
	/**
	 * Test for initializing a worm with an invalid Y-coordinate.
	 */
	@Test(expected = ModelException.class)
	public final void test_Initialize_Worm_Invalid_Y_Coordinate() {
		final Worm WormInvalidY = facade.createWorm(world1, 1.0, Double.NaN, 3.0, 0.3 , "I'm Invalid", null);
	}
	
	/**
	 * Test for initializing a worm with an negative direction.
	 */
	@Test(expected = ModelException.class)
	public final void test_Initialize_Worm_Invalid_Direction1() {
		final Worm WormInvalidDirection1 = facade.createWorm(world1, 1.0, 2.0, -2.1048, 0.3, "I'm Invalid", null);
	}
		
	/**
	 * Test for initializing a worm with an direction equal to 2*PI.
	 */
	@Test(expected =ModelException.class)
	public final void test_Initialize_Worm_Invalid_Direction2() {
		final Worm WormInvalidDirection2 = facade.createWorm(world1,1.0, 2.0, 2*Math.PI, 0.3, "I'm Invalid", null);
	}
	
	/**
	 * Test for initializing a worm with an direction equal to NaN.
	 */
	@Test(expected =ModelException.class)
	public final void test_Initialize_Worm_Invalid_Direction3() {
		final Worm WormInvalidDirection3 = facade.createWorm(world1,1.0, 2.0, Double.NaN, 0.3, "I'm Invalid", null);
	}
	
	/**
	 * Test for initializing a worm with a radius smaller than the minimal radius.
	 */
	@Test(expected = ModelException.class)
	public final void test_Initialize_Worm_Invalid_Radius1() {
		final Worm WormInvalidRadius1 = facade.createWorm(world1,1.0, 2.0, 3.0, 0.0, "I'm Invalid", null);
	}
	
	/**
	 * Test for initializing a worm with a radius equal to infinity.
	 */
	@Test(expected = ModelException.class)
	public final void test_Initialize_Worm_Invalid_Radius2() {
		final Worm WormInvalidRadius2 = facade.createWorm(world1,1.0, 2.0, 3.0, Double.POSITIVE_INFINITY, "I'm Invalid", null);
	}
	
	/**
	 * Test for initializing a worm with an invalid name 1:
	 * 		name doesn't start with capital letter.
	 */
	@Test(expected = ModelException.class)
	public final void test_Initialize_Worm_Invalid_Name1() {
		final Worm WormInvalidName1 = facade.createWorm(world1,1.0, 2.0, 3.0, 0.3, "james o'Hara", null);
	}
	
	/**
	 * Test for initializing a worm with an invalid name 2:
	 * 		name contains invalid character.
	 */
	@Test(expected = ModelException.class)
	public final void test_Initialize_Worm_Invalid_Name2() {
		final Worm WormInvalidName2 = facade.createWorm(world1,1.0, 2.0, 3.0, 0.3, "james o^Hara", null);
	}
	
// ****************setRadius****************
	/**
	 * Test for setRadius and checking the action points.
	 */
	@Test
	public final void test_setRadius1() {
		WormDirectionZero.setRadius(0.4);
		assertEquals(WormDirectionZero.getMaxAPs(), WormDirectionZero.getAPs());
	}

//	****************setHP****************
	
	/**
	 * Test for setting the HPs of the worm to a negative amount. The worm must be terminated.
	 */
	@Test
	public void test_Set_HPs_Negative_Amount() {
		WormJoske.setHPs(-5);
		assertFalse(facade.isAlive(WormJoske));
	}
	
// ****************move****************
	/**
	 * Test for the method to move the worm, valid case.
	 */
	@Test
	public final void test_Move() {
		assertTrue(facade.canMove(WormJoske));
		facade.move(WormJoske);
		assertTrue(Util.fuzzyEquals(1.15, WormJoske.getX()));
		assertTrue(Util.fuzzyEquals(2.5, WormJoske.getY()));
		assertTrue(Util.fuzzyEquals(69, WormJoske.getAPs()));
		assertTrue(Util.fuzzyEquals(3.141592653589793, WormJoske.getDirection()));
	}
	
	@Test
	public final void test_MoveOut() {
		facade.turn(WormZeroPosition, 3.14);
		facade.move(WormZeroPosition);
		assertTrue(WormZeroPosition.isTerminated());

		assertTrue(Util.fuzzyEquals(3.141592653589793, WormJoske.getDirection()));
	}
	
	@Test (expected = ModelException.class)
	public final void test_Move1() {
		facade.jump(WormJoske, timeStep);
		facade.move(WormJoske);
	}

// ****************turn****************
	
	/**
	 * Test for the method to turn the worm over a given angle, valid case 1:
	 * 		turn the worm over a positive angle,
	 * 		starting from a positive angle and ending at a positive angle.
	 */
	@Test
	public final void test_Turn_PositiveAngle() {
		final double testAngle = 1;
		assertTrue(WormJoske.canTurn(testAngle));
		WormJoske.turn(testAngle);
		assertTrue(Util.fuzzyEquals(4.1415926535, WormJoske.getDirection()));
		assertEquals(60, WormJoske.getAPs(), Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Test for the method to move the worm over a given angle, valid case 2:
	 * 		turn the worm over a negative angle, 
	 * 		starting from a positive angle and ending at a positive angle.
	 */
	@Test
	public final void test_Turn_NegativeAngle() {
		final double testAngle = -1;
		assertTrue(WormJoske.canTurn(testAngle));
		WormJoske.turn(testAngle);
		assertTrue(Util.fuzzyEquals(2.1415926535, WormJoske.getDirection()));
		assertEquals(60, WormJoske.getAPs(), Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Test for the method to move the worm over a given angle, valid case 3:
	 * 		turn the worm over a negative angle,
	 * 		starting from a positive angle and ending at a 'negative' angle (will be positive after correction by 2*Pi)
	 */
	@Test
	public final void test_Turn_Positive_To_Negative_Angle() {
		final double testAngle = -4.0;
		assertTrue(WormJoske.canTurn(testAngle));
		WormJoske.turn(testAngle);
		assertTrue(Util.fuzzyEquals(5.424777960760379, WormJoske.getDirection()));
		assertEquals(31, WormJoske.getAPs(), Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Test for the method to move the worm over a given angle, valid case 4:
	 * 		turn the worm over a positive angle,
	 * 		starting from a positive angle and ending at a angle greater dan 2*PI.
	 */
	@Test
	public final void test_Turn_Negative_To_Positive_Angle() {
		final double testAngle = 3.0/2*Math.PI;
		assertTrue(WormFred_FacingDown.canTurn(testAngle));
		WormFred_FacingDown.turn(testAngle);
		assertTrue(Util.fuzzyEquals(3.1415926535, WormFred_FacingDown.getDirection()));
		assertEquals(511, WormFred_FacingDown.getAPs(), Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Test for the method to move the worm over a given angle, invalid case:
	 * 		the worm has zero action points.
	 */
	@Test(expected = AssertionError.class)
	public final void test_Turn_Not_Enough_APs() {
		final double testAngle = 3.0/2*Math.PI;
		facade.selectNextWeapon(WormInvalidMove);
		facade.shoot(WormInvalidMove, 100);
		facade.turn(WormInvalidMove, testAngle);
	}
	
// ****************jump****************	
	
	/**
	 * Test for the method to jump the worm, valid case 1:
	 * 		direction is pi.
	 */
	@Test
	public final void test_Jump_Direction_Up() {
		facade.turn(WormJoske, -1.14);
		facade.jump(WormJoske, timeStep);
		assertEquals(0, WormJoske.getAPs());
		assertTrue(Util.fuzzyEquals(1.2929175909577435, facade.getX(WormJoske)));
		assertTrue(Util.fuzzyEquals(2.7264654960997285, facade.getY(WormJoske)));
	}
	
	/**
	 * Test for the method to jump the worm, invalid case 2:
	 * 		worm is facing up, but has zero action points.
	 */
	@Test(expected = ModelException.class)
	public final void test_Jump_Zero_APs() {
		facade.jump(WormAlbert_FacingUp,timeStep);
		facade.jump(WormAlbert_FacingUp,timeStep);
		facade.getJumpStep(WormAlbert_FacingUp, timeStep);
	}
	
// **************************jumpTime*******************************	
	
	/**
	 * Test for the method to get the jumptime.
	 */
	@Test
	public final void test_JumpTime1() {
		assertTrue(Util.fuzzyEquals(0.0705, facade.getJumpTime(WormAlbert_FacingUp, timeStep)));
	}
	
	/**
	 * Test for the method to get the jumptime with zero action points.
	 */
	@Test
	public final void test_JumpTime2() {
		facade.jump(WormAlbert_FacingUp,timeStep);
		assertTrue(facade.getJumpTime(WormAlbert_FacingUp, timeStep)==0);
	}	
	
	/**
	 * Test for the method to get the jumptime
	 */
	@Test(expected = ModelException.class)
	public final void test_JumpTime3() {
		Worm worm = null;
		facade.getJumpTime(worm, timeStep);
	}	

//********************************jumpstep********************************
	
	/**
	 * Test for the method to get the jumpstep.
	 */
	@Test
	public final void test_JumpStep1() {
		double[] tempPosition =facade.getJumpStep(WormAlbert_FacingUp, (WormAlbert_FacingUp.jumpTime(timeStep))/10);
		assertTrue(Util.fuzzyEquals(tempPosition,new double[] {1.3734730692286536, 2.5446184413650506}));
	}
	
	/**
	 * Test for the method to get the jumpstep with zero action points.
	 */
	@Test(expected = ModelException.class)
	public final void test_JumpStep2() {
		facade.jump(WormAlbert_FacingUp, timeStep);
		facade.getJumpStep(WormAlbert_FacingUp,0.0001);
	}

//********************************selectNextWeapon********************************
	
	@Test
	public final void test_selectNextWeapon1() {
		assertTrue(facade.getSelectedWeapon((WormAlbert_FacingUp)) == "Rifle");
	}
	
	@Test
	public final void test_selectNextWeapon2() {
		facade.selectNextWeapon(WormAlbert_FacingUp);
		assertTrue(facade.getSelectedWeapon((WormAlbert_FacingUp))=="Bazooka");
	}
	
	@Test
	public final void test_selectNextWeapon3() {
		facade.selectNextWeapon(WormAlbert_FacingUp);
		facade.selectNextWeapon(WormAlbert_FacingUp);
		assertTrue(facade.getSelectedWeapon((WormAlbert_FacingUp))=="Rifle");
	}
	
	@Test
	public final void test_selectNextWeapon4() {
		facade.selectNextWeapon(WormAlbert_FacingUp);
		facade.selectNextWeapon(WormAlbert_FacingUp);
		facade.selectNextWeapon(WormAlbert_FacingUp);
		assertTrue(facade.getSelectedWeapon((WormAlbert_FacingUp))=="Bazooka");
	}
	
	//********************************canFall********************************
	
	@Test
	public final void test_canFallValid() {
		assertTrue(facade.canFall(WormFall));
	}
	
	@Test(expected = ModelException.class)
	public final void test_canFall1() {
		WormFall.terminate();
		assertFalse(facade.canFall(WormFall));
		facade.fall(WormFall);
	}
	
	@Test
	public final void test_canFall2() {
		assertFalse(facade.canFall(WormOut));
	}
	
	@Test
	public final void test_canFall3() {
		assertFalse(facade.canFall(WormZeroPosition));
	}
	
	@Test
	public final void test_canFall4() {
		facade.fall(WormJoske);
		assertFalse(facade.canFall(WormJoske));
	}
	
	@Test
	public final void test_canFall5() {
		facade.fall(WormFall);
		assertTrue(WormFall.isTerminated());
	}
	
//********************************canMove********************************
	
	@Test
	public final void test_canMoveValid() {
		assertTrue(facade.canMove(WormFall));
	}
		
	@Test
	public final void test_canMove1() {
		WormFall.terminate();
		assertFalse(facade.canMove(WormFall));
	}
		
//********************************shoot********************************
	/**
	 * Test for the method to move the worm, valid case.
	 */
	@Test
	public final void test_shoot() {
		facade.selectNextWeapon(WormJoske);
		assertTrue(WormJoske.canShoot());
		assertTrue(Util.fuzzyEquals(facade.getActionPoints(WormJoske),
									facade.getMaxActionPoints(WormJoske)));
		facade.shoot(WormJoske,50);
		assertTrue(Util.fuzzyEquals(WormJoske.getAPs(), WormJoske.getMaxAPs() 
				- WormJoske.getActiveWeapon().getShootingCost()));
	}
	
	@Test(expected = ModelException.class)
	public final void test_shoot1() {
		facade.jump(WormJoske, timeStep);
		facade.selectNextWeapon(WormJoske);
		facade.shoot(WormJoske,50);
	}	
	
//********************************program*******************************
		
	@Test
	public void testProgram() {
		IActionHandler handler = new SimpleActionHandler(facade);
		World world = facade.createWorld(100.0, 100.0, new boolean[][] { {true}, {false} }, random);
		ParseOutcome<?> outcome = facade.parseProgram("double x; while (x < 1.5) {\nx := x + 0.1;\n}\n turn x;", handler);
		assertTrue(outcome.isSuccess());
		Program program = ((Success)outcome).getResult();
		Worm worm1 = facade.createWorm(world, 50.0, 50.51, 0, 0.5, "Test", program);
		Worm worm2  = facade.createWorm(world, 50.0, 50.51, 0, 0.5, "Test", null);
		facade.addNewWorm(world, null); // add another worm
		double oldOrientation = facade.getOrientation(worm1);
		facade.startGame(world); // this will run the program
		double newOrientation = facade.getOrientation(worm1);
		assertEquals(oldOrientation + 1.5, newOrientation, EPS);
		assertNotEquals(worm1, facade.getCurrentWorm(world)); // turn must end after executing program
		assertTrue(facade.hasProgram(worm1));
		assertTrue(!facade.hasProgram(worm2));
		assertTrue(worm1.getProgram().getGlobalVariable().getWorm()==worm1);
		assertTrue(worm2.getProgram() == null);
	}
	
	@Test
	public void testProgram2() {
		IActionHandler handler = new SimpleActionHandler(facade);
		World world = facade.createWorld(100.0, 100.0, new boolean[][] { {true}, {false} }, random);
		ParseOutcome<?> outcome = facade.parseProgram("double x; while (x < 1.5) {\nx := x + 0.1;\n}\n turn x;", handler);
		assertTrue(outcome.isSuccess());
		Program program = ((Success)outcome).getResult();
		Worm worm3 = new Worm(world, program);
		Worm worm4  = new Worm(world, null);
		facade.startGame(world); // this will run the program
		assertTrue(worm3.getProgram().getGlobalVariable().getWorm()==worm3);
		assertTrue(worm4.getProgram() == null);
	}
		
	@Test
	public void testIsAilve() {
		assertTrue(facade.isAlive(WormAlbert_FacingUp));
		WormAlbert_FacingUp.terminate();
		assertTrue(!facade.isAlive(WormAlbert_FacingUp));		
	}
		
	@Test
	public void testGetMinimalRadius() {
		assertTrue(facade.getMinimalRadius(WormAlbert_FacingUp)==0.25);			
	}

	@Test
	public void testGetMass() {
		assertTrue(Util.fuzzyEquals(facade.getMass(WormAlbert_FacingUp), 556.0,0.1));
	}
}
